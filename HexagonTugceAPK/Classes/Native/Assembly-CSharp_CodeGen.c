﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void GridMapManager::Awake()
extern void GridMapManager_Awake_m6E8D86D8EFC58038823B4F476086538F8ED331F9 ();
// 0x00000002 System.Void GridMapManager::Start()
extern void GridMapManager_Start_mC2C2F8C9E10A63B36392EA87E6C0E4412751F1CA ();
// 0x00000003 System.Void GridMapManager::CreateGrid()
extern void GridMapManager_CreateGrid_mFFF3421D90C288DB3CA1F87BA5A87AB56FBED4B9 ();
// 0x00000004 System.Void GridMapManager::SelectHexagon(UnityEngine.Collider2D)
extern void GridMapManager_SelectHexagon_mC0DB6D9C605D20DCC436F24D10DDB411040CB9B6 ();
// 0x00000005 System.Void GridMapManager::RotateHexagons(System.Boolean)
extern void GridMapManager_RotateHexagons_mAD1E1D587794465A4189001F3FB4E15685182BF8 ();
// 0x00000006 System.Void GridMapManager::FindHexagonGroup()
extern void GridMapManager_FindHexagonGroup_mE0BE80BC83F5FBC6186AA7FC2B2E16ACA87BCA82 ();
// 0x00000007 System.Collections.IEnumerator GridMapManager::RotationCheckingCoroutine(System.Boolean)
extern void GridMapManager_RotationCheckingCoroutine_mE145CE6BD7A171A4693E2DF826A37D829D61F3A1 ();
// 0x00000008 System.Void GridMapManager::SwapHexagon(System.Boolean)
extern void GridMapManager_SwapHexagon_mC8A4398C219C587EFAB4490F0C24FAC7216E2B27 ();
// 0x00000009 System.Collections.Generic.List`1<HexagonManager> GridMapManager::CheckingExplosion(System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>>)
extern void GridMapManager_CheckingExplosion_m5FBB713BB06BCF9D906D7A401DA316494322B5C1 ();
// 0x0000000A System.Collections.Generic.List`1<System.Int32> GridMapManager::DetectBombANDClearExplodeHexagon(System.Collections.Generic.List`1<HexagonManager>)
extern void GridMapManager_DetectBombANDClearExplodeHexagon_m6A4071F050BF7576CA67F92580349C93711471EF ();
// 0x0000000B System.Void GridMapManager::DestroyOutline()
extern void GridMapManager_DestroyOutline_m9874F92E892AB57F87DE99F9740C6770F1A27385 ();
// 0x0000000C System.Void GridMapManager::CreateOutline()
extern void GridMapManager_CreateOutline_m1308C33D309E14FAE44B40A754A2033952625BD2 ();
// 0x0000000D System.Collections.IEnumerator GridMapManager::ProduceHexagon(System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>>)
extern void GridMapManager_ProduceHexagon_m214E1A2F495919DEC056EA874C97E901CC3F4621 ();
// 0x0000000E System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>> GridMapManager::ColoredGridProduction()
extern void GridMapManager_ColoredGridProduction_m26846DC1A6559F858A1DDC079E04CB70A04344B2 ();
// 0x0000000F System.Boolean GridMapManager::DetectColumn(System.Int32)
extern void GridMapManager_DetectColumn_m10378C564143B7BCC0F3945603BF7499BE472534 ();
// 0x00000010 System.Boolean GridMapManager::InputAvailable()
extern void GridMapManager_InputAvailable_m2DFC0C65CF2EE1843ED10B7DF610A40CF109F5DA ();
// 0x00000011 System.Single GridMapManager::GetGridStartCoordinateX()
extern void GridMapManager_GetGridStartCoordinateX_m039E8599B60121EAB5A5460E2DC44C36FA82C59D ();
// 0x00000012 System.Boolean GridMapManager::IsValid(UnityEngine.Vector2)
extern void GridMapManager_IsValid_m9B4B23F940F3FE203303CD85ACBC41F4596B8093 ();
// 0x00000013 System.Void GridMapManager::SetGridWidth(System.Int32)
extern void GridMapManager_SetGridWidth_mCDC775F41010E50C72D4C68A0643CF7E0D3B904B ();
// 0x00000014 System.Int32 GridMapManager::GetGridWidth()
extern void GridMapManager_GetGridWidth_m4DF42E736F4B0984D7E727901ACF84405DD895BA ();
// 0x00000015 System.Void GridMapManager::SetGridHeight(System.Int32)
extern void GridMapManager_SetGridHeight_m89865CCB85B689CA751046765A238E10A794F509 ();
// 0x00000016 System.Int32 GridMapManager::GetGridHeight()
extern void GridMapManager_GetGridHeight_m76516C97F13FF377E0E91F5A48EA948867E5C41B ();
// 0x00000017 System.Void GridMapManager::SetColorList(System.Collections.Generic.List`1<UnityEngine.Color>)
extern void GridMapManager_SetColorList_mBF555D15D608B2AE94B83811BDFC9DF798FF7651 ();
// 0x00000018 System.Void GridMapManager::SetBombProduction()
extern void GridMapManager_SetBombProduction_mC86F578E5457E53A98856BB567B5EA081BED1BE2 ();
// 0x00000019 HexagonManager GridMapManager::GetSelectedHexagon()
extern void GridMapManager_GetSelectedHexagon_m82E2459ADA9669F14C94801054CBB147501B7A4C ();
// 0x0000001A System.Void GridMapManager::.ctor()
extern void GridMapManager__ctor_mAFC93605BB3157CFBCB81094DDE41DF3ABDDD8B7 ();
// 0x0000001B System.Void GridMapManager::.cctor()
extern void GridMapManager__cctor_m0E439D4CCCD36AF686D721CB271F6BEAE9C2678D ();
// 0x0000001C System.Void HexagonManager::Start()
extern void HexagonManager_Start_mD2964CFB9793B0403254CD1F067061A9B022002F ();
// 0x0000001D System.Void HexagonManager::Update()
extern void HexagonManager_Update_mA608E8C036E21D38479A5E8E28F01F66EC8C03CC ();
// 0x0000001E System.Void HexagonManager::Rotate(System.Int32,System.Int32,UnityEngine.Vector2)
extern void HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC ();
// 0x0000001F HexagonManager_NeighbourHexes HexagonManager::GetNeighbours()
extern void HexagonManager_GetNeighbours_m12298ED576B60FB0F5DF30613D7E508CA6149E12 ();
// 0x00000020 System.Void HexagonManager::ChangeWorldPosition(UnityEngine.Vector2)
extern void HexagonManager_ChangeWorldPosition_m4AFE1537B92653C2C4C7EE6A35C5FA01469AFE1C ();
// 0x00000021 System.Void HexagonManager::ChangeGridPosition(UnityEngine.Vector2)
extern void HexagonManager_ChangeGridPosition_m31C3E0BA897DF72C9A6A28640C0ED63E692BF44F ();
// 0x00000022 System.Void HexagonManager::SetBomb()
extern void HexagonManager_SetBomb_mA40C2AB6C12399179D44B1FD7B64BA47E53C5533 ();
// 0x00000023 System.Void HexagonManager::SetX(System.Int32)
extern void HexagonManager_SetX_m479990D055BBF89BDF258489087EFC8AD084CC0C ();
// 0x00000024 System.Int32 HexagonManager::GetX()
extern void HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48 ();
// 0x00000025 System.Void HexagonManager::SetY(System.Int32)
extern void HexagonManager_SetY_m67E6A395342D87F427D4EE382A82AD1D7CAF0CFD ();
// 0x00000026 System.Int32 HexagonManager::GetY()
extern void HexagonManager_GetY_m3B711DA0B2F3DAA2347CFE9AD3D7AD597DCB8B2D ();
// 0x00000027 System.Void HexagonManager::SetColor(UnityEngine.Color)
extern void HexagonManager_SetColor_mAFE672E70F49F0041DB82EEF7396ADBFFFB12016 ();
// 0x00000028 UnityEngine.Color HexagonManager::GetColor()
extern void HexagonManager_GetColor_mED456B54AEF3D88B662951813B71E6D833F046D0 ();
// 0x00000029 System.Void HexagonManager::Trigger()
extern void HexagonManager_Trigger_mEDA1CF56DE33D03BC1E8F0146550DFAEEED9890B ();
// 0x0000002A System.Int32 HexagonManager::GetTimer()
extern void HexagonManager_GetTimer_mEE88C69BB650E3740B5B30D59302877ED000E980 ();
// 0x0000002B System.Void HexagonManager::.ctor()
extern void HexagonManager__ctor_m8CA3794AD77B9103A96F3FE668C7E559D8E8A04A ();
// 0x0000002C System.Void InputGetter::Start()
extern void InputGetter_Start_m38C9891BD3D7C690C5E8ABD1B553550CBD249AFC ();
// 0x0000002D System.Void InputGetter::Update()
extern void InputGetter_Update_mC7B9E787753BC507B57DAF5EBEF9A90AF27B45E9 ();
// 0x0000002E System.Void InputGetter::DetectingTouch()
extern void InputGetter_DetectingTouch_m245D4ED56A833ABB012042EAECE74957B6D22DEA ();
// 0x0000002F System.Void InputGetter::CheckingSelection(UnityEngine.Collider2D)
extern void InputGetter_CheckingSelection_m21269E14235F6F39DB2DABD55896D41F561CFBBC ();
// 0x00000030 System.Void InputGetter::CheckingRotation()
extern void InputGetter_CheckingRotation_m7CA3205BA425261E6AF8B8F14EE2A6C108AA516E ();
// 0x00000031 System.Void InputGetter::.ctor()
extern void InputGetter__ctor_mE1C1AA3E275CCFF7B2E0BEDFB8CBB76F8C61113C ();
// 0x00000032 System.Void PropertiesClass::.ctor()
extern void PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE ();
// 0x00000033 System.Void StartMenuManager::ChangeScene(System.String)
extern void StartMenuManager_ChangeScene_m5E8158A6EE2A8FBECA35BD8F50A8FB0BFD4E3755 ();
// 0x00000034 System.Void StartMenuManager::QuitGame()
extern void StartMenuManager_QuitGame_m406694FBE1403CE9CDC6F8EFE730F08B427D4BD0 ();
// 0x00000035 System.Void StartMenuManager::.ctor()
extern void StartMenuManager__ctor_mEFFC9A4F8FCA7C0577E735261F26CC6A33AD887F ();
// 0x00000036 System.Void UIManager::Awake()
extern void UIManager_Awake_mA946658D43E2FC2C3479BB9322CED4FF4C5D2EA0 ();
// 0x00000037 System.Void UIManager::Start()
extern void UIManager_Start_mE176851C74E87A3EBAF5D28B5BFC4D8426D91397 ();
// 0x00000038 System.Void UIManager::Update()
extern void UIManager_Update_mEA6AD9AEDC70385E271BEDCF20C0F4CBD5AC9E7C ();
// 0x00000039 System.Void UIManager::BackButton(System.String)
extern void UIManager_BackButton_m43ACD47CEDD88A949C13D3570C8A0E6D5DB0C10D ();
// 0x0000003A System.Void UIManager::WidthDropdownChange(UnityEngine.UI.Dropdown)
extern void UIManager_WidthDropdownChange_mF2FD08C9684FAAE87824DC13C41521AF6FDA0FF5 ();
// 0x0000003B System.Void UIManager::HeightDropdownChange(UnityEngine.UI.Dropdown)
extern void UIManager_HeightDropdownChange_mE3515FC50FF564F9A87A4BBED4DA0BF66334AA61 ();
// 0x0000003C System.Void UIManager::ColorDropdownChanged(UnityEngine.UI.Dropdown)
extern void UIManager_ColorDropdownChanged_m66F922A95A52AE43900A370B06F4A0D15114B189 ();
// 0x0000003D System.Void UIManager::StartGameButton()
extern void UIManager_StartGameButton_m81242AD25701DFE9D8D263296534705B5750DD09 ();
// 0x0000003E System.Void UIManager::DefaultSettings()
extern void UIManager_DefaultSettings_m1E07491312E42FF549B5E429DFB3CDCD025E01A9 ();
// 0x0000003F System.Void UIManager::Score(System.Int32)
extern void UIManager_Score_m88AA8E3F6C04A7C3C11EF47F2A8D65E72EFEF63D ();
// 0x00000040 System.Void UIManager::GameEnd()
extern void UIManager_GameEnd_mA5D7B91BBE6BDDBC191E2860910EB1A927F8E6EB ();
// 0x00000041 System.Void UIManager::.ctor()
extern void UIManager__ctor_m40CA6521CEDDF979D58B6050A6D294A32A1CEA69 ();
// 0x00000042 System.Void GridMapManager_<RotationCheckingCoroutine>d__26::.ctor(System.Int32)
extern void U3CRotationCheckingCoroutineU3Ed__26__ctor_m7A58025DEA96C9E7384F73E935327F43A258C283 ();
// 0x00000043 System.Void GridMapManager_<RotationCheckingCoroutine>d__26::System.IDisposable.Dispose()
extern void U3CRotationCheckingCoroutineU3Ed__26_System_IDisposable_Dispose_m908CC17F2CE87867694454D817BA1FBCC71AB6CD ();
// 0x00000044 System.Boolean GridMapManager_<RotationCheckingCoroutine>d__26::MoveNext()
extern void U3CRotationCheckingCoroutineU3Ed__26_MoveNext_mCC5274CE72BFCD64D9C61EEAEE85261124845CA6 ();
// 0x00000045 System.Object GridMapManager_<RotationCheckingCoroutine>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRotationCheckingCoroutineU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCCD2467DFA08642E5274DD8493DFB6A523C7E590 ();
// 0x00000046 System.Void GridMapManager_<RotationCheckingCoroutine>d__26::System.Collections.IEnumerator.Reset()
extern void U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_Reset_m601DFC85C4329034BFEEC55EA05740CD4237C0EA ();
// 0x00000047 System.Object GridMapManager_<RotationCheckingCoroutine>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_get_Current_m2CE4E623B0B56CD21F3BE1762061C82A0AFF8B69 ();
// 0x00000048 System.Void GridMapManager_<ProduceHexagon>d__32::.ctor(System.Int32)
extern void U3CProduceHexagonU3Ed__32__ctor_m7C3798865DC945DA4EACAAA760443EAFBD8033C8 ();
// 0x00000049 System.Void GridMapManager_<ProduceHexagon>d__32::System.IDisposable.Dispose()
extern void U3CProduceHexagonU3Ed__32_System_IDisposable_Dispose_m9B3606C41110AC2C771F556F19878B6BA3DFA5F4 ();
// 0x0000004A System.Boolean GridMapManager_<ProduceHexagon>d__32::MoveNext()
extern void U3CProduceHexagonU3Ed__32_MoveNext_m16EF3B112413C7F8EA4BC66B99882AB877E9D8E7 ();
// 0x0000004B System.Void GridMapManager_<ProduceHexagon>d__32::<>m__Finally1()
extern void U3CProduceHexagonU3Ed__32_U3CU3Em__Finally1_m6D2CEC25E49CF58E9CF395DD0B1951967D004B63 ();
// 0x0000004C System.Object GridMapManager_<ProduceHexagon>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProduceHexagonU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0A6B4CB588C3EDEC817AFFFF4B6E72163C77269 ();
// 0x0000004D System.Void GridMapManager_<ProduceHexagon>d__32::System.Collections.IEnumerator.Reset()
extern void U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_Reset_m2BBF02C5180830143CB1CA1F8700AE5E16943D11 ();
// 0x0000004E System.Object GridMapManager_<ProduceHexagon>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_get_Current_mA89EE43A7D28B5CCD49A138D1F68D5BD2A258821 ();
static Il2CppMethodPointer s_methodPointers[78] = 
{
	GridMapManager_Awake_m6E8D86D8EFC58038823B4F476086538F8ED331F9,
	GridMapManager_Start_mC2C2F8C9E10A63B36392EA87E6C0E4412751F1CA,
	GridMapManager_CreateGrid_mFFF3421D90C288DB3CA1F87BA5A87AB56FBED4B9,
	GridMapManager_SelectHexagon_mC0DB6D9C605D20DCC436F24D10DDB411040CB9B6,
	GridMapManager_RotateHexagons_mAD1E1D587794465A4189001F3FB4E15685182BF8,
	GridMapManager_FindHexagonGroup_mE0BE80BC83F5FBC6186AA7FC2B2E16ACA87BCA82,
	GridMapManager_RotationCheckingCoroutine_mE145CE6BD7A171A4693E2DF826A37D829D61F3A1,
	GridMapManager_SwapHexagon_mC8A4398C219C587EFAB4490F0C24FAC7216E2B27,
	GridMapManager_CheckingExplosion_m5FBB713BB06BCF9D906D7A401DA316494322B5C1,
	GridMapManager_DetectBombANDClearExplodeHexagon_m6A4071F050BF7576CA67F92580349C93711471EF,
	GridMapManager_DestroyOutline_m9874F92E892AB57F87DE99F9740C6770F1A27385,
	GridMapManager_CreateOutline_m1308C33D309E14FAE44B40A754A2033952625BD2,
	GridMapManager_ProduceHexagon_m214E1A2F495919DEC056EA874C97E901CC3F4621,
	GridMapManager_ColoredGridProduction_m26846DC1A6559F858A1DDC079E04CB70A04344B2,
	GridMapManager_DetectColumn_m10378C564143B7BCC0F3945603BF7499BE472534,
	GridMapManager_InputAvailable_m2DFC0C65CF2EE1843ED10B7DF610A40CF109F5DA,
	GridMapManager_GetGridStartCoordinateX_m039E8599B60121EAB5A5460E2DC44C36FA82C59D,
	GridMapManager_IsValid_m9B4B23F940F3FE203303CD85ACBC41F4596B8093,
	GridMapManager_SetGridWidth_mCDC775F41010E50C72D4C68A0643CF7E0D3B904B,
	GridMapManager_GetGridWidth_m4DF42E736F4B0984D7E727901ACF84405DD895BA,
	GridMapManager_SetGridHeight_m89865CCB85B689CA751046765A238E10A794F509,
	GridMapManager_GetGridHeight_m76516C97F13FF377E0E91F5A48EA948867E5C41B,
	GridMapManager_SetColorList_mBF555D15D608B2AE94B83811BDFC9DF798FF7651,
	GridMapManager_SetBombProduction_mC86F578E5457E53A98856BB567B5EA081BED1BE2,
	GridMapManager_GetSelectedHexagon_m82E2459ADA9669F14C94801054CBB147501B7A4C,
	GridMapManager__ctor_mAFC93605BB3157CFBCB81094DDE41DF3ABDDD8B7,
	GridMapManager__cctor_m0E439D4CCCD36AF686D721CB271F6BEAE9C2678D,
	HexagonManager_Start_mD2964CFB9793B0403254CD1F067061A9B022002F,
	HexagonManager_Update_mA608E8C036E21D38479A5E8E28F01F66EC8C03CC,
	HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC,
	HexagonManager_GetNeighbours_m12298ED576B60FB0F5DF30613D7E508CA6149E12,
	HexagonManager_ChangeWorldPosition_m4AFE1537B92653C2C4C7EE6A35C5FA01469AFE1C,
	HexagonManager_ChangeGridPosition_m31C3E0BA897DF72C9A6A28640C0ED63E692BF44F,
	HexagonManager_SetBomb_mA40C2AB6C12399179D44B1FD7B64BA47E53C5533,
	HexagonManager_SetX_m479990D055BBF89BDF258489087EFC8AD084CC0C,
	HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48,
	HexagonManager_SetY_m67E6A395342D87F427D4EE382A82AD1D7CAF0CFD,
	HexagonManager_GetY_m3B711DA0B2F3DAA2347CFE9AD3D7AD597DCB8B2D,
	HexagonManager_SetColor_mAFE672E70F49F0041DB82EEF7396ADBFFFB12016,
	HexagonManager_GetColor_mED456B54AEF3D88B662951813B71E6D833F046D0,
	HexagonManager_Trigger_mEDA1CF56DE33D03BC1E8F0146550DFAEEED9890B,
	HexagonManager_GetTimer_mEE88C69BB650E3740B5B30D59302877ED000E980,
	HexagonManager__ctor_m8CA3794AD77B9103A96F3FE668C7E559D8E8A04A,
	InputGetter_Start_m38C9891BD3D7C690C5E8ABD1B553550CBD249AFC,
	InputGetter_Update_mC7B9E787753BC507B57DAF5EBEF9A90AF27B45E9,
	InputGetter_DetectingTouch_m245D4ED56A833ABB012042EAECE74957B6D22DEA,
	InputGetter_CheckingSelection_m21269E14235F6F39DB2DABD55896D41F561CFBBC,
	InputGetter_CheckingRotation_m7CA3205BA425261E6AF8B8F14EE2A6C108AA516E,
	InputGetter__ctor_mE1C1AA3E275CCFF7B2E0BEDFB8CBB76F8C61113C,
	PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE,
	StartMenuManager_ChangeScene_m5E8158A6EE2A8FBECA35BD8F50A8FB0BFD4E3755,
	StartMenuManager_QuitGame_m406694FBE1403CE9CDC6F8EFE730F08B427D4BD0,
	StartMenuManager__ctor_mEFFC9A4F8FCA7C0577E735261F26CC6A33AD887F,
	UIManager_Awake_mA946658D43E2FC2C3479BB9322CED4FF4C5D2EA0,
	UIManager_Start_mE176851C74E87A3EBAF5D28B5BFC4D8426D91397,
	UIManager_Update_mEA6AD9AEDC70385E271BEDCF20C0F4CBD5AC9E7C,
	UIManager_BackButton_m43ACD47CEDD88A949C13D3570C8A0E6D5DB0C10D,
	UIManager_WidthDropdownChange_mF2FD08C9684FAAE87824DC13C41521AF6FDA0FF5,
	UIManager_HeightDropdownChange_mE3515FC50FF564F9A87A4BBED4DA0BF66334AA61,
	UIManager_ColorDropdownChanged_m66F922A95A52AE43900A370B06F4A0D15114B189,
	UIManager_StartGameButton_m81242AD25701DFE9D8D263296534705B5750DD09,
	UIManager_DefaultSettings_m1E07491312E42FF549B5E429DFB3CDCD025E01A9,
	UIManager_Score_m88AA8E3F6C04A7C3C11EF47F2A8D65E72EFEF63D,
	UIManager_GameEnd_mA5D7B91BBE6BDDBC191E2860910EB1A927F8E6EB,
	UIManager__ctor_m40CA6521CEDDF979D58B6050A6D294A32A1CEA69,
	U3CRotationCheckingCoroutineU3Ed__26__ctor_m7A58025DEA96C9E7384F73E935327F43A258C283,
	U3CRotationCheckingCoroutineU3Ed__26_System_IDisposable_Dispose_m908CC17F2CE87867694454D817BA1FBCC71AB6CD,
	U3CRotationCheckingCoroutineU3Ed__26_MoveNext_mCC5274CE72BFCD64D9C61EEAEE85261124845CA6,
	U3CRotationCheckingCoroutineU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCCD2467DFA08642E5274DD8493DFB6A523C7E590,
	U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_Reset_m601DFC85C4329034BFEEC55EA05740CD4237C0EA,
	U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_get_Current_m2CE4E623B0B56CD21F3BE1762061C82A0AFF8B69,
	U3CProduceHexagonU3Ed__32__ctor_m7C3798865DC945DA4EACAAA760443EAFBD8033C8,
	U3CProduceHexagonU3Ed__32_System_IDisposable_Dispose_m9B3606C41110AC2C771F556F19878B6BA3DFA5F4,
	U3CProduceHexagonU3Ed__32_MoveNext_m16EF3B112413C7F8EA4BC66B99882AB877E9D8E7,
	U3CProduceHexagonU3Ed__32_U3CU3Em__Finally1_m6D2CEC25E49CF58E9CF395DD0B1951967D004B63,
	U3CProduceHexagonU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0A6B4CB588C3EDEC817AFFFF4B6E72163C77269,
	U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_Reset_m2BBF02C5180830143CB1CA1F8700AE5E16943D11,
	U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_get_Current_mA89EE43A7D28B5CCD49A138D1F68D5BD2A258821,
};
static const int32_t s_InvokerIndices[78] = 
{
	13,
	13,
	13,
	4,
	44,
	13,
	306,
	44,
	6,
	6,
	13,
	13,
	16,
	14,
	61,
	17,
	656,
	1074,
	9,
	18,
	9,
	18,
	4,
	13,
	14,
	13,
	8,
	13,
	13,
	1654,
	1655,
	1209,
	1209,
	13,
	9,
	18,
	9,
	18,
	1215,
	1088,
	13,
	18,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	13,
	4,
	4,
	4,
	4,
	13,
	13,
	9,
	13,
	13,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	13,
	14,
	13,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	78,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
