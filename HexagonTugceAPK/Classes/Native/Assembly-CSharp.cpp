﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// GridMapManager
struct GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773;
// GridMapManager/<ProduceHexagon>d__32
struct U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C;
// GridMapManager/<RotationCheckingCoroutine>d__26
struct U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9;
// HexagonManager
struct HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009;
// HexagonManager[]
struct HexagonManagerU5BU5D_t66D9494A61DC57758F74D5ADCB0B39C378AE97D4;
// InputGetter
struct InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD;
// PropertiesClass
struct PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63;
// StartMenuManager
struct StartMenuManager_t2908FBD03EFB4C62AB2BEC6BB723A8063C085637;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<HexagonManager>
struct List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21;
// System.Collections.Generic.List`1<HexagonManager>[]
struct List_1U5BU5D_tB3506D35DB3EE95C9D7636D91DFF2FF6D22FD1BD;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>>
struct List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>>
struct List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t053DAB6E2110E276A0339D73497193F464BC1F82;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86;
// System.Collections.Generic.List`1<UnityEngine.Color>[]
struct List_1U5BU5D_tF315EAF85763BEA6BFEAA79CC3C9FDC6C1D4E3EA;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t836CD930F5F0862929A362435417DA9BCD4186F8;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UIManager
struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Collider2D
struct Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8;

IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Physics2D_tB21970F986016656D66D2922594F336E1EE7D5C7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral08596D60C57FD87E530CBDD2A06052B06D8C4137;
IL2CPP_EXTERN_C String_t* _stringLiteral9711DD29F19F33DC805E32FBF54610C089EF6DD5;
IL2CPP_EXTERN_C String_t* _stringLiteral9D55681DD84F4176A643D69263854D33720B07E9;
IL2CPP_EXTERN_C String_t* _stringLiteralBEE830DEDB0CB719867FB0342E3E97C80EE48B62;
IL2CPP_EXTERN_C String_t* _stringLiteralCDD6290B6B2860663A3FF7C60CBE70AF640E9C39;
IL2CPP_EXTERN_C String_t* _stringLiteralD183CEB428C06D7EF5C8DEDEEAC4246671FBD944;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCollider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_m2AF5C85C582A45B02A4E940C7E8B19A065A37405_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5B95C6AF8FD437069099C69F7FDB9A4C5FAC6F4B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m992731D0C6BC506250880A1095BCFCFEED22350F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mBA3B0129DABD8274AF3497CC93E6A2DEA0A23892_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m113E33A615748C69D63D1245F5FD820B4B3D43F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m45D437EE4A29B63712A1B64D39D2809F3594A0DE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m6DEA264C3D3852D1E3485CE0DFABE349D2C8FD6F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m88A0089A1A4EEBC3017E2DA569A01C7919B10945_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_m034B7C9CB50717611871DEBE6CB07147F506A8E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF57CA692C5FFBA2C6599F6FEEA08E0F9050C368A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5417F5D6065EA0BDB12492F51D50F061AA28FAF4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m2005A4FE9473352474F6255670AA4F580BDB1CC5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m2DF903991E5929EB07031535A5577397CA88ECA6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m91E44F05F6B99F382B086DBDE40A2EBE95B30F2D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m0D768CC6350116CA5EE94FFE68CD7AD10C81242B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m83178F038A7D4A7E9B0731B7D3078EDCF6FFD0EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m6E540121DD0B257A950F046285B96E436394CACE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m5F0329865A129AC95740F9057A8E346327D312A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mB36E5A3444519179409E72DA2E6AE580BD1962B0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mCB1A1A5FF01B45922A9B49BD843FE64B2118479F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_set_Item_m99A8C26E8C19A07648E04317DD42457C40A68A99_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m6895A7A231540279E01A537649EB42814FD2671B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_Reset_m2BBF02C5180830143CB1CA1F8700AE5E16943D11_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_Reset_m601DFC85C4329034BFEEC55EA05740CD4237C0EA_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t GridMapManager_Awake_m6E8D86D8EFC58038823B4F476086538F8ED331F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_CheckingExplosion_m5FBB713BB06BCF9D906D7A401DA316494322B5C1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_ColoredGridProduction_m26846DC1A6559F858A1DDC079E04CB70A04344B2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_CreateGrid_mFFF3421D90C288DB3CA1F87BA5A87AB56FBED4B9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_CreateOutline_m1308C33D309E14FAE44B40A754A2033952625BD2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_DestroyOutline_m9874F92E892AB57F87DE99F9740C6770F1A27385_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_DetectBombANDClearExplodeHexagon_m6A4071F050BF7576CA67F92580349C93711471EF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_FindHexagonGroup_mE0BE80BC83F5FBC6186AA7FC2B2E16ACA87BCA82_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_ProduceHexagon_m214E1A2F495919DEC056EA874C97E901CC3F4621_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_RotationCheckingCoroutine_mE145CE6BD7A171A4693E2DF826A37D829D61F3A1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_SelectHexagon_mC0DB6D9C605D20DCC436F24D10DDB411040CB9B6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_Start_mC2C2F8C9E10A63B36392EA87E6C0E4412751F1CA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GridMapManager_SwapHexagon_mC8A4398C219C587EFAB4490F0C24FAC7216E2B27_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HexagonManager_GetColor_mED456B54AEF3D88B662951813B71E6D833F046D0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HexagonManager_SetBomb_mA40C2AB6C12399179D44B1FD7B64BA47E53C5533_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HexagonManager_SetColor_mAFE672E70F49F0041DB82EEF7396ADBFFFB12016_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HexagonManager_Start_mD2964CFB9793B0403254CD1F067061A9B022002F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HexagonManager_Update_mA608E8C036E21D38479A5E8E28F01F66EC8C03CC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InputGetter_CheckingRotation_m7CA3205BA425261E6AF8B8F14EE2A6C108AA516E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InputGetter_CheckingSelection_m21269E14235F6F39DB2DABD55896D41F561CFBBC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InputGetter_Start_m38C9891BD3D7C690C5E8ABD1B553550CBD249AFC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InputGetter_Update_mC7B9E787753BC507B57DAF5EBEF9A90AF27B45E9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CProduceHexagonU3Ed__32_MoveNext_m16EF3B112413C7F8EA4BC66B99882AB877E9D8E7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_Reset_m2BBF02C5180830143CB1CA1F8700AE5E16943D11_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CProduceHexagonU3Ed__32_U3CU3Em__Finally1_m6D2CEC25E49CF58E9CF395DD0B1951967D004B63_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CRotationCheckingCoroutineU3Ed__26_MoveNext_mCC5274CE72BFCD64D9C61EEAEE85261124845CA6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_Reset_m601DFC85C4329034BFEEC55EA05740CD4237C0EA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UIManager_Awake_mA946658D43E2FC2C3479BB9322CED4FF4C5D2EA0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UIManager_HeightDropdownChange_mE3515FC50FF564F9A87A4BBED4DA0BF66334AA61_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UIManager_StartGameButton_m81242AD25701DFE9D8D263296534705B5750DD09_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UIManager_Start_mE176851C74E87A3EBAF5D28B5BFC4D8426D91397_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UIManager_WidthDropdownChange_mF2FD08C9684FAAE87824DC13C41521AF6FDA0FF5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UIManager__ctor_m40CA6521CEDDF979D58B6050A6D294A32A1CEA69_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// System.Object


// GridMapManager_<RotationCheckingCoroutine>d__26
struct  U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9  : public RuntimeObject
{
public:
	// System.Int32 GridMapManager_<RotationCheckingCoroutine>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GridMapManager_<RotationCheckingCoroutine>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GridMapManager GridMapManager_<RotationCheckingCoroutine>d__26::<>4__this
	GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * ___U3CU3E4__this_2;
	// System.Boolean GridMapManager_<RotationCheckingCoroutine>d__26::clockWise
	bool ___clockWise_3;
	// System.Collections.Generic.List`1<HexagonManager> GridMapManager_<RotationCheckingCoroutine>d__26::<explosiveHexagon>5__2
	List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * ___U3CexplosiveHexagonU3E5__2_4;
	// System.Boolean GridMapManager_<RotationCheckingCoroutine>d__26::<flag>5__3
	bool ___U3CflagU3E5__3_5;
	// System.Int32 GridMapManager_<RotationCheckingCoroutine>d__26::<i>5__4
	int32_t ___U3CiU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9, ___U3CU3E4__this_2)); }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_clockWise_3() { return static_cast<int32_t>(offsetof(U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9, ___clockWise_3)); }
	inline bool get_clockWise_3() const { return ___clockWise_3; }
	inline bool* get_address_of_clockWise_3() { return &___clockWise_3; }
	inline void set_clockWise_3(bool value)
	{
		___clockWise_3 = value;
	}

	inline static int32_t get_offset_of_U3CexplosiveHexagonU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9, ___U3CexplosiveHexagonU3E5__2_4)); }
	inline List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * get_U3CexplosiveHexagonU3E5__2_4() const { return ___U3CexplosiveHexagonU3E5__2_4; }
	inline List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 ** get_address_of_U3CexplosiveHexagonU3E5__2_4() { return &___U3CexplosiveHexagonU3E5__2_4; }
	inline void set_U3CexplosiveHexagonU3E5__2_4(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * value)
	{
		___U3CexplosiveHexagonU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CexplosiveHexagonU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CflagU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9, ___U3CflagU3E5__3_5)); }
	inline bool get_U3CflagU3E5__3_5() const { return ___U3CflagU3E5__3_5; }
	inline bool* get_address_of_U3CflagU3E5__3_5() { return &___U3CflagU3E5__3_5; }
	inline void set_U3CflagU3E5__3_5(bool value)
	{
		___U3CflagU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9, ___U3CiU3E5__4_6)); }
	inline int32_t get_U3CiU3E5__4_6() const { return ___U3CiU3E5__4_6; }
	inline int32_t* get_address_of_U3CiU3E5__4_6() { return &___U3CiU3E5__4_6; }
	inline void set_U3CiU3E5__4_6(int32_t value)
	{
		___U3CiU3E5__4_6 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<HexagonManager>
struct  List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	HexagonManagerU5BU5D_t66D9494A61DC57758F74D5ADCB0B39C378AE97D4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21, ____items_1)); }
	inline HexagonManagerU5BU5D_t66D9494A61DC57758F74D5ADCB0B39C378AE97D4* get__items_1() const { return ____items_1; }
	inline HexagonManagerU5BU5D_t66D9494A61DC57758F74D5ADCB0B39C378AE97D4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(HexagonManagerU5BU5D_t66D9494A61DC57758F74D5ADCB0B39C378AE97D4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	HexagonManagerU5BU5D_t66D9494A61DC57758F74D5ADCB0B39C378AE97D4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21_StaticFields, ____emptyArray_5)); }
	inline HexagonManagerU5BU5D_t66D9494A61DC57758F74D5ADCB0B39C378AE97D4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline HexagonManagerU5BU5D_t66D9494A61DC57758F74D5ADCB0B39C378AE97D4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(HexagonManagerU5BU5D_t66D9494A61DC57758F74D5ADCB0B39C378AE97D4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>>
struct  List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	List_1U5BU5D_tB3506D35DB3EE95C9D7636D91DFF2FF6D22FD1BD* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C, ____items_1)); }
	inline List_1U5BU5D_tB3506D35DB3EE95C9D7636D91DFF2FF6D22FD1BD* get__items_1() const { return ____items_1; }
	inline List_1U5BU5D_tB3506D35DB3EE95C9D7636D91DFF2FF6D22FD1BD** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(List_1U5BU5D_tB3506D35DB3EE95C9D7636D91DFF2FF6D22FD1BD* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	List_1U5BU5D_tB3506D35DB3EE95C9D7636D91DFF2FF6D22FD1BD* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C_StaticFields, ____emptyArray_5)); }
	inline List_1U5BU5D_tB3506D35DB3EE95C9D7636D91DFF2FF6D22FD1BD* get__emptyArray_5() const { return ____emptyArray_5; }
	inline List_1U5BU5D_tB3506D35DB3EE95C9D7636D91DFF2FF6D22FD1BD** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(List_1U5BU5D_tB3506D35DB3EE95C9D7636D91DFF2FF6D22FD1BD* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>>
struct  List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	List_1U5BU5D_tF315EAF85763BEA6BFEAA79CC3C9FDC6C1D4E3EA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219, ____items_1)); }
	inline List_1U5BU5D_tF315EAF85763BEA6BFEAA79CC3C9FDC6C1D4E3EA* get__items_1() const { return ____items_1; }
	inline List_1U5BU5D_tF315EAF85763BEA6BFEAA79CC3C9FDC6C1D4E3EA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(List_1U5BU5D_tF315EAF85763BEA6BFEAA79CC3C9FDC6C1D4E3EA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	List_1U5BU5D_tF315EAF85763BEA6BFEAA79CC3C9FDC6C1D4E3EA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219_StaticFields, ____emptyArray_5)); }
	inline List_1U5BU5D_tF315EAF85763BEA6BFEAA79CC3C9FDC6C1D4E3EA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline List_1U5BU5D_tF315EAF85763BEA6BFEAA79CC3C9FDC6C1D4E3EA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(List_1U5BU5D_tF315EAF85763BEA6BFEAA79CC3C9FDC6C1D4E3EA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____items_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Color>
struct  List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86, ____items_1)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get__items_1() const { return ____items_1; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86_StaticFields, ____emptyArray_5)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<HexagonManager>
struct  Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578, ___list_0)); }
	inline List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * get_list_0() const { return ___list_0; }
	inline List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578, ___current_3)); }
	inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * get_current_3() const { return ___current_3; }
	inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Int32>
struct  Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C, ___list_0)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_list_0() const { return ___list_0; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_SelectedSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_3)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	float ___m_Seconds_0;
};

// GridMapManager_<ProduceHexagon>d__32
struct  U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C  : public RuntimeObject
{
public:
	// System.Int32 GridMapManager_<ProduceHexagon>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GridMapManager_<ProduceHexagon>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GridMapManager GridMapManager_<ProduceHexagon>d__32::<>4__this
	GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<System.Int32> GridMapManager_<ProduceHexagon>d__32::columns
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___columns_3;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>> GridMapManager_<ProduceHexagon>d__32::color
	List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * ___color_4;
	// UnityEngine.Vector3 GridMapManager_<ProduceHexagon>d__32::<startPosition>5__2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CstartPositionU3E5__2_5;
	// System.Single GridMapManager_<ProduceHexagon>d__32::<startX>5__3
	float ___U3CstartXU3E5__3_6;
	// System.Collections.Generic.List`1_Enumerator<System.Int32> GridMapManager_<ProduceHexagon>d__32::<>7__wrap3
	Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  ___U3CU3E7__wrap3_7;
	// System.Int32 GridMapManager_<ProduceHexagon>d__32::<i>5__5
	int32_t ___U3CiU3E5__5_8;
	// HexagonManager GridMapManager_<ProduceHexagon>d__32::<newHex>5__6
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * ___U3CnewHexU3E5__6_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___U3CU3E4__this_2)); }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_columns_3() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___columns_3)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_columns_3() const { return ___columns_3; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_columns_3() { return &___columns_3; }
	inline void set_columns_3(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___columns_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___columns_3), (void*)value);
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___color_4)); }
	inline List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * get_color_4() const { return ___color_4; }
	inline List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 ** get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * value)
	{
		___color_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___color_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CstartPositionU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___U3CstartPositionU3E5__2_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CstartPositionU3E5__2_5() const { return ___U3CstartPositionU3E5__2_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CstartPositionU3E5__2_5() { return &___U3CstartPositionU3E5__2_5; }
	inline void set_U3CstartPositionU3E5__2_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CstartPositionU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartXU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___U3CstartXU3E5__3_6)); }
	inline float get_U3CstartXU3E5__3_6() const { return ___U3CstartXU3E5__3_6; }
	inline float* get_address_of_U3CstartXU3E5__3_6() { return &___U3CstartXU3E5__3_6; }
	inline void set_U3CstartXU3E5__3_6(float value)
	{
		___U3CstartXU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap3_7() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___U3CU3E7__wrap3_7)); }
	inline Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  get_U3CU3E7__wrap3_7() const { return ___U3CU3E7__wrap3_7; }
	inline Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * get_address_of_U3CU3E7__wrap3_7() { return &___U3CU3E7__wrap3_7; }
	inline void set_U3CU3E7__wrap3_7(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  value)
	{
		___U3CU3E7__wrap3_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap3_7))->___list_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CiU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___U3CiU3E5__5_8)); }
	inline int32_t get_U3CiU3E5__5_8() const { return ___U3CiU3E5__5_8; }
	inline int32_t* get_address_of_U3CiU3E5__5_8() { return &___U3CiU3E5__5_8; }
	inline void set_U3CiU3E5__5_8(int32_t value)
	{
		___U3CiU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CnewHexU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C, ___U3CnewHexU3E5__6_9)); }
	inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * get_U3CnewHexU3E5__6_9() const { return ___U3CnewHexU3E5__6_9; }
	inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 ** get_address_of_U3CnewHexU3E5__6_9() { return &___U3CnewHexU3E5__6_9; }
	inline void set_U3CnewHexU3E5__6_9(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * value)
	{
		___U3CnewHexU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnewHexU3E5__6_9), (void*)value);
	}
};


// HexagonManager_NeighbourHexes
struct  NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 
{
public:
	// UnityEngine.Vector2 HexagonManager_NeighbourHexes::up
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___up_0;
	// UnityEngine.Vector2 HexagonManager_NeighbourHexes::upLeft
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upLeft_1;
	// UnityEngine.Vector2 HexagonManager_NeighbourHexes::upRight
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upRight_2;
	// UnityEngine.Vector2 HexagonManager_NeighbourHexes::down
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___down_3;
	// UnityEngine.Vector2 HexagonManager_NeighbourHexes::downLeft
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downLeft_4;
	// UnityEngine.Vector2 HexagonManager_NeighbourHexes::downRight
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downRight_5;

public:
	inline static int32_t get_offset_of_up_0() { return static_cast<int32_t>(offsetof(NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8, ___up_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_up_0() const { return ___up_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_up_0() { return &___up_0; }
	inline void set_up_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___up_0 = value;
	}

	inline static int32_t get_offset_of_upLeft_1() { return static_cast<int32_t>(offsetof(NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8, ___upLeft_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upLeft_1() const { return ___upLeft_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upLeft_1() { return &___upLeft_1; }
	inline void set_upLeft_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upLeft_1 = value;
	}

	inline static int32_t get_offset_of_upRight_2() { return static_cast<int32_t>(offsetof(NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8, ___upRight_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upRight_2() const { return ___upRight_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upRight_2() { return &___upRight_2; }
	inline void set_upRight_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upRight_2 = value;
	}

	inline static int32_t get_offset_of_down_3() { return static_cast<int32_t>(offsetof(NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8, ___down_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_down_3() const { return ___down_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_down_3() { return &___down_3; }
	inline void set_down_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___down_3 = value;
	}

	inline static int32_t get_offset_of_downLeft_4() { return static_cast<int32_t>(offsetof(NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8, ___downLeft_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downLeft_4() const { return ___downLeft_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downLeft_4() { return &___downLeft_4; }
	inline void set_downLeft_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downLeft_4 = value;
	}

	inline static int32_t get_offset_of_downRight_5() { return static_cast<int32_t>(offsetof(NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8, ___downRight_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downRight_5() const { return ___downRight_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downRight_5() { return &___downRight_5; }
	inline void set_downRight_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downRight_5 = value;
	}
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*INT*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*INT*/* ___native_trace_ips_15;
};

// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.TextAlignment
struct  TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD 
{
public:
	// System.Int32 UnityEngine.TextAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TouchPhase
struct  TouchPhase_t7E9CEC3DD059E32F847242513BD6CE30866AB2A6 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_t7E9CEC3DD059E32F847242513BD6CE30866AB2A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TouchType
struct  TouchType_tBBD83025576FC017B10484014B5C396613A02B8E 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_tBBD83025576FC017B10484014B5C396613A02B8E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_SelectedColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};


// UnityEngine.UI.Navigation_Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable_Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Sprite
struct  Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Touch
struct  Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Position_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_RawPosition_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_PositionDelta_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};


// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation_Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};

// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.TextMesh
struct  TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Collider2D
struct  Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct  SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F  : public Renderer_t0556D67DD582620D1F495627EDE30D03284151F4
{
public:

public:
};


// PropertiesClass
struct  PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 PropertiesClass::Hexagon_Outline_Scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Hexagon_Outline_Scale_21;
	// UnityEngine.Vector2 PropertiesClass::Hexagon_Start_Position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Hexagon_Start_Position_22;

public:
	inline static int32_t get_offset_of_Hexagon_Outline_Scale_21() { return static_cast<int32_t>(offsetof(PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63, ___Hexagon_Outline_Scale_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Hexagon_Outline_Scale_21() const { return ___Hexagon_Outline_Scale_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Hexagon_Outline_Scale_21() { return &___Hexagon_Outline_Scale_21; }
	inline void set_Hexagon_Outline_Scale_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Hexagon_Outline_Scale_21 = value;
	}

	inline static int32_t get_offset_of_Hexagon_Start_Position_22() { return static_cast<int32_t>(offsetof(PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63, ___Hexagon_Start_Position_22)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Hexagon_Start_Position_22() const { return ___Hexagon_Start_Position_22; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Hexagon_Start_Position_22() { return &___Hexagon_Start_Position_22; }
	inline void set_Hexagon_Start_Position_22(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Hexagon_Start_Position_22 = value;
	}
};


// StartMenuManager
struct  StartMenuManager_t2908FBD03EFB4C62AB2BEC6BB723A8063C085637  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// GridMapManager
struct  GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773  : public PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63
{
public:
	// UnityEngine.GameObject GridMapManager::hexPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hexPrefab_24;
	// UnityEngine.GameObject GridMapManager::hexParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hexParent_25;
	// UnityEngine.GameObject GridMapManager::outParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___outParent_26;
	// UnityEngine.Sprite GridMapManager::outlineSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___outlineSprite_27;
	// UnityEngine.Sprite GridMapManager::HexagonSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___HexagonSprite_28;
	// System.Int32 GridMapManager::gridWidth
	int32_t ___gridWidth_29;
	// System.Int32 GridMapManager::gridHeight
	int32_t ___gridHeight_30;
	// System.Int32 GridMapManager::selectionStatus
	int32_t ___selectionStatus_31;
	// System.Boolean GridMapManager::bombProduction
	bool ___bombProduction_32;
	// System.Boolean GridMapManager::gameOver
	bool ___gameOver_33;
	// UnityEngine.Vector2 GridMapManager::selectedHexagonPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___selectedHexagonPosition_34;
	// HexagonManager GridMapManager::selectedHexagon
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * ___selectedHexagon_35;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>> GridMapManager::gameGrid
	List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * ___gameGrid_36;
	// System.Collections.Generic.List`1<HexagonManager> GridMapManager::selectedGroup
	List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * ___selectedGroup_37;
	// System.Collections.Generic.List`1<HexagonManager> GridMapManager::bombs
	List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * ___bombs_38;
	// System.Collections.Generic.List`1<UnityEngine.Color> GridMapManager::colorList
	List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * ___colorList_39;
	// System.Boolean GridMapManager::HexagonRotationStatus
	bool ___HexagonRotationStatus_40;
	// System.Boolean GridMapManager::HexagonExplosionStatus
	bool ___HexagonExplosionStatus_41;
	// System.Boolean GridMapManager::HexagonProductionStatus
	bool ___HexagonProductionStatus_42;

public:
	inline static int32_t get_offset_of_hexPrefab_24() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___hexPrefab_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hexPrefab_24() const { return ___hexPrefab_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hexPrefab_24() { return &___hexPrefab_24; }
	inline void set_hexPrefab_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hexPrefab_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hexPrefab_24), (void*)value);
	}

	inline static int32_t get_offset_of_hexParent_25() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___hexParent_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hexParent_25() const { return ___hexParent_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hexParent_25() { return &___hexParent_25; }
	inline void set_hexParent_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hexParent_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hexParent_25), (void*)value);
	}

	inline static int32_t get_offset_of_outParent_26() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___outParent_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_outParent_26() const { return ___outParent_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_outParent_26() { return &___outParent_26; }
	inline void set_outParent_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___outParent_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___outParent_26), (void*)value);
	}

	inline static int32_t get_offset_of_outlineSprite_27() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___outlineSprite_27)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_outlineSprite_27() const { return ___outlineSprite_27; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_outlineSprite_27() { return &___outlineSprite_27; }
	inline void set_outlineSprite_27(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___outlineSprite_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___outlineSprite_27), (void*)value);
	}

	inline static int32_t get_offset_of_HexagonSprite_28() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___HexagonSprite_28)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_HexagonSprite_28() const { return ___HexagonSprite_28; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_HexagonSprite_28() { return &___HexagonSprite_28; }
	inline void set_HexagonSprite_28(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___HexagonSprite_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HexagonSprite_28), (void*)value);
	}

	inline static int32_t get_offset_of_gridWidth_29() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___gridWidth_29)); }
	inline int32_t get_gridWidth_29() const { return ___gridWidth_29; }
	inline int32_t* get_address_of_gridWidth_29() { return &___gridWidth_29; }
	inline void set_gridWidth_29(int32_t value)
	{
		___gridWidth_29 = value;
	}

	inline static int32_t get_offset_of_gridHeight_30() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___gridHeight_30)); }
	inline int32_t get_gridHeight_30() const { return ___gridHeight_30; }
	inline int32_t* get_address_of_gridHeight_30() { return &___gridHeight_30; }
	inline void set_gridHeight_30(int32_t value)
	{
		___gridHeight_30 = value;
	}

	inline static int32_t get_offset_of_selectionStatus_31() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___selectionStatus_31)); }
	inline int32_t get_selectionStatus_31() const { return ___selectionStatus_31; }
	inline int32_t* get_address_of_selectionStatus_31() { return &___selectionStatus_31; }
	inline void set_selectionStatus_31(int32_t value)
	{
		___selectionStatus_31 = value;
	}

	inline static int32_t get_offset_of_bombProduction_32() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___bombProduction_32)); }
	inline bool get_bombProduction_32() const { return ___bombProduction_32; }
	inline bool* get_address_of_bombProduction_32() { return &___bombProduction_32; }
	inline void set_bombProduction_32(bool value)
	{
		___bombProduction_32 = value;
	}

	inline static int32_t get_offset_of_gameOver_33() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___gameOver_33)); }
	inline bool get_gameOver_33() const { return ___gameOver_33; }
	inline bool* get_address_of_gameOver_33() { return &___gameOver_33; }
	inline void set_gameOver_33(bool value)
	{
		___gameOver_33 = value;
	}

	inline static int32_t get_offset_of_selectedHexagonPosition_34() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___selectedHexagonPosition_34)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_selectedHexagonPosition_34() const { return ___selectedHexagonPosition_34; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_selectedHexagonPosition_34() { return &___selectedHexagonPosition_34; }
	inline void set_selectedHexagonPosition_34(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___selectedHexagonPosition_34 = value;
	}

	inline static int32_t get_offset_of_selectedHexagon_35() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___selectedHexagon_35)); }
	inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * get_selectedHexagon_35() const { return ___selectedHexagon_35; }
	inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 ** get_address_of_selectedHexagon_35() { return &___selectedHexagon_35; }
	inline void set_selectedHexagon_35(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * value)
	{
		___selectedHexagon_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedHexagon_35), (void*)value);
	}

	inline static int32_t get_offset_of_gameGrid_36() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___gameGrid_36)); }
	inline List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * get_gameGrid_36() const { return ___gameGrid_36; }
	inline List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C ** get_address_of_gameGrid_36() { return &___gameGrid_36; }
	inline void set_gameGrid_36(List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * value)
	{
		___gameGrid_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameGrid_36), (void*)value);
	}

	inline static int32_t get_offset_of_selectedGroup_37() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___selectedGroup_37)); }
	inline List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * get_selectedGroup_37() const { return ___selectedGroup_37; }
	inline List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 ** get_address_of_selectedGroup_37() { return &___selectedGroup_37; }
	inline void set_selectedGroup_37(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * value)
	{
		___selectedGroup_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedGroup_37), (void*)value);
	}

	inline static int32_t get_offset_of_bombs_38() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___bombs_38)); }
	inline List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * get_bombs_38() const { return ___bombs_38; }
	inline List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 ** get_address_of_bombs_38() { return &___bombs_38; }
	inline void set_bombs_38(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * value)
	{
		___bombs_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bombs_38), (void*)value);
	}

	inline static int32_t get_offset_of_colorList_39() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___colorList_39)); }
	inline List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * get_colorList_39() const { return ___colorList_39; }
	inline List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 ** get_address_of_colorList_39() { return &___colorList_39; }
	inline void set_colorList_39(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * value)
	{
		___colorList_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorList_39), (void*)value);
	}

	inline static int32_t get_offset_of_HexagonRotationStatus_40() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___HexagonRotationStatus_40)); }
	inline bool get_HexagonRotationStatus_40() const { return ___HexagonRotationStatus_40; }
	inline bool* get_address_of_HexagonRotationStatus_40() { return &___HexagonRotationStatus_40; }
	inline void set_HexagonRotationStatus_40(bool value)
	{
		___HexagonRotationStatus_40 = value;
	}

	inline static int32_t get_offset_of_HexagonExplosionStatus_41() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___HexagonExplosionStatus_41)); }
	inline bool get_HexagonExplosionStatus_41() const { return ___HexagonExplosionStatus_41; }
	inline bool* get_address_of_HexagonExplosionStatus_41() { return &___HexagonExplosionStatus_41; }
	inline void set_HexagonExplosionStatus_41(bool value)
	{
		___HexagonExplosionStatus_41 = value;
	}

	inline static int32_t get_offset_of_HexagonProductionStatus_42() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773, ___HexagonProductionStatus_42)); }
	inline bool get_HexagonProductionStatus_42() const { return ___HexagonProductionStatus_42; }
	inline bool* get_address_of_HexagonProductionStatus_42() { return &___HexagonProductionStatus_42; }
	inline void set_HexagonProductionStatus_42(bool value)
	{
		___HexagonProductionStatus_42 = value;
	}
};

struct GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_StaticFields
{
public:
	// GridMapManager GridMapManager::instance
	GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * ___instance_23;

public:
	inline static int32_t get_offset_of_instance_23() { return static_cast<int32_t>(offsetof(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_StaticFields, ___instance_23)); }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * get_instance_23() const { return ___instance_23; }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 ** get_address_of_instance_23() { return &___instance_23; }
	inline void set_instance_23(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * value)
	{
		___instance_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_23), (void*)value);
	}
};


// HexagonManager
struct  HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009  : public PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63
{
public:
	// GridMapManager HexagonManager::GridManagerObject
	GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * ___GridManagerObject_23;
	// System.Int32 HexagonManager::x
	int32_t ___x_24;
	// System.Int32 HexagonManager::y
	int32_t ___y_25;
	// UnityEngine.Color HexagonManager::color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color_26;
	// UnityEngine.Vector2 HexagonManager::Hexposition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Hexposition_27;
	// System.Boolean HexagonManager::lerp
	bool ___lerp_28;
	// UnityEngine.Vector2 HexagonManager::speed
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___speed_29;
	// System.Int32 HexagonManager::bombTimer
	int32_t ___bombTimer_30;
	// UnityEngine.TextMesh HexagonManager::text
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___text_31;

public:
	inline static int32_t get_offset_of_GridManagerObject_23() { return static_cast<int32_t>(offsetof(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009, ___GridManagerObject_23)); }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * get_GridManagerObject_23() const { return ___GridManagerObject_23; }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 ** get_address_of_GridManagerObject_23() { return &___GridManagerObject_23; }
	inline void set_GridManagerObject_23(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * value)
	{
		___GridManagerObject_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GridManagerObject_23), (void*)value);
	}

	inline static int32_t get_offset_of_x_24() { return static_cast<int32_t>(offsetof(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009, ___x_24)); }
	inline int32_t get_x_24() const { return ___x_24; }
	inline int32_t* get_address_of_x_24() { return &___x_24; }
	inline void set_x_24(int32_t value)
	{
		___x_24 = value;
	}

	inline static int32_t get_offset_of_y_25() { return static_cast<int32_t>(offsetof(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009, ___y_25)); }
	inline int32_t get_y_25() const { return ___y_25; }
	inline int32_t* get_address_of_y_25() { return &___y_25; }
	inline void set_y_25(int32_t value)
	{
		___y_25 = value;
	}

	inline static int32_t get_offset_of_color_26() { return static_cast<int32_t>(offsetof(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009, ___color_26)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color_26() const { return ___color_26; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color_26() { return &___color_26; }
	inline void set_color_26(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color_26 = value;
	}

	inline static int32_t get_offset_of_Hexposition_27() { return static_cast<int32_t>(offsetof(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009, ___Hexposition_27)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Hexposition_27() const { return ___Hexposition_27; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Hexposition_27() { return &___Hexposition_27; }
	inline void set_Hexposition_27(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Hexposition_27 = value;
	}

	inline static int32_t get_offset_of_lerp_28() { return static_cast<int32_t>(offsetof(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009, ___lerp_28)); }
	inline bool get_lerp_28() const { return ___lerp_28; }
	inline bool* get_address_of_lerp_28() { return &___lerp_28; }
	inline void set_lerp_28(bool value)
	{
		___lerp_28 = value;
	}

	inline static int32_t get_offset_of_speed_29() { return static_cast<int32_t>(offsetof(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009, ___speed_29)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_speed_29() const { return ___speed_29; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_speed_29() { return &___speed_29; }
	inline void set_speed_29(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___speed_29 = value;
	}

	inline static int32_t get_offset_of_bombTimer_30() { return static_cast<int32_t>(offsetof(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009, ___bombTimer_30)); }
	inline int32_t get_bombTimer_30() const { return ___bombTimer_30; }
	inline int32_t* get_address_of_bombTimer_30() { return &___bombTimer_30; }
	inline void set_bombTimer_30(int32_t value)
	{
		___bombTimer_30 = value;
	}

	inline static int32_t get_offset_of_text_31() { return static_cast<int32_t>(offsetof(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009, ___text_31)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_text_31() const { return ___text_31; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_text_31() { return &___text_31; }
	inline void set_text_31(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___text_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___text_31), (void*)value);
	}
};


// InputGetter
struct  InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD  : public PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63
{
public:
	// System.Boolean InputGetter::isValidTouch
	bool ___isValidTouch_23;
	// GridMapManager InputGetter::GridMapManagerObject
	GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * ___GridMapManagerObject_24;
	// UnityEngine.Vector2 InputGetter::touchStartPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___touchStartPosition_25;
	// HexagonManager InputGetter::selectedHexagon
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * ___selectedHexagon_26;

public:
	inline static int32_t get_offset_of_isValidTouch_23() { return static_cast<int32_t>(offsetof(InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD, ___isValidTouch_23)); }
	inline bool get_isValidTouch_23() const { return ___isValidTouch_23; }
	inline bool* get_address_of_isValidTouch_23() { return &___isValidTouch_23; }
	inline void set_isValidTouch_23(bool value)
	{
		___isValidTouch_23 = value;
	}

	inline static int32_t get_offset_of_GridMapManagerObject_24() { return static_cast<int32_t>(offsetof(InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD, ___GridMapManagerObject_24)); }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * get_GridMapManagerObject_24() const { return ___GridMapManagerObject_24; }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 ** get_address_of_GridMapManagerObject_24() { return &___GridMapManagerObject_24; }
	inline void set_GridMapManagerObject_24(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * value)
	{
		___GridMapManagerObject_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GridMapManagerObject_24), (void*)value);
	}

	inline static int32_t get_offset_of_touchStartPosition_25() { return static_cast<int32_t>(offsetof(InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD, ___touchStartPosition_25)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_touchStartPosition_25() const { return ___touchStartPosition_25; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_touchStartPosition_25() { return &___touchStartPosition_25; }
	inline void set_touchStartPosition_25(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___touchStartPosition_25 = value;
	}

	inline static int32_t get_offset_of_selectedHexagon_26() { return static_cast<int32_t>(offsetof(InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD, ___selectedHexagon_26)); }
	inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * get_selectedHexagon_26() const { return ___selectedHexagon_26; }
	inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 ** get_address_of_selectedHexagon_26() { return &___selectedHexagon_26; }
	inline void set_selectedHexagon_26(HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * value)
	{
		___selectedHexagon_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedHexagon_26), (void*)value);
	}
};


// UIManager
struct  UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C  : public PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63
{
public:
	// UnityEngine.UI.Text UIManager::score
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___score_23;
	// UnityEngine.UI.Text UIManager::score2
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___score2_24;
	// UnityEngine.UI.Dropdown UIManager::widthDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___widthDropdown_25;
	// UnityEngine.UI.Dropdown UIManager::heightDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___heightDropdown_26;
	// UnityEngine.UI.Dropdown UIManager::colorCountDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___colorCountDropdown_27;
	// UnityEngine.GameObject UIManager::settingsScreen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___settingsScreen_28;
	// UnityEngine.Transform UIManager::informationPanel
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___informationPanel_29;
	// UnityEngine.Transform UIManager::gameOverPanel
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___gameOverPanel_30;
	// System.Boolean UIManager::isClick
	bool ___isClick_31;
	// System.Collections.Generic.List`1<UnityEngine.Color> UIManager::colors
	List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * ___colors_32;
	// GridMapManager UIManager::GridManagerObject
	GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * ___GridManagerObject_33;
	// System.Int32 UIManager::colorCount
	int32_t ___colorCount_35;
	// System.Int32 UIManager::removedHexagons
	int32_t ___removedHexagons_36;
	// System.Int32 UIManager::bombCount
	int32_t ___bombCount_37;
	// System.Int32 UIManager::width
	int32_t ___width_38;
	// System.Int32 UIManager::height
	int32_t ___height_39;
	// System.Int32 UIManager::color
	int32_t ___color_40;

public:
	inline static int32_t get_offset_of_score_23() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___score_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_score_23() const { return ___score_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_score_23() { return &___score_23; }
	inline void set_score_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___score_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___score_23), (void*)value);
	}

	inline static int32_t get_offset_of_score2_24() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___score2_24)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_score2_24() const { return ___score2_24; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_score2_24() { return &___score2_24; }
	inline void set_score2_24(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___score2_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___score2_24), (void*)value);
	}

	inline static int32_t get_offset_of_widthDropdown_25() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___widthDropdown_25)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_widthDropdown_25() const { return ___widthDropdown_25; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_widthDropdown_25() { return &___widthDropdown_25; }
	inline void set_widthDropdown_25(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___widthDropdown_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___widthDropdown_25), (void*)value);
	}

	inline static int32_t get_offset_of_heightDropdown_26() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___heightDropdown_26)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_heightDropdown_26() const { return ___heightDropdown_26; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_heightDropdown_26() { return &___heightDropdown_26; }
	inline void set_heightDropdown_26(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___heightDropdown_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___heightDropdown_26), (void*)value);
	}

	inline static int32_t get_offset_of_colorCountDropdown_27() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___colorCountDropdown_27)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_colorCountDropdown_27() const { return ___colorCountDropdown_27; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_colorCountDropdown_27() { return &___colorCountDropdown_27; }
	inline void set_colorCountDropdown_27(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___colorCountDropdown_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorCountDropdown_27), (void*)value);
	}

	inline static int32_t get_offset_of_settingsScreen_28() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___settingsScreen_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_settingsScreen_28() const { return ___settingsScreen_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_settingsScreen_28() { return &___settingsScreen_28; }
	inline void set_settingsScreen_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___settingsScreen_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___settingsScreen_28), (void*)value);
	}

	inline static int32_t get_offset_of_informationPanel_29() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___informationPanel_29)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_informationPanel_29() const { return ___informationPanel_29; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_informationPanel_29() { return &___informationPanel_29; }
	inline void set_informationPanel_29(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___informationPanel_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___informationPanel_29), (void*)value);
	}

	inline static int32_t get_offset_of_gameOverPanel_30() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___gameOverPanel_30)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_gameOverPanel_30() const { return ___gameOverPanel_30; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_gameOverPanel_30() { return &___gameOverPanel_30; }
	inline void set_gameOverPanel_30(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___gameOverPanel_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameOverPanel_30), (void*)value);
	}

	inline static int32_t get_offset_of_isClick_31() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___isClick_31)); }
	inline bool get_isClick_31() const { return ___isClick_31; }
	inline bool* get_address_of_isClick_31() { return &___isClick_31; }
	inline void set_isClick_31(bool value)
	{
		___isClick_31 = value;
	}

	inline static int32_t get_offset_of_colors_32() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___colors_32)); }
	inline List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * get_colors_32() const { return ___colors_32; }
	inline List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 ** get_address_of_colors_32() { return &___colors_32; }
	inline void set_colors_32(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * value)
	{
		___colors_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colors_32), (void*)value);
	}

	inline static int32_t get_offset_of_GridManagerObject_33() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___GridManagerObject_33)); }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * get_GridManagerObject_33() const { return ___GridManagerObject_33; }
	inline GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 ** get_address_of_GridManagerObject_33() { return &___GridManagerObject_33; }
	inline void set_GridManagerObject_33(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * value)
	{
		___GridManagerObject_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GridManagerObject_33), (void*)value);
	}

	inline static int32_t get_offset_of_colorCount_35() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___colorCount_35)); }
	inline int32_t get_colorCount_35() const { return ___colorCount_35; }
	inline int32_t* get_address_of_colorCount_35() { return &___colorCount_35; }
	inline void set_colorCount_35(int32_t value)
	{
		___colorCount_35 = value;
	}

	inline static int32_t get_offset_of_removedHexagons_36() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___removedHexagons_36)); }
	inline int32_t get_removedHexagons_36() const { return ___removedHexagons_36; }
	inline int32_t* get_address_of_removedHexagons_36() { return &___removedHexagons_36; }
	inline void set_removedHexagons_36(int32_t value)
	{
		___removedHexagons_36 = value;
	}

	inline static int32_t get_offset_of_bombCount_37() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___bombCount_37)); }
	inline int32_t get_bombCount_37() const { return ___bombCount_37; }
	inline int32_t* get_address_of_bombCount_37() { return &___bombCount_37; }
	inline void set_bombCount_37(int32_t value)
	{
		___bombCount_37 = value;
	}

	inline static int32_t get_offset_of_width_38() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___width_38)); }
	inline int32_t get_width_38() const { return ___width_38; }
	inline int32_t* get_address_of_width_38() { return &___width_38; }
	inline void set_width_38(int32_t value)
	{
		___width_38 = value;
	}

	inline static int32_t get_offset_of_height_39() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___height_39)); }
	inline int32_t get_height_39() const { return ___height_39; }
	inline int32_t* get_address_of_height_39() { return &___height_39; }
	inline void set_height_39(int32_t value)
	{
		___height_39 = value;
	}

	inline static int32_t get_offset_of_color_40() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___color_40)); }
	inline int32_t get_color_40() const { return ___color_40; }
	inline int32_t* get_address_of_color_40() { return &___color_40; }
	inline void set_color_40(int32_t value)
	{
		___color_40 = value;
	}
};

struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields
{
public:
	// UIManager UIManager::instance
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * ___instance_34;

public:
	inline static int32_t get_offset_of_instance_34() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___instance_34)); }
	inline UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * get_instance_34() const { return ___instance_34; }
	inline UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C ** get_address_of_instance_34() { return &___instance_34; }
	inline void set_instance_34(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * value)
	{
		___instance_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_34), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_11;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_12;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_13;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_14;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_18;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_CachedMesh_21;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_CachedUvs_22;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_23;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_11() const { return ___m_RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_11() { return &___m_RectTransform_11; }
	inline void set_m_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_12)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_12() const { return ___m_CanvasRenderer_12; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_12() { return &___m_CanvasRenderer_12; }
	inline void set_m_CanvasRenderer_12(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_13)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_14)); }
	inline bool get_m_VertsDirty_14() const { return ___m_VertsDirty_14; }
	inline bool* get_address_of_m_VertsDirty_14() { return &___m_VertsDirty_14; }
	inline void set_m_VertsDirty_14(bool value)
	{
		___m_VertsDirty_14 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_15)); }
	inline bool get_m_MaterialDirty_15() const { return ___m_MaterialDirty_15; }
	inline bool* get_address_of_m_MaterialDirty_15() { return &___m_MaterialDirty_15; }
	inline void set_m_MaterialDirty_15(bool value)
	{
		___m_MaterialDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_16() const { return ___m_OnDirtyLayoutCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_16() { return &___m_OnDirtyLayoutCallback_16; }
	inline void set_m_OnDirtyLayoutCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_17)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_17() const { return ___m_OnDirtyVertsCallback_17; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_17() { return &___m_OnDirtyVertsCallback_17; }
	inline void set_m_OnDirtyVertsCallback_17(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_18)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_18() const { return ___m_OnDirtyMaterialCallback_18; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_18() { return &___m_OnDirtyMaterialCallback_18; }
	inline void set_m_OnDirtyMaterialCallback_18(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_21() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedMesh_21)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_CachedMesh_21() const { return ___m_CachedMesh_21; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_CachedMesh_21() { return &___m_CachedMesh_21; }
	inline void set_m_CachedMesh_21(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_CachedMesh_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_22() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedUvs_22)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_CachedUvs_22() const { return ___m_CachedUvs_22; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_CachedUvs_22() { return &___m_CachedUvs_22; }
	inline void set_m_CachedUvs_22(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_CachedUvs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_23() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_23)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_23() const { return ___m_ColorTweenRunner_23; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_23() { return &___m_ColorTweenRunner_23; }
	inline void set_m_ColorTweenRunner_23(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_24(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_24 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_19;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_20;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_19() const { return ___s_Mesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_19() { return &___s_Mesh_19; }
	inline void set_s_Mesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_20)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_20() const { return ___s_VertexHelper_20; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_20() { return &___s_VertexHelper_20; }
	inline void set_s_VertexHelper_20(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_20), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_7;
	// UnityEngine.UI.Selectable_Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Boolean UnityEngine.UI.Selectable::m_WillRemove
	bool ___m_WillRemove_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_7)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_4), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_9)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_10)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_13)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_WillRemove_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_WillRemove_15)); }
	inline bool get_m_WillRemove_15() const { return ___m_WillRemove_15; }
	inline bool* get_address_of_m_WillRemove_15() { return &___m_WillRemove_15; }
	inline void set_m_WillRemove_15(bool value)
	{
		___m_WillRemove_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_19)); }
	inline List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;
	// System.Boolean UnityEngine.UI.Selectable::s_IsDirty
	bool ___s_IsDirty_6;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}

	inline static int32_t get_offset_of_s_IsDirty_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_IsDirty_6)); }
	inline bool get_s_IsDirty_6() const { return ___s_IsDirty_6; }
	inline bool* get_address_of_s_IsDirty_6() { return &___s_IsDirty_6; }
	inline void set_s_IsDirty_6(bool value)
	{
		___s_IsDirty_6 = value;
	}
};


// UnityEngine.UI.Dropdown
struct  Dropdown_tF6331401084B1213CAB10587A6EC81461501930F  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Template_20;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_CaptionText_21;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_CaptionImage_22;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ItemText_23;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_ItemImage_24;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_25;
	// UnityEngine.UI.Dropdown_OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D * ___m_Options_26;
	// UnityEngine.UI.Dropdown_DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306 * ___m_OnValueChanged_27;
	// System.Single UnityEngine.UI.Dropdown::m_AlphaFadeSpeed
	float ___m_AlphaFadeSpeed_28;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_Dropdown_29;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_Blocker_30;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown_DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t836CD930F5F0862929A362435417DA9BCD4186F8 * ___m_Items_31;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * ___m_AlphaTweenRunner_32;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_33;

public:
	inline static int32_t get_offset_of_m_Template_20() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Template_20)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Template_20() const { return ___m_Template_20; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Template_20() { return &___m_Template_20; }
	inline void set_m_Template_20(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Template_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Template_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaptionText_21() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_CaptionText_21)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_CaptionText_21() const { return ___m_CaptionText_21; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_CaptionText_21() { return &___m_CaptionText_21; }
	inline void set_m_CaptionText_21(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_CaptionText_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CaptionText_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_22() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_CaptionImage_22)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_CaptionImage_22() const { return ___m_CaptionImage_22; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_CaptionImage_22() { return &___m_CaptionImage_22; }
	inline void set_m_CaptionImage_22(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_CaptionImage_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CaptionImage_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ItemText_23() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_ItemText_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ItemText_23() const { return ___m_ItemText_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ItemText_23() { return &___m_ItemText_23; }
	inline void set_m_ItemText_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ItemText_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ItemText_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ItemImage_24() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_ItemImage_24)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_ItemImage_24() const { return ___m_ItemImage_24; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_ItemImage_24() { return &___m_ItemImage_24; }
	inline void set_m_ItemImage_24(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_ItemImage_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ItemImage_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_Value_25() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Value_25)); }
	inline int32_t get_m_Value_25() const { return ___m_Value_25; }
	inline int32_t* get_address_of_m_Value_25() { return &___m_Value_25; }
	inline void set_m_Value_25(int32_t value)
	{
		___m_Value_25 = value;
	}

	inline static int32_t get_offset_of_m_Options_26() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Options_26)); }
	inline OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D * get_m_Options_26() const { return ___m_Options_26; }
	inline OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D ** get_address_of_m_Options_26() { return &___m_Options_26; }
	inline void set_m_Options_26(OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D * value)
	{
		___m_Options_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Options_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_27() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_OnValueChanged_27)); }
	inline DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306 * get_m_OnValueChanged_27() const { return ___m_OnValueChanged_27; }
	inline DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306 ** get_address_of_m_OnValueChanged_27() { return &___m_OnValueChanged_27; }
	inline void set_m_OnValueChanged_27(DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306 * value)
	{
		___m_OnValueChanged_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlphaFadeSpeed_28() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_AlphaFadeSpeed_28)); }
	inline float get_m_AlphaFadeSpeed_28() const { return ___m_AlphaFadeSpeed_28; }
	inline float* get_address_of_m_AlphaFadeSpeed_28() { return &___m_AlphaFadeSpeed_28; }
	inline void set_m_AlphaFadeSpeed_28(float value)
	{
		___m_AlphaFadeSpeed_28 = value;
	}

	inline static int32_t get_offset_of_m_Dropdown_29() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Dropdown_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_Dropdown_29() const { return ___m_Dropdown_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_Dropdown_29() { return &___m_Dropdown_29; }
	inline void set_m_Dropdown_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_Dropdown_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Dropdown_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_Blocker_30() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Blocker_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_Blocker_30() const { return ___m_Blocker_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_Blocker_30() { return &___m_Blocker_30; }
	inline void set_m_Blocker_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_Blocker_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Blocker_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_Items_31() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Items_31)); }
	inline List_1_t836CD930F5F0862929A362435417DA9BCD4186F8 * get_m_Items_31() const { return ___m_Items_31; }
	inline List_1_t836CD930F5F0862929A362435417DA9BCD4186F8 ** get_address_of_m_Items_31() { return &___m_Items_31; }
	inline void set_m_Items_31(List_1_t836CD930F5F0862929A362435417DA9BCD4186F8 * value)
	{
		___m_Items_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Items_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_32() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_AlphaTweenRunner_32)); }
	inline TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * get_m_AlphaTweenRunner_32() const { return ___m_AlphaTweenRunner_32; }
	inline TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF ** get_address_of_m_AlphaTweenRunner_32() { return &___m_AlphaTweenRunner_32; }
	inline void set_m_AlphaTweenRunner_32(TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * value)
	{
		___m_AlphaTweenRunner_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AlphaTweenRunner_32), (void*)value);
	}

	inline static int32_t get_offset_of_validTemplate_33() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___validTemplate_33)); }
	inline bool get_validTemplate_33() const { return ___validTemplate_33; }
	inline bool* get_address_of_validTemplate_33() { return &___validTemplate_33; }
	inline void set_validTemplate_33(bool value)
	{
		___validTemplate_33 = value;
	}
};

struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F_StaticFields
{
public:
	// UnityEngine.UI.Dropdown_OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831 * ___s_NoOptionData_34;

public:
	inline static int32_t get_offset_of_s_NoOptionData_34() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F_StaticFields, ___s_NoOptionData_34)); }
	inline OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831 * get_s_NoOptionData_34() const { return ___s_NoOptionData_34; }
	inline OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831 ** get_address_of_s_NoOptionData_34() { return &___s_NoOptionData_34; }
	inline void set_s_NoOptionData_34(OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831 * value)
	{
		___s_NoOptionData_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_NoOptionData_34), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_25;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_26;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_27;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_29;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_31;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_32;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_33;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_25)); }
	inline bool get_m_ShouldRecalculateStencil_25() const { return ___m_ShouldRecalculateStencil_25; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_25() { return &___m_ShouldRecalculateStencil_25; }
	inline void set_m_ShouldRecalculateStencil_25(bool value)
	{
		___m_ShouldRecalculateStencil_25 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_26() const { return ___m_MaskMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_26() { return &___m_MaskMaterial_26; }
	inline void set_m_MaskMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_27)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_27() const { return ___m_ParentMask_27; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_27() { return &___m_ParentMask_27; }
	inline void set_m_ParentMask_27(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_28)); }
	inline bool get_m_Maskable_28() const { return ___m_Maskable_28; }
	inline bool* get_address_of_m_Maskable_28() { return &___m_Maskable_28; }
	inline void set_m_Maskable_28(bool value)
	{
		___m_Maskable_28 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_29)); }
	inline bool get_m_IncludeForMasking_29() const { return ___m_IncludeForMasking_29; }
	inline bool* get_address_of_m_IncludeForMasking_29() { return &___m_IncludeForMasking_29; }
	inline void set_m_IncludeForMasking_29(bool value)
	{
		___m_IncludeForMasking_29 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_30)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_30() const { return ___m_OnCullStateChanged_30; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_30() { return &___m_OnCullStateChanged_30; }
	inline void set_m_OnCullStateChanged_30(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_31)); }
	inline bool get_m_ShouldRecalculate_31() const { return ___m_ShouldRecalculate_31; }
	inline bool* get_address_of_m_ShouldRecalculate_31() { return &___m_ShouldRecalculate_31; }
	inline void set_m_ShouldRecalculate_31(bool value)
	{
		___m_ShouldRecalculate_31 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_32)); }
	inline int32_t get_m_StencilValue_32() const { return ___m_StencilValue_32; }
	inline int32_t* get_address_of_m_StencilValue_32() { return &___m_StencilValue_32; }
	inline void set_m_StencilValue_32(int32_t value)
	{
		___m_StencilValue_32 = value;
	}

	inline static int32_t get_offset_of_m_Corners_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_33)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_33() const { return ___m_Corners_33; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_33() { return &___m_Corners_33; }
	inline void set_m_Corners_33(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_33), (void*)value);
	}
};


// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_34;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_35;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_36;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_37;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_39;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_40;

public:
	inline static int32_t get_offset_of_m_FontData_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_34)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_34() const { return ___m_FontData_34; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_34() { return &___m_FontData_34; }
	inline void set_m_FontData_34(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_35)); }
	inline String_t* get_m_Text_35() const { return ___m_Text_35; }
	inline String_t** get_address_of_m_Text_35() { return &___m_Text_35; }
	inline void set_m_Text_35(String_t* value)
	{
		___m_Text_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_36)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_36() const { return ___m_TextCache_36; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_36() { return &___m_TextCache_36; }
	inline void set_m_TextCache_36(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_37() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_37)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_37() const { return ___m_TextCacheForLayout_37; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_37() { return &___m_TextCacheForLayout_37; }
	inline void set_m_TextCacheForLayout_37(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_39() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_39)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_39() const { return ___m_DisableFontTextureRebuiltCallback_39; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_39() { return &___m_DisableFontTextureRebuiltCallback_39; }
	inline void set_m_DisableFontTextureRebuiltCallback_39(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_39 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_40() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_40)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_40() const { return ___m_TempVerts_40; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_40() { return &___m_TempVerts_40; }
	inline void set_m_TempVerts_40(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_40), (void*)value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_38;

public:
	inline static int32_t get_offset_of_s_DefaultText_38() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_38)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_38() const { return ___s_DefaultText_38; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_38() { return &___s_DefaultText_38; }
	inline void set_s_DefaultText_38(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_38), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  m_Items[1];

public:
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_set_Item_m451452782977192583A6374A799099FCCD9BD83E_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_mE08D561E86879A26245096C572A8593279383FDB_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  List_1_GetEnumerator_m83178F038A7D4A7E9B0731B7D3078EDCF6FFD0EC_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_m88A0089A1A4EEBC3017E2DA569A01C7919B10945_gshared_inline (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m113E33A615748C69D63D1245F5FD820B4B3D43F7_gshared (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mBA3B0129DABD8274AF3497CC93E6A2DEA0A23892_gshared (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E_gshared (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_gshared_inline (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_gshared_inline (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_gshared (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::set_Item(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_set_Item_m99A8C26E8C19A07648E04317DD42457C40A68A99_gshared (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, int32_t ___index0, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mDDC28234680BE7088AC6F12DC1C7617E26360C85_gshared (RuntimeObject * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent3, const RuntimeMethod* method);

// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<HexagonManager>::.ctor()
inline void List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318 (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>>::.ctor()
inline void List_1__ctor_m5F0329865A129AC95740F9057A8E346327D312A8 (List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4 (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
inline void List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771 (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, int32_t, const RuntimeMethod*))List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771_gshared)(__this, ___item0, method);
}
// System.Int32 GridMapManager::GetGridHeight()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t GridMapManager_GetGridHeight_m76516C97F13FF377E0E91F5A48EA948867E5C41B_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>>::Add(!0)
inline void List_1_Add_m2DF903991E5929EB07031535A5577397CA88ECA6 (List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * __this, List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C *, List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Int32 GridMapManager::GetGridWidth()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t GridMapManager_GetGridWidth_m4DF42E736F4B0984D7E727901ACF84405DD895BA_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>> GridMapManager::ColoredGridProduction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * GridMapManager_ColoredGridProduction_m26846DC1A6559F858A1DDC079E04CB70A04344B2 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator GridMapManager::ProduceHexagon(System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GridMapManager_ProduceHexagon_m214E1A2F495919DEC056EA874C97E901CC3F4621 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___columns0, List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * ___color1, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider2D>()
inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * Component_GetComponent_TisCollider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_m2AF5C85C582A45B02A4E940C7E8B19A065A37405 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<HexagonManager>()
inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * GameObject_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5417F5D6065EA0BDB12492F51D50F061AA28FAF4 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.Int32 HexagonManager::GetX()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method);
// System.Int32 HexagonManager::GetY()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t HexagonManager_GetY_m3B711DA0B2F3DAA2347CFE9AD3D7AD597DCB8B2D_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method);
// System.Void GridMapManager::DestroyOutline()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_DestroyOutline_m9874F92E892AB57F87DE99F9740C6770F1A27385 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Void GridMapManager::CreateOutline()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_CreateOutline_m1308C33D309E14FAE44B40A754A2033952625BD2 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator GridMapManager::RotationCheckingCoroutine(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GridMapManager_RotationCheckingCoroutine_mE145CE6BD7A171A4693E2DF826A37D829D61F3A1 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, bool ___clockWise0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>>::get_Item(System.Int32)
inline List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline (List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * (*) (List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// !0 System.Collections.Generic.List`1<HexagonManager>::get_Item(System.Int32)
inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * (*) (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// HexagonManager/NeighbourHexes HexagonManager::GetNeighbours()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  HexagonManager_GetNeighbours_m12298ED576B60FB0F5DF30613D7E508CA6149E12 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<HexagonManager>::Clear()
inline void List_1_Clear_m91E44F05F6B99F382B086DBDE40A2EBE95B30F2D (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, const RuntimeMethod*))List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<HexagonManager>::Add(!0)
inline void List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5 (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * __this, HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// !!0 UnityEngine.Component::GetComponent<HexagonManager>()
inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * Component_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5B95C6AF8FD437069099C69F7FDB9A4C5FAC6F4B (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared)(__this, method);
}
// System.Void GridMapManager/<RotationCheckingCoroutine>d__26::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRotationCheckingCoroutineU3Ed__26__ctor_m7A58025DEA96C9E7384F73E935327F43A258C283 (U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v0, const RuntimeMethod* method);
// System.Void HexagonManager::Rotate(System.Int32,System.Int32,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, int32_t ___newX0, int32_t ___newY1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___newPos2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<HexagonManager>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * __this, int32_t ___index0, HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, int32_t, HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 *, const RuntimeMethod*))List_1_set_Item_m451452782977192583A6374A799099FCCD9BD83E_gshared)(__this, ___index0, ___value1, method);
}
// UnityEngine.Color HexagonManager::GetColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  HexagonManager_GetColor_mED456B54AEF3D88B662951813B71E6D833F046D0 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method);
// System.Boolean GridMapManager::IsValid(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GridMapManager_IsValid_m9B4B23F940F3FE203303CD85ACBC41F4596B8093 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pos0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Color_op_Equality_m71B1A2F64AD6228F10E20149EF6440460D2C748E (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___lhs0, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___rhs1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<HexagonManager>::Contains(!0)
inline bool List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506 (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * __this, HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 *, const RuntimeMethod*))List_1_Contains_mE08D561E86879A26245096C572A8593279383FDB_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<HexagonManager>::get_Count()
inline int32_t List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>>::get_Count()
inline int32_t List_1_get_Count_mCB1A1A5FF01B45922A9B49BD843FE64B2118479F_inline (List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<HexagonManager>::GetEnumerator()
inline Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578  List_1_GetEnumerator_m0D768CC6350116CA5EE94FFE68CD7AD10C81242B (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578  (*) (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<HexagonManager>::get_Current()
inline HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * Enumerator_get_Current_m6DEA264C3D3852D1E3485CE0DFABE349D2C8FD6F_inline (Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 * __this, const RuntimeMethod* method)
{
	return ((  HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * (*) (Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline)(__this, method);
}
// System.Void HexagonManager::Trigger()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_Trigger_mEDA1CF56DE33D03BC1E8F0146550DFAEEED9890B (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method);
// System.Int32 HexagonManager::GetTimer()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t HexagonManager_GetTimer_mEE88C69BB650E3740B5B30D59302877ED000E980_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method);
// System.Void UIManager::GameEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_GameEnd_mA5D7B91BBE6BDDBC191E2860910EB1A927F8E6EB (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_StopAllCoroutines_mA5469BB7BBB59B8A94BB86590B051E0DFACC12DD (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<HexagonManager>::MoveNext()
inline bool Enumerator_MoveNext_m45D437EE4A29B63712A1B64D39D2809F3594A0DE (Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HexagonManager>::Dispose()
inline void Enumerator_Dispose_m992731D0C6BC506250880A1095BCFCFEED22350F (Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1<HexagonManager>::Remove(!0)
inline bool List_1_Remove_m6E540121DD0B257A950F046285B96E436394CACE (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * __this, HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *, HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 *, const RuntimeMethod*))List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared)(__this, ___item0, method);
}
// System.Void UIManager::Score(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_Score_m88AA8E3F6C04A7C3C11EF47F2A8D65E72EFEF63D (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, int32_t ____Score0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
inline Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  List_1_GetEnumerator_m83178F038A7D4A7E9B0731B7D3078EDCF6FFD0EC (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))List_1_GetEnumerator_m83178F038A7D4A7E9B0731B7D3078EDCF6FFD0EC_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
inline int32_t Enumerator_get_Current_m88A0089A1A4EEBC3017E2DA569A01C7919B10945_inline (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C *, const RuntimeMethod*))Enumerator_get_Current_m88A0089A1A4EEBC3017E2DA569A01C7919B10945_gshared_inline)(__this, method);
}
// System.Single GridMapManager::GetGridStartCoordinateX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GridMapManager_GetGridStartCoordinateX_m039E8599B60121EAB5A5460E2DC44C36FA82C59D (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Boolean GridMapManager::DetectColumn(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GridMapManager_DetectColumn_m10378C564143B7BCC0F3945603BF7499BE472534 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, int32_t ___x0, const RuntimeMethod* method);
// System.Void HexagonManager::SetY(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HexagonManager_SetY_m67E6A395342D87F427D4EE382A82AD1D7CAF0CFD_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void HexagonManager::SetX(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HexagonManager_SetX_m479990D055BBF89BDF258489087EFC8AD084CC0C_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void HexagonManager::ChangeWorldPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_ChangeWorldPosition_m4AFE1537B92653C2C4C7EE6A35C5FA01469AFE1C (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___newPosition0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
inline bool Enumerator_MoveNext_m113E33A615748C69D63D1245F5FD820B4B3D43F7 (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C *, const RuntimeMethod*))Enumerator_MoveNext_m113E33A615748C69D63D1245F5FD820B4B3D43F7_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
inline void Enumerator_Dispose_mBA3B0129DABD8274AF3497CC93E6A2DEA0A23892 (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C *, const RuntimeMethod*))Enumerator_Dispose_mBA3B0129DABD8274AF3497CC93E6A2DEA0A23892_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_m7665D779DCDB6B175FB52A254276CDF0C384A724 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void GridMapManager::FindHexagonGroup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_FindHexagonGroup_mE0BE80BC83F5FBC6186AA7FC2B2E16ACA87BCA82 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * GameObject_AddComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_m034B7C9CB50717611871DEBE6CB07147F506A8E9 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_sprite_m9F5C8B2007AA03FAB66F0CB61260349DF1E28611 (SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * __this, Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___value0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905 (const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580 (SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  SpriteRenderer_get_color_m1456AB27D5B09F28A273EC0BBD4F03A0FDA51E99 (SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void GridMapManager/<ProduceHexagon>d__32::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProduceHexagonU3Ed__32__ctor_m7C3798865DC945DA4EACAAA760443EAFBD8033C8 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>>::.ctor()
inline void List_1__ctor_mB36E5A3444519179409E72DA2E6AE580BD1962B0 (List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
inline void List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 *, const RuntimeMethod*))List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>>::Add(!0)
inline void List_1_Add_m2005A4FE9473352474F6255670AA4F580BDB1CC5 (List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * __this, List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 *, List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// !0 System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>>::get_Item(System.Int32)
inline List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_inline (List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * (*) (List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Single UnityEngine.Random::get_value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_get_value_mC998749E08291DD42CF31C026FAC4F14F746831C (const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::get_Count()
inline int32_t List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_inline (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 *, const RuntimeMethod*))List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_inline (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  (*) (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 *, int32_t, const RuntimeMethod*))List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
inline void List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 *, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 , const RuntimeMethod*))List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m99A8C26E8C19A07648E04317DD42457C40A68A99 (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, int32_t ___index0, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value1, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 *, int32_t, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 , const RuntimeMethod*))List_1_set_Item_m99A8C26E8C19A07648E04317DD42457C40A68A99_gshared)(__this, ___index0, ___value1, method);
}
// System.Void PropertiesClass::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE (PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void GridMapManager/<ProduceHexagon>d__32::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProduceHexagonU3Ed__32_U3CU3Em__Finally1_m6D2CEC25E49CF58E9CF395DD0B1951967D004B63 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m6895A7A231540279E01A537649EB42814FD2671B (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent3, const RuntimeMethod* method)
{
	return ((  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 , Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mDDC28234680BE7088AC6F12DC1C7617E26360C85_gshared)(___original0, ___position1, ___rotation2, ___parent3, method);
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559 (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Void HexagonManager::SetBomb()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_SetBomb_mA40C2AB6C12399179D44B1FD7B64BA47E53C5533 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method);
// System.Void HexagonManager::SetColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_SetColor_mAFE672E70F49F0041DB82EEF7396ADBFFFB12016 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___newColor0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void HexagonManager::ChangeGridPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_ChangeGridPosition_m31C3E0BA897DF72C9A6A28640C0ED63E692BF44F (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___newPosition0, const RuntimeMethod* method);
// System.Void GridMapManager/<ProduceHexagon>d__32::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProduceHexagonU3Ed__32_System_IDisposable_Dispose_m9B3606C41110AC2C771F556F19878B6BA3DFA5F4 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// System.Void GridMapManager::SwapHexagon(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_SwapHexagon_mC8A4398C219C587EFAB4490F0C24FAC7216E2B27 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, bool ___clockWise0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<HexagonManager> GridMapManager::CheckingExplosion(System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * GridMapManager_CheckingExplosion_m5FBB713BB06BCF9D906D7A401DA316494322B5C1 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * ___checkingList0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.Int32> GridMapManager::DetectBombANDClearExplodeHexagon(System.Collections.Generic.List`1<HexagonManager>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * GridMapManager_DetectBombANDClearExplodeHexagon_m6A4071F050BF7576CA67F92580349C93711471EF (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * ___list0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.TextMesh>()
inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * GameObject_AddComponent_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF57CA692C5FFBA2C6599F6FEEA08E0F9050C368A (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared)(__this, method);
}
// System.Void UnityEngine.TextMesh::set_alignment(UnityEngine.TextAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextMesh_set_alignment_mC79810263A381B2CD7ECAB76D4399E18D928F82E (TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.TextMesh::set_anchor(UnityEngine.TextAnchor)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextMesh_set_anchor_m013CFCFA46AB8478ADD1C4818FAAD90596BF4E15 (TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * __this, int32_t ___value0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_black_mEB3C91F45F8AA7E4842238DFCC578BB322723DAF (const RuntimeMethod* method);
// System.Void UnityEngine.TextMesh::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextMesh_set_color_mF86B9E8CD0F9FD387AF7D543337B5C14DFE67AF0 (TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value0, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02 (int32_t* __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextMesh::set_text(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220 (TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * __this, String_t* ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared)(__this, method);
}
// System.Boolean GridMapManager::InputAvailable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GridMapManager_InputAvailable_m2DFC0C65CF2EE1843ED10B7DF610A40CF109F5DA (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Input::get_touchCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Input_get_touchCount_m497E19AA4FA22DB659F631B20FAEF65572D1B44E (const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (const RuntimeMethod* method);
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  Input_GetTouch_m8082D8EE3A187488373CE6AC66A70B0AAD7CC23F (int32_t ___index0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F (Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method);
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * Physics2D_OverlapPoint_m9DDCF583FA2141D3DC89CF00393DF00806E72FC2 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point0, const RuntimeMethod* method);
// HexagonManager GridMapManager::GetSelectedHexagon()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * GridMapManager_GetSelectedHexagon_m82E2459ADA9669F14C94801054CBB147501B7A4C_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Void InputGetter::DetectingTouch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputGetter_DetectingTouch_m245D4ED56A833ABB012042EAECE74957B6D22DEA (InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD * __this, const RuntimeMethod* method);
// System.Void InputGetter::CheckingSelection(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputGetter_CheckingSelection_m21269E14235F6F39DB2DABD55896D41F561CFBBC (InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD * __this, Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___collider0, const RuntimeMethod* method);
// System.Void InputGetter::CheckingRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputGetter_CheckingRotation_m7CA3205BA425261E6AF8B8F14EE2A6C108AA516E (InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD * __this, const RuntimeMethod* method);
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Touch_get_phase_m759A61477ECBBD90A57E36F1166EB9340A0FE349 (Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Component::get_tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Component_get_tag_mA183075586ED6BFA81D303804359AE6B02C477CC (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void GridMapManager::SelectHexagon(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_SelectHexagon_mC0DB6D9C605D20DCC436F24D10DDB411040CB9B6 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___collider0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Camera_WorldToScreenPoint_m880F9611E4848C11F21FDF1A1D307B401C61B1BF (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method);
// System.Void GridMapManager::RotateHexagons(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_RotateHexagons_mAD1E1D587794465A4189001F3FB4E15685182BF8 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, bool ___clockWise0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9 (String_t* ___sceneName0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95 (const RuntimeMethod* method);
// System.Void UIManager::DefaultSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_DefaultSettings_m1E07491312E42FF549B5E429DFB3CDCD025E01A9 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UIManager::StartGameButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_StartGameButton_m81242AD25701DFE9D8D263296534705B5750DD09 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.UI.Dropdown::get_value()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline (Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// System.Void GridMapManager::SetGridHeight(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void GridMapManager_SetGridHeight_m89865CCB85B689CA751046765A238E10A794F509_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, int32_t ___height0, const RuntimeMethod* method);
// System.Void GridMapManager::SetGridWidth(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void GridMapManager_SetGridWidth_mCDC775F41010E50C72D4C68A0643CF7E0D3B904B_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, int32_t ___width0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957 (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_yellow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_yellow_mC8BD62CCC364EA5FC4273D4C2E116D0E2DE135AE (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_green()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0 (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_magenta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_magenta_m04E2DDB63AA6288C701A93E248643A06EBD2D7AD (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_blue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_blue_m5449DCBB31EEB2324489989754C00123982EBABA (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_cyan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_cyan_m4E9C84C7E1003311C2D4BDB281F2D11DF5F7FDE2 (const RuntimeMethod* method);
// System.Void GridMapManager::SetColorList(System.Collections.Generic.List`1<UnityEngine.Color>)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void GridMapManager_SetColorList_mBF555D15D608B2AE94B83811BDFC9DF798FF7651_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * ___list0, const RuntimeMethod* method);
// System.Void GridMapManager::CreateGrid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_CreateGrid_mFFF3421D90C288DB3CA1F87BA5A87AB56FBED4B9 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Dropdown::set_value(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dropdown_set_value_m155A45649AB62AC1B7AB10213EA556F22E8E91F3 (Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 System.Int32::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA (String_t* ___s0, const RuntimeMethod* method);
// System.Void GridMapManager::SetBombProduction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_SetBombProduction_mC86F578E5457E53A98856BB567B5EA081BED1BE2 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GridMapManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_Awake_m6E8D86D8EFC58038823B4F476086538F8ED331F9 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_Awake_m6E8D86D8EFC58038823B4F476086538F8ED331F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_0 = ((GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_StaticFields*)il2cpp_codegen_static_fields_for(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var))->get_instance_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var);
		((GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_StaticFields*)il2cpp_codegen_static_fields_for(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var))->set_instance_23(__this);
		return;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GridMapManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_Start_mC2C2F8C9E10A63B36392EA87E6C0E4412751F1CA (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_Start_mC2C2F8C9E10A63B36392EA87E6C0E4412751F1CA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_gameOver_33((bool)0);
		__this->set_bombProduction_32((bool)0);
		__this->set_HexagonRotationStatus_40((bool)0);
		__this->set_HexagonExplosionStatus_41((bool)0);
		__this->set_HexagonProductionStatus_42((bool)0);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_0 = (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *)il2cpp_codegen_object_new(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21_il2cpp_TypeInfo_var);
		List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318(L_0, /*hidden argument*/List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318_RuntimeMethod_var);
		__this->set_bombs_38(L_0);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_1 = (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *)il2cpp_codegen_object_new(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21_il2cpp_TypeInfo_var);
		List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318(L_1, /*hidden argument*/List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318_RuntimeMethod_var);
		__this->set_selectedGroup_37(L_1);
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_2 = (List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C *)il2cpp_codegen_object_new(List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C_il2cpp_TypeInfo_var);
		List_1__ctor_m5F0329865A129AC95740F9057A8E346327D312A8(L_2, /*hidden argument*/List_1__ctor_m5F0329865A129AC95740F9057A8E346327D312A8_RuntimeMethod_var);
		__this->set_gameGrid_36(L_2);
		return;
	}
}
// System.Void GridMapManager::CreateGrid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_CreateGrid_mFFF3421D90C288DB3CA1F87BA5A87AB56FBED4B9 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_CreateGrid_mFFF3421D90C288DB3CA1F87BA5A87AB56FBED4B9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_0 = (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)il2cpp_codegen_object_new(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_il2cpp_TypeInfo_var);
		List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4(L_0, /*hidden argument*/List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_RuntimeMethod_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0036;
	}

IL_000a:
	{
		V_2 = 0;
		goto IL_0019;
	}

IL_000e:
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771(L_1, L_2, /*hidden argument*/List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771_RuntimeMethod_var);
		int32_t L_3 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_0019:
	{
		int32_t L_4 = V_2;
		int32_t L_5 = GridMapManager_GetGridHeight_m76516C97F13FF377E0E91F5A48EA948867E5C41B_inline(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_000e;
		}
	}
	{
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_6 = __this->get_gameGrid_36();
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_7 = (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *)il2cpp_codegen_object_new(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21_il2cpp_TypeInfo_var);
		List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318(L_7, /*hidden argument*/List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318_RuntimeMethod_var);
		NullCheck(L_6);
		List_1_Add_m2DF903991E5929EB07031535A5577397CA88ECA6(L_6, L_7, /*hidden argument*/List_1_Add_m2DF903991E5929EB07031535A5577397CA88ECA6_RuntimeMethod_var);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = GridMapManager_GetGridWidth_m4DF42E736F4B0984D7E727901ACF84405DD895BA_inline(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_000a;
		}
	}
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_11 = V_0;
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_12 = GridMapManager_ColoredGridProduction_m26846DC1A6559F858A1DDC079E04CB70A04344B2(__this, /*hidden argument*/NULL);
		RuntimeObject* L_13 = GridMapManager_ProduceHexagon_m214E1A2F495919DEC056EA874C97E901CC3F4621(__this, L_11, L_12, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GridMapManager::SelectHexagon(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_SelectHexagon_mC0DB6D9C605D20DCC436F24D10DDB411040CB9B6 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_SelectHexagon_mC0DB6D9C605D20DCC436F24D10DDB411040CB9B6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_0 = __this->get_selectedHexagon_35();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_2 = __this->get_selectedHexagon_35();
		NullCheck(L_2);
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_3 = Component_GetComponent_TisCollider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_m2AF5C85C582A45B02A4E940C7E8B19A065A37405(L_2, /*hidden argument*/Component_GetComponent_TisCollider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_m2AF5C85C582A45B02A4E940C7E8B19A065A37405_RuntimeMethod_var);
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_4 = ___collider0;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_3, L_4);
		if (L_5)
		{
			goto IL_0069;
		}
	}

IL_0021:
	{
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_6 = ___collider0;
		NullCheck(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_8 = GameObject_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5417F5D6065EA0BDB12492F51D50F061AA28FAF4(L_7, /*hidden argument*/GameObject_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5417F5D6065EA0BDB12492F51D50F061AA28FAF4_RuntimeMethod_var);
		__this->set_selectedHexagon_35(L_8);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_9 = __this->get_address_of_selectedHexagonPosition_34();
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_10 = __this->get_selectedHexagon_35();
		NullCheck(L_10);
		int32_t L_11 = HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48_inline(L_10, /*hidden argument*/NULL);
		L_9->set_x_0((((float)((float)L_11))));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_12 = __this->get_address_of_selectedHexagonPosition_34();
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_13 = __this->get_selectedHexagon_35();
		NullCheck(L_13);
		int32_t L_14 = HexagonManager_GetY_m3B711DA0B2F3DAA2347CFE9AD3D7AD597DCB8B2D_inline(L_13, /*hidden argument*/NULL);
		L_12->set_y_1((((float)((float)L_14))));
		__this->set_selectionStatus_31(0);
		goto IL_0082;
	}

IL_0069:
	{
		int32_t L_15 = __this->get_selectionStatus_31();
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		int32_t L_16 = V_0;
		__this->set_selectionStatus_31(L_16);
		int32_t L_17 = V_0;
		__this->set_selectionStatus_31(((int32_t)((int32_t)L_17%(int32_t)6)));
	}

IL_0082:
	{
		GridMapManager_DestroyOutline_m9874F92E892AB57F87DE99F9740C6770F1A27385(__this, /*hidden argument*/NULL);
		GridMapManager_CreateOutline_m1308C33D309E14FAE44B40A754A2033952625BD2(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GridMapManager::RotateHexagons(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_RotateHexagons_mAD1E1D587794465A4189001F3FB4E15685182BF8 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, bool ___clockWise0, const RuntimeMethod* method)
{
	{
		GridMapManager_DestroyOutline_m9874F92E892AB57F87DE99F9740C6770F1A27385(__this, /*hidden argument*/NULL);
		bool L_0 = ___clockWise0;
		RuntimeObject* L_1 = GridMapManager_RotationCheckingCoroutine_mE145CE6BD7A171A4693E2DF826A37D829D61F3A1(__this, L_0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GridMapManager::FindHexagonGroup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_FindHexagonGroup_mE0BE80BC83F5FBC6186AA7FC2B2E16ACA87BCA82 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_FindHexagonGroup_mE0BE80BC83F5FBC6186AA7FC2B2E16ACA87BCA82_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  V_2;
	memset((&V_2), 0, sizeof(V_2));
	bool V_3 = false;
	int32_t V_4 = 0;
	{
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_0 = __this->get_gameGrid_36();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_1 = __this->get_address_of_selectedHexagonPosition_34();
		float L_2 = L_1->get_x_0();
		NullCheck(L_0);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_3 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_0, (((int32_t)((int32_t)L_2))), /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_4 = __this->get_address_of_selectedHexagonPosition_34();
		float L_5 = L_4->get_y_1();
		NullCheck(L_3);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_6 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_3, (((int32_t)((int32_t)L_5))), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		__this->set_selectedHexagon_35(L_6);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_7 = __this->get_selectedHexagon_35();
		NullCheck(L_7);
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_8 = HexagonManager_GetNeighbours_m12298ED576B60FB0F5DF30613D7E508CA6149E12(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		V_3 = (bool)0;
	}

IL_003c:
	{
		int32_t L_9 = __this->get_selectionStatus_31();
		V_4 = L_9;
		int32_t L_10 = V_4;
		switch (L_10)
		{
			case 0:
			{
				goto IL_0065;
			}
			case 1:
			{
				goto IL_0075;
			}
			case 2:
			{
				goto IL_0085;
			}
			case 3:
			{
				goto IL_0095;
			}
			case 4:
			{
				goto IL_00a5;
			}
			case 5:
			{
				goto IL_00b5;
			}
		}
	}
	{
		goto IL_00c5;
	}

IL_0065:
	{
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_11 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = L_11.get_up_0();
		V_0 = L_12;
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_13 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_14 = L_13.get_upRight_2();
		V_1 = L_14;
		goto IL_00d1;
	}

IL_0075:
	{
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_15 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16 = L_15.get_upRight_2();
		V_0 = L_16;
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_17 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_18 = L_17.get_downRight_5();
		V_1 = L_18;
		goto IL_00d1;
	}

IL_0085:
	{
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_19 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_20 = L_19.get_downRight_5();
		V_0 = L_20;
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_21 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_22 = L_21.get_down_3();
		V_1 = L_22;
		goto IL_00d1;
	}

IL_0095:
	{
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_23 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_24 = L_23.get_down_3();
		V_0 = L_24;
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_25 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_26 = L_25.get_downLeft_4();
		V_1 = L_26;
		goto IL_00d1;
	}

IL_00a5:
	{
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_27 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_28 = L_27.get_downLeft_4();
		V_0 = L_28;
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_29 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_30 = L_29.get_upLeft_1();
		V_1 = L_30;
		goto IL_00d1;
	}

IL_00b5:
	{
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_31 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_32 = L_31.get_upLeft_1();
		V_0 = L_32;
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_33 = V_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_34 = L_33.get_up_0();
		V_1 = L_34;
		goto IL_00d1;
	}

IL_00c5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_35 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		V_0 = L_35;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		V_1 = L_36;
	}

IL_00d1:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_37 = V_0;
		float L_38 = L_37.get_x_0();
		if ((((float)L_38) < ((float)(0.0f))))
		{
			goto IL_0141;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_39 = V_0;
		float L_40 = L_39.get_x_0();
		int32_t L_41 = __this->get_gridWidth_29();
		if ((((float)L_40) >= ((float)(((float)((float)L_41))))))
		{
			goto IL_0141;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_42 = V_0;
		float L_43 = L_42.get_y_1();
		if ((((float)L_43) < ((float)(0.0f))))
		{
			goto IL_0141;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_44 = V_0;
		float L_45 = L_44.get_y_1();
		int32_t L_46 = __this->get_gridHeight_30();
		if ((((float)L_45) >= ((float)(((float)((float)L_46))))))
		{
			goto IL_0141;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_47 = V_1;
		float L_48 = L_47.get_x_0();
		if ((((float)L_48) < ((float)(0.0f))))
		{
			goto IL_0141;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_49 = V_1;
		float L_50 = L_49.get_x_0();
		int32_t L_51 = __this->get_gridWidth_29();
		if ((((float)L_50) >= ((float)(((float)((float)L_51))))))
		{
			goto IL_0141;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_52 = V_1;
		float L_53 = L_52.get_y_1();
		if ((((float)L_53) < ((float)(0.0f))))
		{
			goto IL_0141;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_54 = V_1;
		float L_55 = L_54.get_y_1();
		int32_t L_56 = __this->get_gridHeight_30();
		if ((!(((float)L_55) >= ((float)(((float)((float)L_56)))))))
		{
			goto IL_015f;
		}
	}

IL_0141:
	{
		int32_t L_57 = __this->get_selectionStatus_31();
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)1));
		int32_t L_58 = V_4;
		__this->set_selectionStatus_31(L_58);
		int32_t L_59 = V_4;
		__this->set_selectionStatus_31(((int32_t)((int32_t)L_59%(int32_t)6)));
		goto IL_0161;
	}

IL_015f:
	{
		V_3 = (bool)1;
	}

IL_0161:
	{
		bool L_60 = V_3;
		if (!L_60)
		{
			goto IL_003c;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_61 = __this->get_selectedGroup_37();
		NullCheck(L_61);
		List_1_Clear_m91E44F05F6B99F382B086DBDE40A2EBE95B30F2D(L_61, /*hidden argument*/List_1_Clear_m91E44F05F6B99F382B086DBDE40A2EBE95B30F2D_RuntimeMethod_var);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_62 = __this->get_selectedGroup_37();
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_63 = __this->get_selectedHexagon_35();
		NullCheck(L_62);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_62, L_63, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_64 = __this->get_selectedGroup_37();
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_65 = __this->get_gameGrid_36();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_66 = V_0;
		float L_67 = L_66.get_x_0();
		NullCheck(L_65);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_68 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_65, (((int32_t)((int32_t)L_67))), /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_69 = V_0;
		float L_70 = L_69.get_y_1();
		NullCheck(L_68);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_71 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_68, (((int32_t)((int32_t)L_70))), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_71);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_72 = Component_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5B95C6AF8FD437069099C69F7FDB9A4C5FAC6F4B(L_71, /*hidden argument*/Component_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5B95C6AF8FD437069099C69F7FDB9A4C5FAC6F4B_RuntimeMethod_var);
		NullCheck(L_64);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_64, L_72, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_73 = __this->get_selectedGroup_37();
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_74 = __this->get_gameGrid_36();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_75 = V_1;
		float L_76 = L_75.get_x_0();
		NullCheck(L_74);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_77 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_74, (((int32_t)((int32_t)L_76))), /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_78 = V_1;
		float L_79 = L_78.get_y_1();
		NullCheck(L_77);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_80 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_77, (((int32_t)((int32_t)L_79))), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_80);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_81 = Component_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5B95C6AF8FD437069099C69F7FDB9A4C5FAC6F4B(L_80, /*hidden argument*/Component_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5B95C6AF8FD437069099C69F7FDB9A4C5FAC6F4B_RuntimeMethod_var);
		NullCheck(L_73);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_73, L_81, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
		return;
	}
}
// System.Collections.IEnumerator GridMapManager::RotationCheckingCoroutine(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GridMapManager_RotationCheckingCoroutine_mE145CE6BD7A171A4693E2DF826A37D829D61F3A1 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, bool ___clockWise0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_RotationCheckingCoroutine_mE145CE6BD7A171A4693E2DF826A37D829D61F3A1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * L_0 = (U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 *)il2cpp_codegen_object_new(U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9_il2cpp_TypeInfo_var);
		U3CRotationCheckingCoroutineU3Ed__26__ctor_m7A58025DEA96C9E7384F73E935327F43A258C283(L_0, 0, /*hidden argument*/NULL);
		U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * L_2 = L_1;
		bool L_3 = ___clockWise0;
		NullCheck(L_2);
		L_2->set_clockWise_3(L_3);
		return L_2;
	}
}
// System.Void GridMapManager::SwapHexagon(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_SwapHexagon_mC8A4398C219C587EFAB4490F0C24FAC7216E2B27 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, bool ___clockWise0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_SwapHexagon_mC8A4398C219C587EFAB4490F0C24FAC7216E2B27_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_8;
	memset((&V_8), 0, sizeof(V_8));
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * V_9 = NULL;
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * V_10 = NULL;
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * V_11 = NULL;
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_0 = __this->get_selectedGroup_37();
		NullCheck(L_0);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_1 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_0, 0, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		V_9 = L_1;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_2 = __this->get_selectedGroup_37();
		NullCheck(L_2);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_3 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_2, 1, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		V_10 = L_3;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_4 = __this->get_selectedGroup_37();
		NullCheck(L_4);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_5 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_4, 2, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		V_11 = L_5;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_6 = V_9;
		NullCheck(L_6);
		int32_t L_7 = HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48_inline(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_8 = V_10;
		NullCheck(L_8);
		int32_t L_9 = HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48_inline(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_10 = V_11;
		NullCheck(L_10);
		int32_t L_11 = HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48_inline(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_12 = V_9;
		NullCheck(L_12);
		int32_t L_13 = HexagonManager_GetY_m3B711DA0B2F3DAA2347CFE9AD3D7AD597DCB8B2D_inline(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_14 = V_10;
		NullCheck(L_14);
		int32_t L_15 = HexagonManager_GetY_m3B711DA0B2F3DAA2347CFE9AD3D7AD597DCB8B2D_inline(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_16 = V_11;
		NullCheck(L_16);
		int32_t L_17 = HexagonManager_GetY_m3B711DA0B2F3DAA2347CFE9AD3D7AD597DCB8B2D_inline(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_18 = V_9;
		NullCheck(L_18);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_21 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_22 = V_10;
		NullCheck(L_22);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_23 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_23, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_25 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_24, /*hidden argument*/NULL);
		V_7 = L_25;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_26 = V_11;
		NullCheck(L_26);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_27 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_27, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_29 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_28, /*hidden argument*/NULL);
		V_8 = L_29;
		bool L_30 = ___clockWise0;
		if (!L_30)
		{
			goto IL_00fa;
		}
	}
	{
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_31 = V_9;
		int32_t L_32 = V_1;
		int32_t L_33 = V_4;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_34 = V_7;
		NullCheck(L_31);
		HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC(L_31, L_32, L_33, L_34, /*hidden argument*/NULL);
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_35 = __this->get_gameGrid_36();
		int32_t L_36 = V_1;
		NullCheck(L_35);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_37 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_35, L_36, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		int32_t L_38 = V_4;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_39 = V_9;
		NullCheck(L_37);
		List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE(L_37, L_38, L_39, /*hidden argument*/List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE_RuntimeMethod_var);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_40 = V_10;
		int32_t L_41 = V_2;
		int32_t L_42 = V_5;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_43 = V_8;
		NullCheck(L_40);
		HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC(L_40, L_41, L_42, L_43, /*hidden argument*/NULL);
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_44 = __this->get_gameGrid_36();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_46 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_44, L_45, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		int32_t L_47 = V_5;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_48 = V_10;
		NullCheck(L_46);
		List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE(L_46, L_47, L_48, /*hidden argument*/List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE_RuntimeMethod_var);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_49 = V_11;
		int32_t L_50 = V_0;
		int32_t L_51 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_52 = V_6;
		NullCheck(L_49);
		HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC(L_49, L_50, L_51, L_52, /*hidden argument*/NULL);
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_53 = __this->get_gameGrid_36();
		int32_t L_54 = V_0;
		NullCheck(L_53);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_55 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_53, L_54, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		int32_t L_56 = V_3;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_57 = V_11;
		NullCheck(L_55);
		List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE(L_55, L_56, L_57, /*hidden argument*/List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE_RuntimeMethod_var);
		return;
	}

IL_00fa:
	{
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_58 = V_9;
		int32_t L_59 = V_2;
		int32_t L_60 = V_5;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_61 = V_8;
		NullCheck(L_58);
		HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC(L_58, L_59, L_60, L_61, /*hidden argument*/NULL);
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_62 = __this->get_gameGrid_36();
		int32_t L_63 = V_2;
		NullCheck(L_62);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_64 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_62, L_63, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		int32_t L_65 = V_5;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_66 = V_9;
		NullCheck(L_64);
		List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE(L_64, L_65, L_66, /*hidden argument*/List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE_RuntimeMethod_var);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_67 = V_10;
		int32_t L_68 = V_0;
		int32_t L_69 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_70 = V_6;
		NullCheck(L_67);
		HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC(L_67, L_68, L_69, L_70, /*hidden argument*/NULL);
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_71 = __this->get_gameGrid_36();
		int32_t L_72 = V_0;
		NullCheck(L_71);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_73 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_71, L_72, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		int32_t L_74 = V_3;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_75 = V_10;
		NullCheck(L_73);
		List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE(L_73, L_74, L_75, /*hidden argument*/List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE_RuntimeMethod_var);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_76 = V_11;
		int32_t L_77 = V_1;
		int32_t L_78 = V_4;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_79 = V_7;
		NullCheck(L_76);
		HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC(L_76, L_77, L_78, L_79, /*hidden argument*/NULL);
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_80 = __this->get_gameGrid_36();
		int32_t L_81 = V_1;
		NullCheck(L_80);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_82 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_80, L_81, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		int32_t L_83 = V_4;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_84 = V_11;
		NullCheck(L_82);
		List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE(L_82, L_83, L_84, /*hidden argument*/List_1_set_Item_m59F9C0DFB61461F7931CBB00358ACD301DCDD2EE_RuntimeMethod_var);
		return;
	}
}
// System.Collections.Generic.List`1<HexagonManager> GridMapManager::CheckingExplosion(System.Collections.Generic.List`1<System.Collections.Generic.List`1<HexagonManager>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * GridMapManager_CheckingExplosion_m5FBB713BB06BCF9D906D7A401DA316494322B5C1 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * ___checkingList0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_CheckingExplosion_m5FBB713BB06BCF9D906D7A401DA316494322B5C1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * V_0 = NULL;
	List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * V_1 = NULL;
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * V_2 = NULL;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  V_4;
	memset((&V_4), 0, sizeof(V_4));
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_0 = (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *)il2cpp_codegen_object_new(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21_il2cpp_TypeInfo_var);
		List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318(L_0, /*hidden argument*/List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_1 = (List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *)il2cpp_codegen_object_new(List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21_il2cpp_TypeInfo_var);
		List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318(L_1, /*hidden argument*/List_1__ctor_mE6D0B55D0FA4591B386DAED62284FD75D0829318_RuntimeMethod_var);
		V_1 = L_1;
		V_5 = 0;
		goto IL_01f0;
	}

IL_0014:
	{
		V_6 = 0;
		goto IL_01d6;
	}

IL_001c:
	{
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_2 = ___checkingList0;
		int32_t L_3 = V_5;
		NullCheck(L_2);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_4 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_2, L_3, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		int32_t L_5 = V_6;
		NullCheck(L_4);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_6 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		V_2 = L_6;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_7 = V_2;
		NullCheck(L_7);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_8 = HexagonManager_GetColor_mED456B54AEF3D88B662951813B71E6D833F046D0(L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_9 = V_2;
		NullCheck(L_9);
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_10 = HexagonManager_GetNeighbours_m12298ED576B60FB0F5DF30613D7E508CA6149E12(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_11 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = L_11.get_up_0();
		bool L_13 = GridMapManager_IsValid_m9B4B23F940F3FE203303CD85ACBC41F4596B8093(__this, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0079;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_14 = V_0;
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_15 = __this->get_gameGrid_36();
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_16 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_17 = L_16.get_up_0();
		float L_18 = L_17.get_x_0();
		NullCheck(L_15);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_19 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_15, (((int32_t)((int32_t)L_18))), /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_20 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_21 = L_20.get_up_0();
		float L_22 = L_21.get_y_1();
		NullCheck(L_19);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_23 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_19, (((int32_t)((int32_t)L_22))), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_14);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_14, L_23, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
		goto IL_0080;
	}

IL_0079:
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_24 = V_0;
		NullCheck(L_24);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_24, (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 *)NULL, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
	}

IL_0080:
	{
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_25 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_26 = L_25.get_upRight_2();
		bool L_27 = GridMapManager_IsValid_m9B4B23F940F3FE203303CD85ACBC41F4596B8093(__this, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00be;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_28 = V_0;
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_29 = __this->get_gameGrid_36();
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_30 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_31 = L_30.get_upRight_2();
		float L_32 = L_31.get_x_0();
		NullCheck(L_29);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_33 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_29, (((int32_t)((int32_t)L_32))), /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_34 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_35 = L_34.get_upRight_2();
		float L_36 = L_35.get_y_1();
		NullCheck(L_33);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_37 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_33, (((int32_t)((int32_t)L_36))), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_28);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_28, L_37, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
		goto IL_00c5;
	}

IL_00be:
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_38 = V_0;
		NullCheck(L_38);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_38, (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 *)NULL, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
	}

IL_00c5:
	{
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_39 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_40 = L_39.get_downRight_5();
		bool L_41 = GridMapManager_IsValid_m9B4B23F940F3FE203303CD85ACBC41F4596B8093(__this, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0103;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_42 = V_0;
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_43 = __this->get_gameGrid_36();
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_44 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_45 = L_44.get_downRight_5();
		float L_46 = L_45.get_x_0();
		NullCheck(L_43);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_47 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_43, (((int32_t)((int32_t)L_46))), /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_48 = V_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_49 = L_48.get_downRight_5();
		float L_50 = L_49.get_y_1();
		NullCheck(L_47);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_51 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_47, (((int32_t)((int32_t)L_50))), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_42);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_42, L_51, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
		goto IL_010a;
	}

IL_0103:
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_52 = V_0;
		NullCheck(L_52);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_52, (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 *)NULL, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
	}

IL_010a:
	{
		V_7 = 0;
		goto IL_01bb;
	}

IL_0112:
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_53 = V_0;
		int32_t L_54 = V_7;
		NullCheck(L_53);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_55 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_53, L_54, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_56 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_55, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_01b5;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_57 = V_0;
		int32_t L_58 = V_7;
		NullCheck(L_57);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_59 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_57, ((int32_t)il2cpp_codegen_add((int32_t)L_58, (int32_t)1)), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_60 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_59, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_01b5;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_61 = V_0;
		int32_t L_62 = V_7;
		NullCheck(L_61);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_63 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_61, L_62, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_63);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_64 = HexagonManager_GetColor_mED456B54AEF3D88B662951813B71E6D833F046D0(L_63, /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_65 = V_4;
		bool L_66 = Color_op_Equality_m71B1A2F64AD6228F10E20149EF6440460D2C748E(L_64, L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01b5;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_67 = V_0;
		int32_t L_68 = V_7;
		NullCheck(L_67);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_69 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_67, ((int32_t)il2cpp_codegen_add((int32_t)L_68, (int32_t)1)), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_69);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_70 = HexagonManager_GetColor_mED456B54AEF3D88B662951813B71E6D833F046D0(L_69, /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_71 = V_4;
		bool L_72 = Color_op_Equality_m71B1A2F64AD6228F10E20149EF6440460D2C748E(L_70, L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_01b5;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_73 = V_1;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_74 = V_0;
		int32_t L_75 = V_7;
		NullCheck(L_74);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_76 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_74, L_75, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_73);
		bool L_77 = List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506(L_73, L_76, /*hidden argument*/List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506_RuntimeMethod_var);
		if (L_77)
		{
			goto IL_0183;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_78 = V_1;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_79 = V_0;
		int32_t L_80 = V_7;
		NullCheck(L_79);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_81 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_79, L_80, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_78);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_78, L_81, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
	}

IL_0183:
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_82 = V_1;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_83 = V_0;
		int32_t L_84 = V_7;
		NullCheck(L_83);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_85 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_83, ((int32_t)il2cpp_codegen_add((int32_t)L_84, (int32_t)1)), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_82);
		bool L_86 = List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506(L_82, L_85, /*hidden argument*/List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506_RuntimeMethod_var);
		if (L_86)
		{
			goto IL_01a5;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_87 = V_1;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_88 = V_0;
		int32_t L_89 = V_7;
		NullCheck(L_88);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_90 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_88, ((int32_t)il2cpp_codegen_add((int32_t)L_89, (int32_t)1)), /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
		NullCheck(L_87);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_87, L_90, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
	}

IL_01a5:
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_91 = V_1;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_92 = V_2;
		NullCheck(L_91);
		bool L_93 = List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506(L_91, L_92, /*hidden argument*/List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506_RuntimeMethod_var);
		if (L_93)
		{
			goto IL_01b5;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_94 = V_1;
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_95 = V_2;
		NullCheck(L_94);
		List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_94, L_95, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
	}

IL_01b5:
	{
		int32_t L_96 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_96, (int32_t)1));
	}

IL_01bb:
	{
		int32_t L_97 = V_7;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_98 = V_0;
		NullCheck(L_98);
		int32_t L_99 = List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline(L_98, /*hidden argument*/List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var);
		if ((((int32_t)L_97) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_99, (int32_t)1)))))
		{
			goto IL_0112;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_100 = V_0;
		NullCheck(L_100);
		List_1_Clear_m91E44F05F6B99F382B086DBDE40A2EBE95B30F2D(L_100, /*hidden argument*/List_1_Clear_m91E44F05F6B99F382B086DBDE40A2EBE95B30F2D_RuntimeMethod_var);
		int32_t L_101 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_101, (int32_t)1));
	}

IL_01d6:
	{
		int32_t L_102 = V_6;
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_103 = ___checkingList0;
		int32_t L_104 = V_5;
		NullCheck(L_103);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_105 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_103, L_104, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
		NullCheck(L_105);
		int32_t L_106 = List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline(L_105, /*hidden argument*/List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var);
		if ((((int32_t)L_102) < ((int32_t)L_106)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_107 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_107, (int32_t)1));
	}

IL_01f0:
	{
		int32_t L_108 = V_5;
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_109 = ___checkingList0;
		NullCheck(L_109);
		int32_t L_110 = List_1_get_Count_mCB1A1A5FF01B45922A9B49BD843FE64B2118479F_inline(L_109, /*hidden argument*/List_1_get_Count_mCB1A1A5FF01B45922A9B49BD843FE64B2118479F_RuntimeMethod_var);
		if ((((int32_t)L_108) < ((int32_t)L_110)))
		{
			goto IL_0014;
		}
	}
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_111 = V_1;
		return L_111;
	}
}
// System.Collections.Generic.List`1<System.Int32> GridMapManager::DetectBombANDClearExplodeHexagon(System.Collections.Generic.List`1<HexagonManager>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * GridMapManager_DetectBombANDClearExplodeHexagon_m6A4071F050BF7576CA67F92580349C93711471EF (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * ___list0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_DetectBombANDClearExplodeHexagon_m6A4071F050BF7576CA67F92580349C93711471EF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578  V_3;
	memset((&V_3), 0, sizeof(V_3));
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * V_4 = NULL;
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * V_5 = NULL;
	HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * V_6 = NULL;
	Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  V_7;
	memset((&V_7), 0, sizeof(V_7));
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 4);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	float G_B21_0 = 0.0f;
	float G_B20_0 = 0.0f;
	float G_B22_0 = 0.0f;
	float G_B22_1 = 0.0f;
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_0 = (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)il2cpp_codegen_object_new(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_il2cpp_TypeInfo_var);
		List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4(L_0, /*hidden argument*/List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_1 = __this->get_bombs_38();
		NullCheck(L_1);
		Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578  L_2 = List_1_GetEnumerator_m0D768CC6350116CA5EE94FFE68CD7AD10C81242B(L_1, /*hidden argument*/List_1_GetEnumerator_m0D768CC6350116CA5EE94FFE68CD7AD10C81242B_RuntimeMethod_var);
		V_3 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0056;
		}

IL_0014:
		{
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_3 = Enumerator_get_Current_m6DEA264C3D3852D1E3485CE0DFABE349D2C8FD6F_inline((Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m6DEA264C3D3852D1E3485CE0DFABE349D2C8FD6F_RuntimeMethod_var);
			V_4 = L_3;
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_4 = ___list0;
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_5 = V_4;
			NullCheck(L_4);
			bool L_6 = List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506(L_4, L_5, /*hidden argument*/List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506_RuntimeMethod_var);
			if (L_6)
			{
				goto IL_0056;
			}
		}

IL_0027:
		{
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_7 = V_4;
			NullCheck(L_7);
			HexagonManager_Trigger_mEDA1CF56DE33D03BC1E8F0146550DFAEEED9890B(L_7, /*hidden argument*/NULL);
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_8 = V_4;
			NullCheck(L_8);
			int32_t L_9 = HexagonManager_GetTimer_mEE88C69BB650E3740B5B30D59302877ED000E980_inline(L_8, /*hidden argument*/NULL);
			if (L_9)
			{
				goto IL_0056;
			}
		}

IL_0037:
		{
			__this->set_gameOver_33((bool)1);
			UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * L_10 = ((UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields*)il2cpp_codegen_static_fields_for(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var))->get_instance_34();
			NullCheck(L_10);
			UIManager_GameEnd_mA5D7B91BBE6BDDBC191E2860910EB1A927F8E6EB(L_10, /*hidden argument*/NULL);
			MonoBehaviour_StopAllCoroutines_mA5469BB7BBB59B8A94BB86590B051E0DFACC12DD(__this, /*hidden argument*/NULL);
			List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_11 = V_0;
			V_5 = L_11;
			IL2CPP_LEAVE(0x1F5, FINALLY_0061);
		}

IL_0056:
		{
			bool L_12 = Enumerator_MoveNext_m45D437EE4A29B63712A1B64D39D2809F3594A0DE((Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m45D437EE4A29B63712A1B64D39D2809F3594A0DE_RuntimeMethod_var);
			if (L_12)
			{
				goto IL_0014;
			}
		}

IL_005f:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0061);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0061;
	}

FINALLY_0061:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m992731D0C6BC506250880A1095BCFCFEED22350F((Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *)(&V_3), /*hidden argument*/Enumerator_Dispose_m992731D0C6BC506250880A1095BCFCFEED22350F_RuntimeMethod_var);
		IL2CPP_END_FINALLY(97)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(97)
	{
		IL2CPP_JUMP_TBL(0x1F5, IL_01f5)
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006f:
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_13 = ___list0;
		NullCheck(L_13);
		Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578  L_14 = List_1_GetEnumerator_m0D768CC6350116CA5EE94FFE68CD7AD10C81242B(L_13, /*hidden argument*/List_1_GetEnumerator_m0D768CC6350116CA5EE94FFE68CD7AD10C81242B_RuntimeMethod_var);
		V_3 = L_14;
	}

IL_0076:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00dc;
		}

IL_0078:
		{
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_15 = Enumerator_get_Current_m6DEA264C3D3852D1E3485CE0DFABE349D2C8FD6F_inline((Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m6DEA264C3D3852D1E3485CE0DFABE349D2C8FD6F_RuntimeMethod_var);
			V_6 = L_15;
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_16 = __this->get_bombs_38();
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_17 = V_6;
			NullCheck(L_16);
			bool L_18 = List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506(L_16, L_17, /*hidden argument*/List_1_Contains_m79AB446145C2FA6F011DC84D36B2A4D44D13E506_RuntimeMethod_var);
			if (!L_18)
			{
				goto IL_009e;
			}
		}

IL_0090:
		{
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_19 = __this->get_bombs_38();
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_20 = V_6;
			NullCheck(L_19);
			List_1_Remove_m6E540121DD0B257A950F046285B96E436394CACE(L_19, L_20, /*hidden argument*/List_1_Remove_m6E540121DD0B257A950F046285B96E436394CACE_RuntimeMethod_var);
		}

IL_009e:
		{
			UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * L_21 = ((UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields*)il2cpp_codegen_static_fields_for(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var))->get_instance_34();
			NullCheck(L_21);
			UIManager_Score_m88AA8E3F6C04A7C3C11EF47F2A8D65E72EFEF63D(L_21, 1, /*hidden argument*/NULL);
			List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_22 = __this->get_gameGrid_36();
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_23 = V_6;
			NullCheck(L_23);
			int32_t L_24 = HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48_inline(L_23, /*hidden argument*/NULL);
			NullCheck(L_22);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_25 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_22, L_24, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_26 = V_6;
			NullCheck(L_25);
			List_1_Remove_m6E540121DD0B257A950F046285B96E436394CACE(L_25, L_26, /*hidden argument*/List_1_Remove_m6E540121DD0B257A950F046285B96E436394CACE_RuntimeMethod_var);
			List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_27 = V_0;
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_28 = V_6;
			NullCheck(L_28);
			int32_t L_29 = HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48_inline(L_28, /*hidden argument*/NULL);
			NullCheck(L_27);
			List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771(L_27, L_29, /*hidden argument*/List_1_Add_m50C0D1F69B2EF31137658E2F052EBBAC7BF82771_RuntimeMethod_var);
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_30 = V_6;
			NullCheck(L_30);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_31 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_30, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_31, /*hidden argument*/NULL);
		}

IL_00dc:
		{
			bool L_32 = Enumerator_MoveNext_m45D437EE4A29B63712A1B64D39D2809F3594A0DE((Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m45D437EE4A29B63712A1B64D39D2809F3594A0DE_RuntimeMethod_var);
			if (L_32)
			{
				goto IL_0078;
			}
		}

IL_00e5:
		{
			IL2CPP_LEAVE(0xF5, FINALLY_00e7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00e7;
	}

FINALLY_00e7:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m992731D0C6BC506250880A1095BCFCFEED22350F((Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *)(&V_3), /*hidden argument*/Enumerator_Dispose_m992731D0C6BC506250880A1095BCFCFEED22350F_RuntimeMethod_var);
		IL2CPP_END_FINALLY(231)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(231)
	{
		IL2CPP_JUMP_TBL(0xF5, IL_00f5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00f5:
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_33 = V_0;
		NullCheck(L_33);
		Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  L_34 = List_1_GetEnumerator_m83178F038A7D4A7E9B0731B7D3078EDCF6FFD0EC(L_33, /*hidden argument*/List_1_GetEnumerator_m83178F038A7D4A7E9B0731B7D3078EDCF6FFD0EC_RuntimeMethod_var);
		V_7 = L_34;
	}

IL_00fd:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01d0;
		}

IL_0102:
		{
			int32_t L_35 = Enumerator_get_Current_m88A0089A1A4EEBC3017E2DA569A01C7919B10945_inline((Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C *)(&V_7), /*hidden argument*/Enumerator_get_Current_m88A0089A1A4EEBC3017E2DA569A01C7919B10945_RuntimeMethod_var);
			V_8 = L_35;
			V_9 = 0;
			goto IL_01b7;
		}

IL_0113:
		{
			float L_36 = GridMapManager_GetGridStartCoordinateX_m039E8599B60121EAB5A5460E2DC44C36FA82C59D(__this, /*hidden argument*/NULL);
			int32_t L_37 = V_8;
			V_1 = ((float)il2cpp_codegen_add((float)L_36, (float)((float)il2cpp_codegen_multiply((float)(0.445f), (float)(((float)((float)L_37)))))));
			int32_t L_38 = V_9;
			int32_t L_39 = V_8;
			bool L_40 = GridMapManager_DetectColumn_m10378C564143B7BCC0F3945603BF7499BE472534(__this, L_39, /*hidden argument*/NULL);
			G_B20_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(0.23f), (float)(((float)((float)L_38))))), (float)(2.0f))), (float)(-3.0f)));
			if (L_40)
			{
				G_B21_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(0.23f), (float)(((float)((float)L_38))))), (float)(2.0f))), (float)(-3.0f)));
				goto IL_014a;
			}
		}

IL_0143:
		{
			G_B22_0 = (0.0f);
			G_B22_1 = G_B20_0;
			goto IL_014f;
		}

IL_014a:
		{
			G_B22_0 = (0.23f);
			G_B22_1 = G_B21_0;
		}

IL_014f:
		{
			V_2 = ((float)il2cpp_codegen_add((float)G_B22_1, (float)G_B22_0));
			List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_41 = __this->get_gameGrid_36();
			int32_t L_42 = V_8;
			NullCheck(L_41);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_43 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_41, L_42, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
			int32_t L_44 = V_9;
			NullCheck(L_43);
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_45 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_43, L_44, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
			int32_t L_46 = V_9;
			NullCheck(L_45);
			HexagonManager_SetY_m67E6A395342D87F427D4EE382A82AD1D7CAF0CFD_inline(L_45, L_46, /*hidden argument*/NULL);
			List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_47 = __this->get_gameGrid_36();
			int32_t L_48 = V_8;
			NullCheck(L_47);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_49 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_47, L_48, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
			int32_t L_50 = V_9;
			NullCheck(L_49);
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_51 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_49, L_50, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
			int32_t L_52 = V_8;
			NullCheck(L_51);
			HexagonManager_SetX_m479990D055BBF89BDF258489087EFC8AD084CC0C_inline(L_51, L_52, /*hidden argument*/NULL);
			List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_53 = __this->get_gameGrid_36();
			int32_t L_54 = V_8;
			NullCheck(L_53);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_55 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_53, L_54, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
			int32_t L_56 = V_9;
			NullCheck(L_55);
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_57 = List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_inline(L_55, L_56, /*hidden argument*/List_1_get_Item_mA17B80B5C87D7F426158AC2C45E52EA633116ABF_RuntimeMethod_var);
			float L_58 = V_1;
			float L_59 = V_2;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60;
			memset((&L_60), 0, sizeof(L_60));
			Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_60), L_58, L_59, (0.0f), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
			Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_61 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_60, /*hidden argument*/NULL);
			NullCheck(L_57);
			HexagonManager_ChangeWorldPosition_m4AFE1537B92653C2C4C7EE6A35C5FA01469AFE1C(L_57, L_61, /*hidden argument*/NULL);
			int32_t L_62 = V_9;
			V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_62, (int32_t)1));
		}

IL_01b7:
		{
			int32_t L_63 = V_9;
			List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_64 = __this->get_gameGrid_36();
			int32_t L_65 = V_8;
			NullCheck(L_64);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_66 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_64, L_65, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
			NullCheck(L_66);
			int32_t L_67 = List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline(L_66, /*hidden argument*/List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var);
			if ((((int32_t)L_63) < ((int32_t)L_67)))
			{
				goto IL_0113;
			}
		}

IL_01d0:
		{
			bool L_68 = Enumerator_MoveNext_m113E33A615748C69D63D1245F5FD820B4B3D43F7((Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C *)(&V_7), /*hidden argument*/Enumerator_MoveNext_m113E33A615748C69D63D1245F5FD820B4B3D43F7_RuntimeMethod_var);
			if (L_68)
			{
				goto IL_0102;
			}
		}

IL_01dc:
		{
			IL2CPP_LEAVE(0x1EC, FINALLY_01de);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01de;
	}

FINALLY_01de:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mBA3B0129DABD8274AF3497CC93E6A2DEA0A23892((Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C *)(&V_7), /*hidden argument*/Enumerator_Dispose_mBA3B0129DABD8274AF3497CC93E6A2DEA0A23892_RuntimeMethod_var);
		IL2CPP_END_FINALLY(478)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(478)
	{
		IL2CPP_JUMP_TBL(0x1EC, IL_01ec)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01ec:
	{
		__this->set_HexagonExplosionStatus_41((bool)0);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_69 = V_0;
		return L_69;
	}

IL_01f5:
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_70 = V_5;
		return L_70;
	}
}
// System.Void GridMapManager::DestroyOutline()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_DestroyOutline_m9874F92E892AB57F87DE99F9740C6770F1A27385 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_DestroyOutline_m9874F92E892AB57F87DE99F9740C6770F1A27385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_outParent_26();
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Transform_get_childCount_m7665D779DCDB6B175FB52A254276CDF0C384A724(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get_outParent_26();
		NullCheck(L_3);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject* L_5 = Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0024:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003b;
		}

IL_0026:
		{
			RuntimeObject* L_6 = V_0;
			NullCheck(L_6);
			RuntimeObject * L_7 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_6);
			NullCheck(((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)CastclassClass((RuntimeObject*)L_7, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var)));
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)CastclassClass((RuntimeObject*)L_7, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_8, /*hidden argument*/NULL);
		}

IL_003b:
		{
			RuntimeObject* L_9 = V_0;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0026;
			}
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x56, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_11 = V_0;
			V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_11, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
			RuntimeObject* L_12 = V_1;
			if (!L_12)
			{
				goto IL_0055;
			}
		}

IL_004f:
		{
			RuntimeObject* L_13 = V_1;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_13);
		}

IL_0055:
		{
			IL2CPP_END_FINALLY(69)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0056:
	{
		return;
	}
}
// System.Void GridMapManager::CreateOutline()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_CreateOutline_m1308C33D309E14FAE44B40A754A2033952625BD2 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_CreateOutline_m1308C33D309E14FAE44B40A754A2033952625BD2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578  V_0;
	memset((&V_0), 0, sizeof(V_0));
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_1 = NULL;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		GridMapManager_FindHexagonGroup_mE0BE80BC83F5FBC6186AA7FC2B2E16ACA87BCA82(__this, /*hidden argument*/NULL);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_0 = __this->get_selectedGroup_37();
		NullCheck(L_0);
		Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578  L_1 = List_1_GetEnumerator_m0D768CC6350116CA5EE94FFE68CD7AD10C81242B(L_0, /*hidden argument*/List_1_GetEnumerator_m0D768CC6350116CA5EE94FFE68CD7AD10C81242B_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0146;
		}

IL_0017:
		{
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_2 = Enumerator_get_Current_m6DEA264C3D3852D1E3485CE0DFABE349D2C8FD6F_inline((Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m6DEA264C3D3852D1E3485CE0DFABE349D2C8FD6F_RuntimeMethod_var);
			NullCheck(L_2);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_2, /*hidden argument*/NULL);
			V_1 = L_3;
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
			GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_4, _stringLiteralCDD6290B6B2860663A3FF7C60CBE70AF640E9C39, /*hidden argument*/NULL);
			V_2 = L_4;
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
			GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_5, _stringLiteral08596D60C57FD87E530CBDD2A06052B06D8C4137, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = V_2;
			NullCheck(L_6);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_6, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = __this->get_outParent_26();
			NullCheck(L_8);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_8, /*hidden argument*/NULL);
			NullCheck(L_7);
			Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_7, L_9, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = V_2;
			NullCheck(L_10);
			GameObject_AddComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_m034B7C9CB50717611871DEBE6CB07147F506A8E9(L_10, /*hidden argument*/GameObject_AddComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_m034B7C9CB50717611871DEBE6CB07147F506A8E9_RuntimeMethod_var);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = V_2;
			NullCheck(L_11);
			SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_12 = GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538(L_11, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538_RuntimeMethod_var);
			Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_13 = __this->get_outlineSprite_27();
			NullCheck(L_12);
			SpriteRenderer_set_sprite_m9F5C8B2007AA03FAB66F0CB61260349DF1E28611(L_12, L_13, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = V_2;
			NullCheck(L_14);
			SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_15 = GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538(L_14, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538_RuntimeMethod_var);
			Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_16 = Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905(/*hidden argument*/NULL);
			NullCheck(L_15);
			SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580(L_15, L_16, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = V_2;
			NullCheck(L_17);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_17, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_19 = V_1;
			NullCheck(L_19);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_19, /*hidden argument*/NULL);
			NullCheck(L_20);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_20, /*hidden argument*/NULL);
			float L_22 = L_21.get_x_2();
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_23 = V_1;
			NullCheck(L_23);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_23, /*hidden argument*/NULL);
			NullCheck(L_24);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_24, /*hidden argument*/NULL);
			float L_26 = L_25.get_y_3();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27;
			memset((&L_27), 0, sizeof(L_27));
			Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_27), L_22, L_26, (-1.0f), /*hidden argument*/NULL);
			NullCheck(L_18);
			Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_18, L_27, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_28 = V_2;
			NullCheck(L_28);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_29 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_28, /*hidden argument*/NULL);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = ((PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63 *)__this)->get_Hexagon_Outline_Scale_21();
			NullCheck(L_29);
			Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_29, L_30, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_31 = L_5;
			NullCheck(L_31);
			GameObject_AddComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_m034B7C9CB50717611871DEBE6CB07147F506A8E9(L_31, /*hidden argument*/GameObject_AddComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_m034B7C9CB50717611871DEBE6CB07147F506A8E9_RuntimeMethod_var);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_32 = L_31;
			NullCheck(L_32);
			SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_33 = GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538(L_32, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538_RuntimeMethod_var);
			Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_34 = __this->get_HexagonSprite_28();
			NullCheck(L_33);
			SpriteRenderer_set_sprite_m9F5C8B2007AA03FAB66F0CB61260349DF1E28611(L_33, L_34, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_35 = L_32;
			NullCheck(L_35);
			SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_36 = GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538(L_35, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538_RuntimeMethod_var);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_37 = V_1;
			NullCheck(L_37);
			SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_38 = GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538(L_37, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mD25CEAAA219FA1235F8E88F914D2F8AC57303538_RuntimeMethod_var);
			NullCheck(L_38);
			Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_39 = SpriteRenderer_get_color_m1456AB27D5B09F28A273EC0BBD4F03A0FDA51E99(L_38, /*hidden argument*/NULL);
			NullCheck(L_36);
			SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580(L_36, L_39, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_40 = L_35;
			NullCheck(L_40);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_41 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_40, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_42 = V_1;
			NullCheck(L_42);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_43 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_42, /*hidden argument*/NULL);
			NullCheck(L_43);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_43, /*hidden argument*/NULL);
			float L_45 = L_44.get_x_2();
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_46 = V_1;
			NullCheck(L_46);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_47 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_46, /*hidden argument*/NULL);
			NullCheck(L_47);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_47, /*hidden argument*/NULL);
			float L_49 = L_48.get_y_3();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_50;
			memset((&L_50), 0, sizeof(L_50));
			Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_50), L_45, L_49, (-2.0f), /*hidden argument*/NULL);
			NullCheck(L_41);
			Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_41, L_50, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_51 = L_40;
			NullCheck(L_51);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_52 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_51, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_53 = V_1;
			NullCheck(L_53);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_54 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_53, /*hidden argument*/NULL);
			NullCheck(L_54);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_54, /*hidden argument*/NULL);
			NullCheck(L_52);
			Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_52, L_55, /*hidden argument*/NULL);
			NullCheck(L_51);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_56 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_51, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_57 = V_2;
			NullCheck(L_57);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_58 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_57, /*hidden argument*/NULL);
			NullCheck(L_56);
			Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_56, L_58, /*hidden argument*/NULL);
		}

IL_0146:
		{
			bool L_59 = Enumerator_MoveNext_m45D437EE4A29B63712A1B64D39D2809F3594A0DE((Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m45D437EE4A29B63712A1B64D39D2809F3594A0DE_RuntimeMethod_var);
			if (L_59)
			{
				goto IL_0017;
			}
		}

IL_0152:
		{
			IL2CPP_LEAVE(0x162, FINALLY_0154);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0154;
	}

FINALLY_0154:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m992731D0C6BC506250880A1095BCFCFEED22350F((Enumerator_t4094D8CD532DD89ABF2B9E585CD609104A1CD578 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m992731D0C6BC506250880A1095BCFCFEED22350F_RuntimeMethod_var);
		IL2CPP_END_FINALLY(340)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(340)
	{
		IL2CPP_JUMP_TBL(0x162, IL_0162)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0162:
	{
		return;
	}
}
// System.Collections.IEnumerator GridMapManager::ProduceHexagon(System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GridMapManager_ProduceHexagon_m214E1A2F495919DEC056EA874C97E901CC3F4621 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___columns0, List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * ___color1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_ProduceHexagon_m214E1A2F495919DEC056EA874C97E901CC3F4621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * L_0 = (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C *)il2cpp_codegen_object_new(U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C_il2cpp_TypeInfo_var);
		U3CProduceHexagonU3Ed__32__ctor_m7C3798865DC945DA4EACAAA760443EAFBD8033C8(L_0, 0, /*hidden argument*/NULL);
		U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * L_2 = L_1;
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_3 = ___columns0;
		NullCheck(L_2);
		L_2->set_columns_3(L_3);
		U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * L_4 = L_2;
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_5 = ___color1;
		NullCheck(L_4);
		L_4->set_color_4(L_5);
		return L_4;
	}
}
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Color>> GridMapManager::ColoredGridProduction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * GridMapManager_ColoredGridProduction_m26846DC1A6559F858A1DDC079E04CB70A04344B2 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridMapManager_ColoredGridProduction_m26846DC1A6559F858A1DDC079E04CB70A04344B2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_0 = (List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 *)il2cpp_codegen_object_new(List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219_il2cpp_TypeInfo_var);
		List_1__ctor_mB36E5A3444519179409E72DA2E6AE580BD1962B0(L_0, /*hidden argument*/List_1__ctor_mB36E5A3444519179409E72DA2E6AE580BD1962B0_RuntimeMethod_var);
		V_0 = L_0;
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_1 = (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 *)il2cpp_codegen_object_new(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86_il2cpp_TypeInfo_var);
		List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E(L_1, /*hidden argument*/List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E_RuntimeMethod_var);
		V_1 = (bool)1;
		V_2 = 0;
		goto IL_00f6;
	}

IL_0015:
	{
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_2 = V_0;
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_3 = (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 *)il2cpp_codegen_object_new(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86_il2cpp_TypeInfo_var);
		List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E(L_3, /*hidden argument*/List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E_RuntimeMethod_var);
		NullCheck(L_2);
		List_1_Add_m2005A4FE9473352474F6255670AA4F580BDB1CC5(L_2, L_3, /*hidden argument*/List_1_Add_m2005A4FE9473352474F6255670AA4F580BDB1CC5_RuntimeMethod_var);
		V_3 = 0;
		goto IL_00e6;
	}

IL_0027:
	{
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_4 = V_0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_6 = List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_7 = __this->get_colorList_39();
		float L_8 = Random_get_value_mC998749E08291DD42CF31C026FAC4F14F746831C(/*hidden argument*/NULL);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_9 = __this->get_colorList_39();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_inline(L_9, /*hidden argument*/List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_RuntimeMethod_var);
		NullCheck(L_7);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_11 = List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_inline(L_7, ((int32_t)((int32_t)(((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)L_8, (float)(75486.0f))))))%(int32_t)L_10)), /*hidden argument*/List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_RuntimeMethod_var);
		NullCheck(L_6);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_6, L_11, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
	}

IL_0056:
	{
		V_1 = (bool)1;
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_12 = V_0;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_14 = List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_inline(L_12, L_13, /*hidden argument*/List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_RuntimeMethod_var);
		int32_t L_15 = V_3;
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_16 = __this->get_colorList_39();
		float L_17 = Random_get_value_mC998749E08291DD42CF31C026FAC4F14F746831C(/*hidden argument*/NULL);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_18 = __this->get_colorList_39();
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_inline(L_18, /*hidden argument*/List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_RuntimeMethod_var);
		NullCheck(L_16);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_20 = List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_inline(L_16, ((int32_t)((int32_t)(((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)L_17, (float)(75486.0f))))))%(int32_t)L_19)), /*hidden argument*/List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_RuntimeMethod_var);
		NullCheck(L_14);
		List_1_set_Item_m99A8C26E8C19A07648E04317DD42457C40A68A99(L_14, L_15, L_20, /*hidden argument*/List_1_set_Item_m99A8C26E8C19A07648E04317DD42457C40A68A99_RuntimeMethod_var);
		int32_t L_21 = V_2;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)1))) < ((int32_t)0)))
		{
			goto IL_00dc;
		}
	}
	{
		int32_t L_22 = V_3;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)1))) < ((int32_t)0)))
		{
			goto IL_00dc;
		}
	}
	{
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_23 = V_0;
		int32_t L_24 = V_2;
		NullCheck(L_23);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_25 = List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_inline(L_23, L_24, /*hidden argument*/List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_RuntimeMethod_var);
		int32_t L_26 = V_3;
		NullCheck(L_25);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_27 = List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_inline(L_25, ((int32_t)il2cpp_codegen_subtract((int32_t)L_26, (int32_t)1)), /*hidden argument*/List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_RuntimeMethod_var);
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_28 = V_0;
		int32_t L_29 = V_2;
		NullCheck(L_28);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_30 = List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_inline(L_28, L_29, /*hidden argument*/List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_RuntimeMethod_var);
		int32_t L_31 = V_3;
		NullCheck(L_30);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_32 = List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_inline(L_30, L_31, /*hidden argument*/List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_RuntimeMethod_var);
		bool L_33 = Color_op_Equality_m71B1A2F64AD6228F10E20149EF6440460D2C748E(L_27, L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00da;
		}
	}
	{
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_34 = V_0;
		int32_t L_35 = V_2;
		NullCheck(L_34);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_36 = List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_inline(L_34, ((int32_t)il2cpp_codegen_subtract((int32_t)L_35, (int32_t)1)), /*hidden argument*/List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_RuntimeMethod_var);
		int32_t L_37 = V_3;
		NullCheck(L_36);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_38 = List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_inline(L_36, L_37, /*hidden argument*/List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_RuntimeMethod_var);
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_39 = V_0;
		int32_t L_40 = V_2;
		NullCheck(L_39);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_41 = List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_inline(L_39, L_40, /*hidden argument*/List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_RuntimeMethod_var);
		int32_t L_42 = V_3;
		NullCheck(L_41);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_43 = List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_inline(L_41, L_42, /*hidden argument*/List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_RuntimeMethod_var);
		bool L_44 = Color_op_Equality_m71B1A2F64AD6228F10E20149EF6440460D2C748E(L_38, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_00dc;
		}
	}

IL_00da:
	{
		V_1 = (bool)0;
	}

IL_00dc:
	{
		bool L_45 = V_1;
		if (!L_45)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_46 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1));
	}

IL_00e6:
	{
		int32_t L_47 = V_3;
		int32_t L_48 = GridMapManager_GetGridHeight_m76516C97F13FF377E0E91F5A48EA948867E5C41B_inline(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_47) < ((int32_t)L_48)))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_49 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
	}

IL_00f6:
	{
		int32_t L_50 = V_2;
		int32_t L_51 = GridMapManager_GetGridWidth_m4DF42E736F4B0984D7E727901ACF84405DD895BA_inline(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_0015;
		}
	}
	{
		List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_52 = V_0;
		return L_52;
	}
}
// System.Boolean GridMapManager::DetectColumn(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GridMapManager_DetectColumn_m10378C564143B7BCC0F3945603BF7499BE472534 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, int32_t ___x0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = GridMapManager_GetGridWidth_m4DF42E736F4B0984D7E727901ACF84405DD895BA_inline(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___x0;
		return (bool)((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)2))%(int32_t)2))) == ((int32_t)((int32_t)((int32_t)L_1%(int32_t)2))))? 1 : 0);
	}
}
// System.Boolean GridMapManager::InputAvailable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GridMapManager_InputAvailable_m2DFC0C65CF2EE1843ED10B7DF610A40CF109F5DA (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_HexagonProductionStatus_42();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		bool L_1 = __this->get_gameOver_33();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		bool L_2 = __this->get_HexagonRotationStatus_40();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		bool L_3 = __this->get_HexagonExplosionStatus_41();
		return (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
	}

IL_0022:
	{
		return (bool)0;
	}
}
// System.Single GridMapManager::GetGridStartCoordinateX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GridMapManager_GetGridStartCoordinateX_m039E8599B60121EAB5A5460E2DC44C36FA82C59D (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_gridWidth_29();
		return ((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)((int32_t)L_0/(int32_t)2))))), (float)(-0.445f)));
	}
}
// System.Boolean GridMapManager::IsValid(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GridMapManager_IsValid_m9B4B23F940F3FE203303CD85ACBC41F4596B8093 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pos0, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___pos0;
		float L_1 = L_0.get_x_0();
		if ((!(((float)L_1) >= ((float)(0.0f)))))
		{
			goto IL_0039;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___pos0;
		float L_3 = L_2.get_x_0();
		int32_t L_4 = GridMapManager_GetGridWidth_m4DF42E736F4B0984D7E727901ACF84405DD895BA_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) < ((float)(((float)((float)L_4)))))))
		{
			goto IL_0039;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = ___pos0;
		float L_6 = L_5.get_y_1();
		if ((!(((float)L_6) >= ((float)(0.0f)))))
		{
			goto IL_0039;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = ___pos0;
		float L_8 = L_7.get_y_1();
		int32_t L_9 = GridMapManager_GetGridHeight_m76516C97F13FF377E0E91F5A48EA948867E5C41B_inline(__this, /*hidden argument*/NULL);
		return (bool)((((float)L_8) < ((float)(((float)((float)L_9)))))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
// System.Void GridMapManager::SetGridWidth(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_SetGridWidth_mCDC775F41010E50C72D4C68A0643CF7E0D3B904B (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, int32_t ___width0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___width0;
		__this->set_gridWidth_29(L_0);
		return;
	}
}
// System.Int32 GridMapManager::GetGridWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GridMapManager_GetGridWidth_m4DF42E736F4B0984D7E727901ACF84405DD895BA (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_gridWidth_29();
		return L_0;
	}
}
// System.Void GridMapManager::SetGridHeight(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_SetGridHeight_m89865CCB85B689CA751046765A238E10A794F509 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, int32_t ___height0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___height0;
		__this->set_gridHeight_30(L_0);
		return;
	}
}
// System.Int32 GridMapManager::GetGridHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GridMapManager_GetGridHeight_m76516C97F13FF377E0E91F5A48EA948867E5C41B (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_gridHeight_30();
		return L_0;
	}
}
// System.Void GridMapManager::SetColorList(System.Collections.Generic.List`1<UnityEngine.Color>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_SetColorList_mBF555D15D608B2AE94B83811BDFC9DF798FF7651 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * ___list0, const RuntimeMethod* method)
{
	{
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_0 = ___list0;
		__this->set_colorList_39(L_0);
		return;
	}
}
// System.Void GridMapManager::SetBombProduction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager_SetBombProduction_mC86F578E5457E53A98856BB567B5EA081BED1BE2 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		__this->set_bombProduction_32((bool)1);
		return;
	}
}
// HexagonManager GridMapManager::GetSelectedHexagon()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * GridMapManager_GetSelectedHexagon_m82E2459ADA9669F14C94801054CBB147501B7A4C (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_0 = __this->get_selectedHexagon_35();
		return L_0;
	}
}
// System.Void GridMapManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager__ctor_mAFC93605BB3157CFBCB81094DDE41DF3ABDDD8B7 (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GridMapManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GridMapManager__cctor_m0E439D4CCCD36AF686D721CB271F6BEAE9C2678D (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GridMapManager_<ProduceHexagon>d__32::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProduceHexagonU3Ed__32__ctor_m7C3798865DC945DA4EACAAA760443EAFBD8033C8 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void GridMapManager_<ProduceHexagon>d__32::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProduceHexagonU3Ed__32_System_IDisposable_Dispose_m9B3606C41110AC2C771F556F19878B6BA3DFA5F4 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x1A, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		U3CProduceHexagonU3Ed__32_U3CU3Em__Finally1_m6D2CEC25E49CF58E9CF395DD0B1951967D004B63(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x1A, IL_001a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean GridMapManager_<ProduceHexagon>d__32::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CProduceHexagonU3Ed__32_MoveNext_m16EF3B112413C7F8EA4BC66B99882AB877E9D8E7 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CProduceHexagonU3Ed__32_MoveNext_m16EF3B112413C7F8EA4BC66B99882AB877E9D8E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	bool V_5 = false;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_6 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	float G_B6_0 = 0.0f;
	float G_B5_0 = 0.0f;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_0();
			V_1 = L_0;
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_1 = __this->get_U3CU3E4__this_2();
			V_2 = L_1;
			int32_t L_2 = V_1;
			if (!L_2)
			{
				goto IL_001f;
			}
		}

IL_0011:
		{
			int32_t L_3 = V_1;
			if ((((int32_t)L_3) == ((int32_t)1)))
			{
				goto IL_012d;
			}
		}

IL_0018:
		{
			V_0 = (bool)0;
			goto IL_026a;
		}

IL_001f:
		{
			__this->set_U3CU3E1__state_0((-1));
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_4 = V_2;
			NullCheck(L_4);
			float L_5 = GridMapManager_GetGridStartCoordinateX_m039E8599B60121EAB5A5460E2DC44C36FA82C59D(L_4, /*hidden argument*/NULL);
			__this->set_U3CstartXU3E5__3_6(L_5);
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_6 = V_2;
			NullCheck(L_6);
			L_6->set_HexagonProductionStatus_42((bool)1);
			List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_7 = __this->get_columns_3();
			NullCheck(L_7);
			Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  L_8 = List_1_GetEnumerator_m83178F038A7D4A7E9B0731B7D3078EDCF6FFD0EC(L_7, /*hidden argument*/List_1_GetEnumerator_m83178F038A7D4A7E9B0731B7D3078EDCF6FFD0EC_RuntimeMethod_var);
			__this->set_U3CU3E7__wrap3_7(L_8);
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			goto IL_0236;
		}

IL_0057:
		{
			Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * L_9 = __this->get_address_of_U3CU3E7__wrap3_7();
			int32_t L_10 = Enumerator_get_Current_m88A0089A1A4EEBC3017E2DA569A01C7919B10945_inline((Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C *)L_9, /*hidden argument*/Enumerator_get_Current_m88A0089A1A4EEBC3017E2DA569A01C7919B10945_RuntimeMethod_var);
			__this->set_U3CiU3E5__5_8(L_10);
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_11 = V_2;
			int32_t L_12 = __this->get_U3CiU3E5__5_8();
			NullCheck(L_11);
			bool L_13 = GridMapManager_DetectColumn_m10378C564143B7BCC0F3945603BF7499BE472534(L_11, L_12, /*hidden argument*/NULL);
			V_5 = L_13;
			float L_14 = __this->get_U3CstartXU3E5__3_6();
			int32_t L_15 = __this->get_U3CiU3E5__5_8();
			V_3 = ((float)il2cpp_codegen_add((float)L_14, (float)((float)il2cpp_codegen_multiply((float)(0.445f), (float)(((float)((float)L_15)))))));
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_16 = V_2;
			NullCheck(L_16);
			List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_17 = L_16->get_gameGrid_36();
			int32_t L_18 = __this->get_U3CiU3E5__5_8();
			NullCheck(L_17);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_19 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_17, L_18, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
			NullCheck(L_19);
			int32_t L_20 = List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline(L_19, /*hidden argument*/List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var);
			bool L_21 = V_5;
			G_B5_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(0.23f), (float)(((float)((float)L_20))))), (float)(2.0f))), (float)(-3.0f)));
			if (L_21)
			{
				G_B6_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(0.23f), (float)(((float)((float)L_20))))), (float)(2.0f))), (float)(-3.0f)));
				goto IL_00bf;
			}
		}

IL_00b8:
		{
			G_B7_0 = (0.0f);
			G_B7_1 = G_B5_0;
			goto IL_00c4;
		}

IL_00bf:
		{
			G_B7_0 = (0.23f);
			G_B7_1 = G_B6_0;
		}

IL_00c4:
		{
			V_4 = ((float)il2cpp_codegen_add((float)G_B7_1, (float)G_B7_0));
			float L_22 = V_3;
			float L_23 = V_4;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24;
			memset((&L_24), 0, sizeof(L_24));
			Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_24), L_22, L_23, (0.0f), /*hidden argument*/NULL);
			__this->set_U3CstartPositionU3E5__2_5(L_24);
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_25 = V_2;
			NullCheck(L_25);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = L_25->get_hexPrefab_24();
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_27 = V_2;
			NullCheck(L_27);
			Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_28 = ((PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63 *)L_27)->get_Hexagon_Start_Position_22();
			IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_28, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
			Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_30 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_31 = V_2;
			NullCheck(L_31);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_32 = L_31->get_hexParent_25();
			NullCheck(L_32);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_33 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_32, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_34 = Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m6895A7A231540279E01A537649EB42814FD2671B(L_26, L_29, L_30, L_33, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m6895A7A231540279E01A537649EB42814FD2671B_RuntimeMethod_var);
			V_6 = L_34;
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_35 = V_6;
			NullCheck(L_35);
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_36 = GameObject_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5417F5D6065EA0BDB12492F51D50F061AA28FAF4(L_35, /*hidden argument*/GameObject_GetComponent_TisHexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009_m5417F5D6065EA0BDB12492F51D50F061AA28FAF4_RuntimeMethod_var);
			__this->set_U3CnewHexU3E5__6_9(L_36);
			WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_37 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
			WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_37, (0.025f), /*hidden argument*/NULL);
			__this->set_U3CU3E2__current_1(L_37);
			__this->set_U3CU3E1__state_0(1);
			V_0 = (bool)1;
			goto IL_026a;
		}

IL_012d:
		{
			__this->set_U3CU3E1__state_0(((int32_t)-3));
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_38 = V_2;
			NullCheck(L_38);
			bool L_39 = L_38->get_bombProduction_32();
			if (!L_39)
			{
				goto IL_0160;
			}
		}

IL_013d:
		{
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_40 = __this->get_U3CnewHexU3E5__6_9();
			NullCheck(L_40);
			HexagonManager_SetBomb_mA40C2AB6C12399179D44B1FD7B64BA47E53C5533(L_40, /*hidden argument*/NULL);
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_41 = V_2;
			NullCheck(L_41);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_42 = L_41->get_bombs_38();
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_43 = __this->get_U3CnewHexU3E5__6_9();
			NullCheck(L_42);
			List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_42, L_43, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_44 = V_2;
			NullCheck(L_44);
			L_44->set_bombProduction_32((bool)0);
		}

IL_0160:
		{
			List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_45 = __this->get_color_4();
			if (L_45)
			{
				goto IL_0198;
			}
		}

IL_0168:
		{
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_46 = __this->get_U3CnewHexU3E5__6_9();
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_47 = V_2;
			NullCheck(L_47);
			List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_48 = L_47->get_colorList_39();
			float L_49 = Random_get_value_mC998749E08291DD42CF31C026FAC4F14F746831C(/*hidden argument*/NULL);
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_50 = V_2;
			NullCheck(L_50);
			List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_51 = L_50->get_colorList_39();
			NullCheck(L_51);
			int32_t L_52 = List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_inline(L_51, /*hidden argument*/List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_RuntimeMethod_var);
			NullCheck(L_48);
			Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_53 = List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_inline(L_48, ((int32_t)((int32_t)(((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)L_49, (float)(75486.0f))))))%(int32_t)L_52)), /*hidden argument*/List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_RuntimeMethod_var);
			NullCheck(L_46);
			HexagonManager_SetColor_mAFE672E70F49F0041DB82EEF7396ADBFFFB12016(L_46, L_53, /*hidden argument*/NULL);
			goto IL_01cf;
		}

IL_0198:
		{
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_54 = __this->get_U3CnewHexU3E5__6_9();
			List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 * L_55 = __this->get_color_4();
			int32_t L_56 = __this->get_U3CiU3E5__5_8();
			NullCheck(L_55);
			List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_57 = List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_inline(L_55, L_56, /*hidden argument*/List_1_get_Item_mFBC83D17E37EA51660A2B55FA1BBC265DE8AFE89_RuntimeMethod_var);
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_58 = V_2;
			NullCheck(L_58);
			List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_59 = L_58->get_gameGrid_36();
			int32_t L_60 = __this->get_U3CiU3E5__5_8();
			NullCheck(L_59);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_61 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_59, L_60, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
			NullCheck(L_61);
			int32_t L_62 = List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline(L_61, /*hidden argument*/List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var);
			NullCheck(L_57);
			Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_63 = List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_inline(L_57, L_62, /*hidden argument*/List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_RuntimeMethod_var);
			NullCheck(L_54);
			HexagonManager_SetColor_mAFE672E70F49F0041DB82EEF7396ADBFFFB12016(L_54, L_63, /*hidden argument*/NULL);
		}

IL_01cf:
		{
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_64 = __this->get_U3CnewHexU3E5__6_9();
			int32_t L_65 = __this->get_U3CiU3E5__5_8();
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_66 = V_2;
			NullCheck(L_66);
			List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_67 = L_66->get_gameGrid_36();
			int32_t L_68 = __this->get_U3CiU3E5__5_8();
			NullCheck(L_67);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_69 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_67, L_68, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
			NullCheck(L_69);
			int32_t L_70 = List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline(L_69, /*hidden argument*/List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var);
			Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_71;
			memset((&L_71), 0, sizeof(L_71));
			Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_71), (((float)((float)L_65))), (((float)((float)L_70))), /*hidden argument*/NULL);
			NullCheck(L_64);
			HexagonManager_ChangeGridPosition_m31C3E0BA897DF72C9A6A28640C0ED63E692BF44F(L_64, L_71, /*hidden argument*/NULL);
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_72 = __this->get_U3CnewHexU3E5__6_9();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_73 = __this->get_U3CstartPositionU3E5__2_5();
			IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
			Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_74 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_73, /*hidden argument*/NULL);
			NullCheck(L_72);
			HexagonManager_ChangeWorldPosition_m4AFE1537B92653C2C4C7EE6A35C5FA01469AFE1C(L_72, L_74, /*hidden argument*/NULL);
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_75 = V_2;
			NullCheck(L_75);
			List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_76 = L_75->get_gameGrid_36();
			int32_t L_77 = __this->get_U3CiU3E5__5_8();
			NullCheck(L_76);
			List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_78 = List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_inline(L_76, L_77, /*hidden argument*/List_1_get_Item_m626368B0C50B0584F0FD711A64E2D0E7E7CD700B_RuntimeMethod_var);
			HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_79 = __this->get_U3CnewHexU3E5__6_9();
			NullCheck(L_78);
			List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5(L_78, L_79, /*hidden argument*/List_1_Add_m2E571DC0BCFB2A1BEF1CFAA626E4008F6F5D05F5_RuntimeMethod_var);
			__this->set_U3CnewHexU3E5__6_9((HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 *)NULL);
		}

IL_0236:
		{
			Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * L_80 = __this->get_address_of_U3CU3E7__wrap3_7();
			bool L_81 = Enumerator_MoveNext_m113E33A615748C69D63D1245F5FD820B4B3D43F7((Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C *)L_80, /*hidden argument*/Enumerator_MoveNext_m113E33A615748C69D63D1245F5FD820B4B3D43F7_RuntimeMethod_var);
			if (L_81)
			{
				goto IL_0057;
			}
		}

IL_0246:
		{
			U3CProduceHexagonU3Ed__32_U3CU3Em__Finally1_m6D2CEC25E49CF58E9CF395DD0B1951967D004B63(__this, /*hidden argument*/NULL);
			Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * L_82 = __this->get_address_of_U3CU3E7__wrap3_7();
			il2cpp_codegen_initobj(L_82, sizeof(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C ));
			GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_83 = V_2;
			NullCheck(L_83);
			L_83->set_HexagonProductionStatus_42((bool)0);
			V_0 = (bool)0;
			goto IL_026a;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FAULT_0263;
	}

FAULT_0263:
	{ // begin fault (depth: 1)
		U3CProduceHexagonU3Ed__32_System_IDisposable_Dispose_m9B3606C41110AC2C771F556F19878B6BA3DFA5F4(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(611)
	} // end fault
	IL2CPP_CLEANUP(611)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_026a:
	{
		bool L_84 = V_0;
		return L_84;
	}
}
// System.Void GridMapManager_<ProduceHexagon>d__32::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProduceHexagonU3Ed__32_U3CU3Em__Finally1_m6D2CEC25E49CF58E9CF395DD0B1951967D004B63 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CProduceHexagonU3Ed__32_U3CU3Em__Finally1_m6D2CEC25E49CF58E9CF395DD0B1951967D004B63_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_U3CU3E1__state_0((-1));
		Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * L_0 = __this->get_address_of_U3CU3E7__wrap3_7();
		Enumerator_Dispose_mBA3B0129DABD8274AF3497CC93E6A2DEA0A23892((Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C *)L_0, /*hidden argument*/Enumerator_Dispose_mBA3B0129DABD8274AF3497CC93E6A2DEA0A23892_RuntimeMethod_var);
		return;
	}
}
// System.Object GridMapManager_<ProduceHexagon>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProduceHexagonU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0A6B4CB588C3EDEC817AFFFF4B6E72163C77269 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void GridMapManager_<ProduceHexagon>d__32::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_Reset_m2BBF02C5180830143CB1CA1F8700AE5E16943D11 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_Reset_m2BBF02C5180830143CB1CA1F8700AE5E16943D11_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_Reset_m2BBF02C5180830143CB1CA1F8700AE5E16943D11_RuntimeMethod_var);
	}
}
// System.Object GridMapManager_<ProduceHexagon>d__32::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProduceHexagonU3Ed__32_System_Collections_IEnumerator_get_Current_mA89EE43A7D28B5CCD49A138D1F68D5BD2A258821 (U3CProduceHexagonU3Ed__32_tAB9BEB7EB76BD37A5581E3129C5B481A1FFDFB4C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GridMapManager_<RotationCheckingCoroutine>d__26::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRotationCheckingCoroutineU3Ed__26__ctor_m7A58025DEA96C9E7384F73E935327F43A258C283 (U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void GridMapManager_<RotationCheckingCoroutine>d__26::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRotationCheckingCoroutineU3Ed__26_System_IDisposable_Dispose_m908CC17F2CE87867694454D817BA1FBCC71AB6CD (U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean GridMapManager_<RotationCheckingCoroutine>d__26::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CRotationCheckingCoroutineU3Ed__26_MoveNext_mCC5274CE72BFCD64D9C61EEAEE85261124845CA6 (U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRotationCheckingCoroutineU3Ed__26_MoveNext_mCC5274CE72BFCD64D9C61EEAEE85261124845CA6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_006c;
			}
			case 2:
			{
				goto IL_0132;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		__this->set_U3CexplosiveHexagonU3E5__2_4((List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 *)NULL);
		__this->set_U3CflagU3E5__3_5((bool)1);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_HexagonRotationStatus_40((bool)1);
		__this->set_U3CiU3E5__4_6(0);
		goto IL_00a3;
	}

IL_0047:
	{
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_4 = V_1;
		bool L_5 = __this->get_clockWise_3();
		NullCheck(L_4);
		GridMapManager_SwapHexagon_mC8A4398C219C587EFAB4490F0C24FAC7216E2B27(L_4, L_5, /*hidden argument*/NULL);
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_6 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_6, (0.3f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_6);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_006c:
	{
		__this->set_U3CU3E1__state_0((-1));
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_7 = V_1;
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_8 = V_1;
		NullCheck(L_8);
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_9 = L_8->get_gameGrid_36();
		NullCheck(L_7);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_10 = GridMapManager_CheckingExplosion_m5FBB713BB06BCF9D906D7A401DA316494322B5C1(L_7, L_9, /*hidden argument*/NULL);
		__this->set_U3CexplosiveHexagonU3E5__2_4(L_10);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_11 = __this->get_U3CexplosiveHexagonU3E5__2_4();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline(L_11, /*hidden argument*/List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var);
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_00b6;
		}
	}
	{
		int32_t L_13 = __this->get_U3CiU3E5__4_6();
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		int32_t L_14 = V_2;
		__this->set_U3CiU3E5__4_6(L_14);
	}

IL_00a3:
	{
		int32_t L_15 = __this->get_U3CiU3E5__4_6();
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_16 = V_1;
		NullCheck(L_16);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_17 = L_16->get_selectedGroup_37();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline(L_17, /*hidden argument*/List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var);
		if ((((int32_t)L_15) < ((int32_t)L_18)))
		{
			goto IL_0047;
		}
	}

IL_00b6:
	{
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_19 = V_1;
		NullCheck(L_19);
		L_19->set_HexagonExplosionStatus_41((bool)1);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_20 = V_1;
		NullCheck(L_20);
		L_20->set_HexagonRotationStatus_40((bool)0);
		goto IL_0139;
	}

IL_00c6:
	{
		bool L_21 = __this->get_U3CflagU3E5__3_5();
		if (!L_21)
		{
			goto IL_00f8;
		}
	}
	{
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_22 = V_1;
		NullCheck(L_22);
		L_22->set_HexagonProductionStatus_42((bool)1);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_23 = V_1;
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_24 = V_1;
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_25 = V_1;
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_26 = __this->get_U3CexplosiveHexagonU3E5__2_4();
		NullCheck(L_25);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_27 = GridMapManager_DetectBombANDClearExplodeHexagon_m6A4071F050BF7576CA67F92580349C93711471EF(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		RuntimeObject* L_28 = GridMapManager_ProduceHexagon_m214E1A2F495919DEC056EA874C97E901CC3F4621(L_24, L_27, (List_1_t9DE6512E8A1BC0C29A9B42A3AD4E298F5F483219 *)NULL, /*hidden argument*/NULL);
		NullCheck(L_23);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(L_23, L_28, /*hidden argument*/NULL);
		__this->set_U3CflagU3E5__3_5((bool)0);
		goto IL_0119;
	}

IL_00f8:
	{
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_29 = V_1;
		NullCheck(L_29);
		bool L_30 = L_29->get_HexagonProductionStatus_42();
		if (L_30)
		{
			goto IL_0119;
		}
	}
	{
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_31 = V_1;
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_32 = V_1;
		NullCheck(L_32);
		List_1_t7AFE89C7B448943B888E1AE87D35DBD634C52C1C * L_33 = L_32->get_gameGrid_36();
		NullCheck(L_31);
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_34 = GridMapManager_CheckingExplosion_m5FBB713BB06BCF9D906D7A401DA316494322B5C1(L_31, L_33, /*hidden argument*/NULL);
		__this->set_U3CexplosiveHexagonU3E5__2_4(L_34);
		__this->set_U3CflagU3E5__3_5((bool)1);
	}

IL_0119:
	{
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_35 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_35, (0.3f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_35);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0132:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0139:
	{
		List_1_t34A98867CBC57CD06FDCE193D65187AD6C327B21 * L_36 = __this->get_U3CexplosiveHexagonU3E5__2_4();
		NullCheck(L_36);
		int32_t L_37 = List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_inline(L_36, /*hidden argument*/List_1_get_Count_mD2B361CE973940EB2A9D5887EA9DADCD15F61E33_RuntimeMethod_var);
		if ((((int32_t)L_37) > ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_38 = V_1;
		NullCheck(L_38);
		L_38->set_HexagonExplosionStatus_41((bool)0);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_39 = V_1;
		NullCheck(L_39);
		GridMapManager_FindHexagonGroup_mE0BE80BC83F5FBC6186AA7FC2B2E16ACA87BCA82(L_39, /*hidden argument*/NULL);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_40 = V_1;
		NullCheck(L_40);
		GridMapManager_CreateOutline_m1308C33D309E14FAE44B40A754A2033952625BD2(L_40, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Object GridMapManager_<RotationCheckingCoroutine>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CRotationCheckingCoroutineU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCCD2467DFA08642E5274DD8493DFB6A523C7E590 (U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void GridMapManager_<RotationCheckingCoroutine>d__26::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_Reset_m601DFC85C4329034BFEEC55EA05740CD4237C0EA (U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_Reset_m601DFC85C4329034BFEEC55EA05740CD4237C0EA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_Reset_m601DFC85C4329034BFEEC55EA05740CD4237C0EA_RuntimeMethod_var);
	}
}
// System.Object GridMapManager_<RotationCheckingCoroutine>d__26::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CRotationCheckingCoroutineU3Ed__26_System_Collections_IEnumerator_get_Current_m2CE4E623B0B56CD21F3BE1762061C82A0AFF8B69 (U3CRotationCheckingCoroutineU3Ed__26_t4059A246CBE2E17003D3B11A47A13D8AE13E90E9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HexagonManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_Start_mD2964CFB9793B0403254CD1F067061A9B022002F (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexagonManager_Start_mD2964CFB9793B0403254CD1F067061A9B022002F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_0 = ((GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_StaticFields*)il2cpp_codegen_static_fields_for(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var))->get_instance_23();
		__this->set_GridManagerObject_23(L_0);
		__this->set_lerp_28((bool)0);
		return;
	}
}
// System.Void HexagonManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_Update_mA608E8C036E21D38479A5E8E28F01F66EC8C03CC (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexagonManager_Update_mA608E8C036E21D38479A5E8E28F01F66EC8C03CC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		bool L_0 = __this->get_lerp_28();
		if (!L_0)
		{
			goto IL_00b9;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_2();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_4 = __this->get_address_of_Hexposition_27();
		float L_5 = L_4->get_x_0();
		float L_6 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364(L_3, L_5, ((float)il2cpp_codegen_multiply((float)L_6, (float)(9.0f))), /*hidden argument*/NULL);
		V_0 = L_7;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_y_3();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_11 = __this->get_address_of_Hexposition_27();
		float L_12 = L_11->get_y_1();
		float L_13 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		float L_14 = Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364(L_10, L_12, ((float)il2cpp_codegen_multiply((float)L_13, (float)(9.0f))), /*hidden argument*/NULL);
		V_1 = L_14;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_18), L_16, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_18, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_15, L_19, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_20, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_22 = __this->get_Hexposition_27();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		float L_24 = Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7(L_21, L_23, /*hidden argument*/NULL);
		if ((!(((float)L_24) < ((float)(0.05f)))))
		{
			goto IL_00b9;
		}
	}
	{
		__this->set_lerp_28((bool)0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_25 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_26 = __this->get_Hexposition_27();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_25, L_27, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		return;
	}
}
// System.Void HexagonManager::Rotate(System.Int32,System.Int32,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_Rotate_mB7690CFEDFCB1DE499F28404977D6DCE7CBC09AC (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, int32_t ___newX0, int32_t ___newY1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___newPos2, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___newPos2;
		__this->set_Hexposition_27(L_0);
		int32_t L_1 = ___newX0;
		HexagonManager_SetX_m479990D055BBF89BDF258489087EFC8AD084CC0C_inline(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___newY1;
		HexagonManager_SetY_m67E6A395342D87F427D4EE382A82AD1D7CAF0CFD_inline(__this, L_2, /*hidden argument*/NULL);
		__this->set_lerp_28((bool)1);
		return;
	}
}
// HexagonManager_NeighbourHexes HexagonManager::GetNeighbours()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  HexagonManager_GetNeighbours_m12298ED576B60FB0F5DF30613D7E508CA6149E12 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	float G_B2_0 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B2_1 = NULL;
	float G_B1_0 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	float G_B3_1 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B3_2 = NULL;
	float G_B5_0 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B5_1 = NULL;
	float G_B4_0 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B4_1 = NULL;
	int32_t G_B6_0 = 0;
	float G_B6_1 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B6_2 = NULL;
	float G_B8_0 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B8_1 = NULL;
	float G_B7_0 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B7_1 = NULL;
	int32_t G_B9_0 = 0;
	float G_B9_1 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B9_2 = NULL;
	float G_B11_0 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B11_1 = NULL;
	float G_B10_0 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B10_1 = NULL;
	int32_t G_B12_0 = 0;
	float G_B12_1 = 0.0f;
	NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8 * G_B12_2 = NULL;
	{
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_0 = __this->get_GridManagerObject_23();
		int32_t L_1 = HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_2 = GridMapManager_DetectColumn_m10378C564143B7BCC0F3945603BF7499BE472534(L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = __this->get_x_24();
		int32_t L_4 = __this->get_y_25();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_5), (((float)((float)L_3))), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1))))), /*hidden argument*/NULL);
		(&V_0)->set_down_3(L_5);
		int32_t L_6 = __this->get_x_24();
		int32_t L_7 = __this->get_y_25();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_8), (((float)((float)L_6))), (((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1))))), /*hidden argument*/NULL);
		(&V_0)->set_up_0(L_8);
		int32_t L_9 = __this->get_x_24();
		bool L_10 = V_1;
		G_B1_0 = (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1)))));
		G_B1_1 = (&V_0);
		if (L_10)
		{
			G_B2_0 = (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1)))));
			G_B2_1 = (&V_0);
			goto IL_0060;
		}
	}
	{
		int32_t L_11 = __this->get_y_25();
		G_B3_0 = L_11;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0068;
	}

IL_0060:
	{
		int32_t L_12 = __this->get_y_25();
		G_B3_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0068:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_13), G_B3_1, (((float)((float)G_B3_0))), /*hidden argument*/NULL);
		G_B3_2->set_upLeft_1(L_13);
		int32_t L_14 = __this->get_x_24();
		bool L_15 = V_1;
		G_B4_0 = (((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1)))));
		G_B4_1 = (&V_0);
		if (L_15)
		{
			G_B5_0 = (((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1)))));
			G_B5_1 = (&V_0);
			goto IL_0089;
		}
	}
	{
		int32_t L_16 = __this->get_y_25();
		G_B6_0 = L_16;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_0091;
	}

IL_0089:
	{
		int32_t L_17 = __this->get_y_25();
		G_B6_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_0091:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_18), G_B6_1, (((float)((float)G_B6_0))), /*hidden argument*/NULL);
		G_B6_2->set_upRight_2(L_18);
		int32_t L_19 = __this->get_x_24();
		bool L_20 = V_1;
		G_B7_0 = (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)))));
		G_B7_1 = (&V_0);
		if (L_20)
		{
			G_B8_0 = (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)))));
			G_B8_1 = (&V_0);
			goto IL_00b4;
		}
	}
	{
		int32_t L_21 = __this->get_y_25();
		G_B9_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)1));
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_00ba;
	}

IL_00b4:
	{
		int32_t L_22 = __this->get_y_25();
		G_B9_0 = L_22;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00ba:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_23), G_B9_1, (((float)((float)G_B9_0))), /*hidden argument*/NULL);
		G_B9_2->set_downLeft_4(L_23);
		int32_t L_24 = __this->get_x_24();
		bool L_25 = V_1;
		G_B10_0 = (((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1)))));
		G_B10_1 = (&V_0);
		if (L_25)
		{
			G_B11_0 = (((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1)))));
			G_B11_1 = (&V_0);
			goto IL_00dd;
		}
	}
	{
		int32_t L_26 = __this->get_y_25();
		G_B12_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_26, (int32_t)1));
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_00e3;
	}

IL_00dd:
	{
		int32_t L_27 = __this->get_y_25();
		G_B12_0 = L_27;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_00e3:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_28), G_B12_1, (((float)((float)G_B12_0))), /*hidden argument*/NULL);
		G_B12_2->set_downRight_5(L_28);
		NeighbourHexes_t788B58302442AF958BB7EBB39A0AC579B4CE59D8  L_29 = V_0;
		return L_29;
	}
}
// System.Void HexagonManager::ChangeWorldPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_ChangeWorldPosition_m4AFE1537B92653C2C4C7EE6A35C5FA01469AFE1C (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___newPosition0, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___newPosition0;
		__this->set_Hexposition_27(L_0);
		__this->set_lerp_28((bool)1);
		return;
	}
}
// System.Void HexagonManager::ChangeGridPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_ChangeGridPosition_m31C3E0BA897DF72C9A6A28640C0ED63E692BF44F (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___newPosition0, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___newPosition0;
		float L_1 = L_0.get_x_0();
		__this->set_x_24((((int32_t)((int32_t)L_1))));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___newPosition0;
		float L_3 = L_2.get_y_1();
		__this->set_y_25((((int32_t)((int32_t)L_3))));
		return;
	}
}
// System.Void HexagonManager::SetBomb()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_SetBomb_mA40C2AB6C12399179D44B1FD7B64BA47E53C5533 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexagonManager_SetBomb_mA40C2AB6C12399179D44B1FD7B64BA47E53C5533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_1 = GameObject_AddComponent_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF57CA692C5FFBA2C6599F6FEEA08E0F9050C368A(L_0, /*hidden argument*/GameObject_AddComponent_TisTextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A_mF57CA692C5FFBA2C6599F6FEEA08E0F9050C368A_RuntimeMethod_var);
		__this->set_text_31(L_1);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_2 = __this->get_text_31();
		NullCheck(L_2);
		TextMesh_set_alignment_mC79810263A381B2CD7ECAB76D4399E18D928F82E(L_2, 1, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_3 = __this->get_text_31();
		NullCheck(L_3);
		TextMesh_set_anchor_m013CFCFA46AB8478ADD1C4818FAAD90596BF4E15(L_3, 4, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_4 = __this->get_text_31();
		NullCheck(L_4);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_4, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_5, L_7, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_8 = __this->get_text_31();
		NullCheck(L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_8, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_x_2();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_13, /*hidden argument*/NULL);
		float L_15 = L_14.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_16), L_12, L_15, (-4.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_9, L_16, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_17 = __this->get_text_31();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_18 = Color_get_black_mEB3C91F45F8AA7E4842238DFCC578BB322723DAF(/*hidden argument*/NULL);
		NullCheck(L_17);
		TextMesh_set_color_mF86B9E8CD0F9FD387AF7D543337B5C14DFE67AF0(L_17, L_18, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_19 = __this->get_text_31();
		NullCheck(L_19);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_19, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_20, L_21, /*hidden argument*/NULL);
		__this->set_bombTimer_30(6);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_22 = __this->get_text_31();
		int32_t* L_23 = __this->get_address_of_bombTimer_30();
		String_t* L_24 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HexagonManager::SetX(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_SetX_m479990D055BBF89BDF258489087EFC8AD084CC0C (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_x_24(L_0);
		return;
	}
}
// System.Int32 HexagonManager::GetX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_x_24();
		return L_0;
	}
}
// System.Void HexagonManager::SetY(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_SetY_m67E6A395342D87F427D4EE382A82AD1D7CAF0CFD (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_y_25(L_0);
		return;
	}
}
// System.Int32 HexagonManager::GetY()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HexagonManager_GetY_m3B711DA0B2F3DAA2347CFE9AD3D7AD597DCB8B2D (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_y_25();
		return L_0;
	}
}
// System.Void HexagonManager::SetColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_SetColor_mAFE672E70F49F0041DB82EEF7396ADBFFFB12016 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___newColor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexagonManager_SetColor_mAFE672E70F49F0041DB82EEF7396ADBFFFB12016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_0 = Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693_RuntimeMethod_var);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_1 = ___newColor0;
		NullCheck(L_0);
		SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580(L_0, L_1, /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_2 = ___newColor0;
		__this->set_color_26(L_2);
		return;
	}
}
// UnityEngine.Color HexagonManager::GetColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  HexagonManager_GetColor_mED456B54AEF3D88B662951813B71E6D833F046D0 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HexagonManager_GetColor_mED456B54AEF3D88B662951813B71E6D833F046D0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_0 = Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693_RuntimeMethod_var);
		NullCheck(L_0);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_1 = SpriteRenderer_get_color_m1456AB27D5B09F28A273EC0BBD4F03A0FDA51E99(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void HexagonManager::Trigger()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager_Trigger_mEDA1CF56DE33D03BC1E8F0146550DFAEEED9890B (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_bombTimer_30();
		__this->set_bombTimer_30(((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1)));
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_1 = __this->get_text_31();
		int32_t* L_2 = __this->get_address_of_bombTimer_30();
		String_t* L_3 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 HexagonManager::GetTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HexagonManager_GetTimer_mEE88C69BB650E3740B5B30D59302877ED000E980 (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_bombTimer_30();
		return L_0;
	}
}
// System.Void HexagonManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HexagonManager__ctor_m8CA3794AD77B9103A96F3FE668C7E559D8E8A04A (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	{
		PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InputGetter::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputGetter_Start_m38C9891BD3D7C690C5E8ABD1B553550CBD249AFC (InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InputGetter_Start_m38C9891BD3D7C690C5E8ABD1B553550CBD249AFC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_0 = ((GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_StaticFields*)il2cpp_codegen_static_fields_for(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var))->get_instance_23();
		__this->set_GridMapManagerObject_24(L_0);
		return;
	}
}
// System.Void InputGetter::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputGetter_Update_mC7B9E787753BC507B57DAF5EBEF9A90AF27B45E9 (InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InputGetter_Update_mC7B9E787753BC507B57DAF5EBEF9A90AF27B45E9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * V_1 = NULL;
	Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_0 = __this->get_GridMapManagerObject_24();
		NullCheck(L_0);
		bool L_1 = GridMapManager_InputAvailable_m2DFC0C65CF2EE1843ED10B7DF610A40CF109F5DA(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_2 = Input_get_touchCount_m497E19AA4FA22DB659F631B20FAEF65572D1B44E(/*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_3 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  L_4 = Input_GetTouch_m8082D8EE3A187488373CE6AC66A70B0AAD7CC23F(0, /*hidden argument*/NULL);
		V_2 = L_4;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)(&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5(L_3, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = V_0;
		float L_9 = L_8.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_0;
		float L_11 = L_10.get_y_3();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_12), L_9, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_tB21970F986016656D66D2922594F336E1EE7D5C7_il2cpp_TypeInfo_var);
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_13 = Physics2D_OverlapPoint_m9DDCF583FA2141D3DC89CF00393DF00806E72FC2(L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_14 = __this->get_GridMapManagerObject_24();
		NullCheck(L_14);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_15 = GridMapManager_GetSelectedHexagon_m82E2459ADA9669F14C94801054CBB147501B7A4C_inline(L_14, /*hidden argument*/NULL);
		__this->set_selectedHexagon_26(L_15);
		InputGetter_DetectingTouch_m245D4ED56A833ABB012042EAECE74957B6D22DEA(__this, /*hidden argument*/NULL);
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_16 = V_1;
		InputGetter_CheckingSelection_m21269E14235F6F39DB2DABD55896D41F561CFBBC(__this, L_16, /*hidden argument*/NULL);
		InputGetter_CheckingRotation_m7CA3205BA425261E6AF8B8F14EE2A6C108AA516E(__this, /*hidden argument*/NULL);
	}

IL_006e:
	{
		return;
	}
}
// System.Void InputGetter::DetectingTouch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputGetter_DetectingTouch_m245D4ED56A833ABB012042EAECE74957B6D22DEA (InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD * __this, const RuntimeMethod* method)
{
	Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  L_0 = Input_GetTouch_m8082D8EE3A187488373CE6AC66A70B0AAD7CC23F(0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Touch_get_phase_m759A61477ECBBD90A57E36F1166EB9340A0FE349((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)(&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		__this->set_isValidTouch_23((bool)1);
		Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  L_2 = Input_GetTouch_m8082D8EE3A187488373CE6AC66A70B0AAD7CC23F(0, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)(&V_0), /*hidden argument*/NULL);
		__this->set_touchStartPosition_25(L_3);
	}

IL_002b:
	{
		return;
	}
}
// System.Void InputGetter::CheckingSelection(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputGetter_CheckingSelection_m21269E14235F6F39DB2DABD55896D41F561CFBBC (InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD * __this, Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InputGetter_CheckingSelection_m21269E14235F6F39DB2DABD55896D41F561CFBBC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_0 = ___collider0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004c;
		}
	}
	{
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_2 = ___collider0;
		NullCheck(L_2);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = Component_get_tag_mA183075586ED6BFA81D303804359AE6B02C477CC(L_3, /*hidden argument*/NULL);
		bool L_5 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_4, _stringLiteral08596D60C57FD87E530CBDD2A06052B06D8C4137, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  L_6 = Input_GetTouch_m8082D8EE3A187488373CE6AC66A70B0AAD7CC23F(0, /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = Touch_get_phase_m759A61477ECBBD90A57E36F1166EB9340A0FE349((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_004c;
		}
	}
	{
		bool L_8 = __this->get_isValidTouch_23();
		if (!L_8)
		{
			goto IL_004c;
		}
	}
	{
		__this->set_isValidTouch_23((bool)0);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_9 = __this->get_GridMapManagerObject_24();
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_10 = ___collider0;
		NullCheck(L_9);
		GridMapManager_SelectHexagon_mC0DB6D9C605D20DCC436F24D10DDB411040CB9B6(L_9, L_10, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Void InputGetter::CheckingRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputGetter_CheckingRotation_m7CA3205BA425261E6AF8B8F14EE2A6C108AA516E (InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InputGetter_CheckingRotation_m7CA3205BA425261E6AF8B8F14EE2A6C108AA516E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_4;
	memset((&V_4), 0, sizeof(V_4));
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	int32_t G_B7_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B8_1 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	int32_t G_B14_0 = 0;
	{
		Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  L_0 = Input_GetTouch_m8082D8EE3A187488373CE6AC66A70B0AAD7CC23F(0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Touch_get_phase_m759A61477ECBBD90A57E36F1166EB9340A0FE349((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0111;
		}
	}
	{
		bool L_2 = __this->get_isValidTouch_23();
		if (!L_2)
		{
			goto IL_0111;
		}
	}
	{
		Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8  L_3 = Input_GetTouch_m8082D8EE3A187488373CE6AC66A70B0AAD7CC23F(0, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = Touch_get_position_m2E60676112DA3628CF2DC76418A275C7FE521D8F((Touch_tAACD32535FF3FE5DD91125E0B6987B93C68D2DE8 *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_4;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = V_1;
		float L_6 = L_5.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_7 = __this->get_address_of_touchStartPosition_25();
		float L_8 = L_7->get_x_0();
		V_2 = ((float)il2cpp_codegen_subtract((float)L_6, (float)L_8));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = V_1;
		float L_10 = L_9.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_11 = __this->get_address_of_touchStartPosition_25();
		float L_12 = L_11->get_y_1();
		V_3 = ((float)il2cpp_codegen_subtract((float)L_10, (float)L_12));
		float L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_14 = fabsf(L_13);
		if ((((float)L_14) > ((float)(5.0f))))
		{
			goto IL_0071;
		}
	}
	{
		float L_15 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_16 = fabsf(L_15);
		if ((!(((float)L_16) > ((float)(5.0f)))))
		{
			goto IL_0111;
		}
	}

IL_0071:
	{
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_17 = __this->get_selectedHexagon_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_17, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0111;
		}
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_19 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_20 = __this->get_selectedHexagon_26();
		NullCheck(L_20);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Camera_WorldToScreenPoint_m880F9611E4848C11F21FDF1A1D307B401C61B1BF(L_19, L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		float L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_25 = fabsf(L_24);
		float L_26 = V_3;
		float L_27 = fabsf(L_26);
		int32_t L_28 = ((((float)L_25) > ((float)L_27))? 1 : 0);
		G_B6_0 = L_28;
		if (L_28)
		{
			G_B7_0 = L_28;
			goto IL_00b9;
		}
	}
	{
		float L_29 = V_3;
		G_B8_0 = ((((float)L_29) > ((float)(0.0f)))? 1 : 0);
		G_B8_1 = G_B6_0;
		goto IL_00c1;
	}

IL_00b9:
	{
		float L_30 = V_2;
		G_B8_0 = ((((float)L_30) > ((float)(0.0f)))? 1 : 0);
		G_B8_1 = G_B7_0;
	}

IL_00c1:
	{
		V_5 = (bool)G_B8_0;
		int32_t L_31 = G_B8_1;
		G_B9_0 = L_31;
		if (L_31)
		{
			G_B10_0 = L_31;
			goto IL_00d7;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_32 = V_1;
		float L_33 = L_32.get_x_0();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34 = V_4;
		float L_35 = L_34.get_x_2();
		G_B11_0 = ((((float)L_33) > ((float)L_35))? 1 : 0);
		G_B11_1 = G_B9_0;
		goto IL_00e6;
	}

IL_00d7:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = V_1;
		float L_37 = L_36.get_y_1();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_38 = V_4;
		float L_39 = L_38.get_y_3();
		G_B11_0 = ((((float)L_37) > ((float)L_39))? 1 : 0);
		G_B11_1 = G_B10_0;
	}

IL_00e6:
	{
		V_6 = (bool)G_B11_0;
		if (G_B11_1)
		{
			goto IL_00f5;
		}
	}
	{
		bool L_40 = V_5;
		bool L_41 = V_6;
		G_B14_0 = ((((int32_t)((((int32_t)L_40) == ((int32_t)L_41))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00fb;
	}

IL_00f5:
	{
		bool L_42 = V_5;
		bool L_43 = V_6;
		G_B14_0 = ((((int32_t)L_42) == ((int32_t)L_43))? 1 : 0);
	}

IL_00fb:
	{
		V_7 = (bool)G_B14_0;
		__this->set_isValidTouch_23((bool)0);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_44 = __this->get_GridMapManagerObject_24();
		bool L_45 = V_7;
		NullCheck(L_44);
		GridMapManager_RotateHexagons_mAD1E1D587794465A4189001F3FB4E15685182BF8(L_44, L_45, /*hidden argument*/NULL);
	}

IL_0111:
	{
		return;
	}
}
// System.Void InputGetter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputGetter__ctor_mE1C1AA3E275CCFF7B2E0BEDFB8CBB76F8C61113C (InputGetter_t772AE9622E907AA051158740FBC4592BF81971AD * __this, const RuntimeMethod* method)
{
	{
		PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PropertiesClass::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE (PropertiesClass_t7F6088467E5D3E8DBD98C6B00E5FB0ABEA29AF63 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_0), (0.685f), (0.685f), (0.685f), /*hidden argument*/NULL);
		__this->set_Hexagon_Outline_Scale_21(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_1), (0.0f), (5.5f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_1, /*hidden argument*/NULL);
		__this->set_Hexagon_Start_Position_22(L_2);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void StartMenuManager::ChangeScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartMenuManager_ChangeScene_m5E8158A6EE2A8FBECA35BD8F50A8FB0BFD4E3755 (StartMenuManager_t2908FBD03EFB4C62AB2BEC6BB723A8063C085637 * __this, String_t* ___sceneName0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___sceneName0;
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartMenuManager::QuitGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartMenuManager_QuitGame_m406694FBE1403CE9CDC6F8EFE730F08B427D4BD0 (StartMenuManager_t2908FBD03EFB4C62AB2BEC6BB723A8063C085637 * __this, const RuntimeMethod* method)
{
	{
		Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95(/*hidden argument*/NULL);
		return;
	}
}
// System.Void StartMenuManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartMenuManager__ctor_mEFFC9A4F8FCA7C0577E735261F26CC6A33AD887F (StartMenuManager_t2908FBD03EFB4C62AB2BEC6BB723A8063C085637 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_Awake_mA946658D43E2FC2C3479BB9322CED4FF4C5D2EA0 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_Awake_mA946658D43E2FC2C3479BB9322CED4FF4C5D2EA0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * L_0 = ((UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields*)il2cpp_codegen_static_fields_for(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var))->get_instance_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		((UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields*)il2cpp_codegen_static_fields_for(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var))->set_instance_34(__this);
		return;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_Start_mE176851C74E87A3EBAF5D28B5BFC4D8426D91397 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_Start_mE176851C74E87A3EBAF5D28B5BFC4D8426D91397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_bombCount_37(0);
		IL2CPP_RUNTIME_CLASS_INIT(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_0 = ((GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_StaticFields*)il2cpp_codegen_static_fields_for(GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773_il2cpp_TypeInfo_var))->get_instance_23();
		__this->set_GridManagerObject_33(L_0);
		__this->set_removedHexagons_36(0);
		__this->set_colorCount_35(7);
		UIManager_DefaultSettings_m1E07491312E42FF549B5E429DFB3CDCD025E01A9(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_height_39();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_2);
		String_t* L_4 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralD183CEB428C06D7EF5C8DEDEEAC4246671FBD944, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_width_38();
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_6);
		String_t* L_8 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralBEE830DEDB0CB719867FB0342E3E97C80EE48B62, L_7, /*hidden argument*/NULL);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_Update_mEA6AD9AEDC70385E271BEDCF20C0F4CBD5AC9E7C (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isClick_31();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		UIManager_StartGameButton_m81242AD25701DFE9D8D263296534705B5750DD09(__this, /*hidden argument*/NULL);
		__this->set_isClick_31((bool)0);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UIManager::BackButton(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_BackButton_m43ACD47CEDD88A949C13D3570C8A0E6D5DB0C10D (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, String_t* ___sceneName0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___sceneName0;
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::WidthDropdownChange(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_WidthDropdownChange_mF2FD08C9684FAAE87824DC13C41521AF6FDA0FF5 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___dropdown0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_WidthDropdownChange_mF2FD08C9684FAAE87824DC13C41521AF6FDA0FF5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_0 = ___dropdown0;
		NullCheck(L_0);
		int32_t L_1 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		__this->set_width_38(5);
		goto IL_0058;
	}

IL_0011:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_2 = ___dropdown0;
		NullCheck(L_2);
		int32_t L_3 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0023;
		}
	}
	{
		__this->set_width_38(6);
		goto IL_0058;
	}

IL_0023:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_4 = ___dropdown0;
		NullCheck(L_4);
		int32_t L_5 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0035;
		}
	}
	{
		__this->set_width_38(7);
		goto IL_0058;
	}

IL_0035:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_6 = ___dropdown0;
		NullCheck(L_6);
		int32_t L_7 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0047;
		}
	}
	{
		__this->set_width_38(8);
		goto IL_0058;
	}

IL_0047:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_8 = ___dropdown0;
		NullCheck(L_8);
		int32_t L_9 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)4))))
		{
			goto IL_0058;
		}
	}
	{
		__this->set_width_38(((int32_t)9));
	}

IL_0058:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_10 = ___dropdown0;
		NullCheck(L_10);
		int32_t L_11 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_10, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_12);
		String_t* L_14 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral9D55681DD84F4176A643D69263854D33720B07E9, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_14, /*hidden argument*/NULL);
		int32_t L_15 = __this->get_width_38();
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_16);
		String_t* L_18 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralBEE830DEDB0CB719867FB0342E3E97C80EE48B62, L_17, /*hidden argument*/NULL);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::HeightDropdownChange(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_HeightDropdownChange_mE3515FC50FF564F9A87A4BBED4DA0BF66334AA61 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___dropdown0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_HeightDropdownChange_mE3515FC50FF564F9A87A4BBED4DA0BF66334AA61_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_0 = ___dropdown0;
		NullCheck(L_0);
		int32_t L_1 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		__this->set_height_39(5);
		goto IL_00aa;
	}

IL_0014:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_2 = ___dropdown0;
		NullCheck(L_2);
		int32_t L_3 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0029;
		}
	}
	{
		__this->set_height_39(6);
		goto IL_00aa;
	}

IL_0029:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_4 = ___dropdown0;
		NullCheck(L_4);
		int32_t L_5 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_003b;
		}
	}
	{
		__this->set_height_39(7);
		goto IL_00aa;
	}

IL_003b:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_6 = ___dropdown0;
		NullCheck(L_6);
		int32_t L_7 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_004d;
		}
	}
	{
		__this->set_height_39(8);
		goto IL_00aa;
	}

IL_004d:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_8 = ___dropdown0;
		NullCheck(L_8);
		int32_t L_9 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)4))))
		{
			goto IL_0060;
		}
	}
	{
		__this->set_height_39(((int32_t)9));
		goto IL_00aa;
	}

IL_0060:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_10 = ___dropdown0;
		NullCheck(L_10);
		int32_t L_11 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_10, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)5))))
		{
			goto IL_0073;
		}
	}
	{
		__this->set_height_39(((int32_t)10));
		goto IL_00aa;
	}

IL_0073:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_12 = ___dropdown0;
		NullCheck(L_12);
		int32_t L_13 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_12, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)6))))
		{
			goto IL_0086;
		}
	}
	{
		__this->set_height_39(((int32_t)11));
		goto IL_00aa;
	}

IL_0086:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_14 = ___dropdown0;
		NullCheck(L_14);
		int32_t L_15 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_14, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)7))))
		{
			goto IL_0099;
		}
	}
	{
		__this->set_height_39(((int32_t)12));
		goto IL_00aa;
	}

IL_0099:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_16 = ___dropdown0;
		NullCheck(L_16);
		int32_t L_17 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_16, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)8))))
		{
			goto IL_00aa;
		}
	}
	{
		__this->set_height_39(((int32_t)13));
	}

IL_00aa:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_18 = ___dropdown0;
		NullCheck(L_18);
		int32_t L_19 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_18, /*hidden argument*/NULL);
		int32_t L_20 = L_19;
		RuntimeObject * L_21 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_20);
		String_t* L_22 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral9711DD29F19F33DC805E32FBF54610C089EF6DD5, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_22, /*hidden argument*/NULL);
		int32_t L_23 = __this->get_height_39();
		int32_t L_24 = L_23;
		RuntimeObject * L_25 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_24);
		String_t* L_26 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralD183CEB428C06D7EF5C8DEDEEAC4246671FBD944, L_25, /*hidden argument*/NULL);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::ColorDropdownChanged(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_ColorDropdownChanged_m66F922A95A52AE43900A370B06F4A0D15114B189 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___dropdown0, const RuntimeMethod* method)
{
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_0 = ___dropdown0;
		NullCheck(L_0);
		int32_t L_1 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		__this->set_color_40(5);
		return;
	}

IL_0010:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_2 = ___dropdown0;
		NullCheck(L_2);
		int32_t L_3 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		__this->set_color_40(6);
		return;
	}

IL_0021:
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_4 = ___dropdown0;
		NullCheck(L_4);
		int32_t L_5 = Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0031;
		}
	}
	{
		__this->set_color_40(7);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UIManager::StartGameButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_StartGameButton_m81242AD25701DFE9D8D263296534705B5750DD09 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_StartGameButton_m81242AD25701DFE9D8D263296534705B5750DD09_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_settingsScreen_28();
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, (bool)0, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = __this->get_informationPanel_29();
		NullCheck(L_1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_2, (bool)1, /*hidden argument*/NULL);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_3 = __this->get_GridManagerObject_33();
		int32_t L_4 = __this->get_height_39();
		NullCheck(L_3);
		GridMapManager_SetGridHeight_m89865CCB85B689CA751046765A238E10A794F509_inline(L_3, L_4, /*hidden argument*/NULL);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_5 = __this->get_GridManagerObject_33();
		int32_t L_6 = __this->get_width_38();
		NullCheck(L_5);
		GridMapManager_SetGridWidth_mCDC775F41010E50C72D4C68A0643CF7E0D3B904B_inline(L_5, L_6, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_color_40();
		if ((!(((uint32_t)L_7) == ((uint32_t)5))))
		{
			goto IL_009d;
		}
	}
	{
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_8 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_9 = Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905(/*hidden argument*/NULL);
		NullCheck(L_8);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_8, L_9, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_10 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_11 = Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957(/*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_10, L_11, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_12 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_13 = Color_get_yellow_mC8BD62CCC364EA5FC4273D4C2E116D0E2DE135AE(/*hidden argument*/NULL);
		NullCheck(L_12);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_12, L_13, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_14 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_15 = Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0(/*hidden argument*/NULL);
		NullCheck(L_14);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_14, L_15, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_16 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_17 = Color_get_magenta_m04E2DDB63AA6288C701A93E248643A06EBD2D7AD(/*hidden argument*/NULL);
		NullCheck(L_16);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_16, L_17, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		goto IL_0181;
	}

IL_009d:
	{
		int32_t L_18 = __this->get_color_40();
		if ((!(((uint32_t)L_18) == ((uint32_t)6))))
		{
			goto IL_0108;
		}
	}
	{
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_19 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_20 = Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905(/*hidden argument*/NULL);
		NullCheck(L_19);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_19, L_20, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_21 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_22 = Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957(/*hidden argument*/NULL);
		NullCheck(L_21);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_21, L_22, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_23 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_24 = Color_get_yellow_mC8BD62CCC364EA5FC4273D4C2E116D0E2DE135AE(/*hidden argument*/NULL);
		NullCheck(L_23);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_23, L_24, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_25 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_26 = Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0(/*hidden argument*/NULL);
		NullCheck(L_25);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_25, L_26, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_27 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_28 = Color_get_magenta_m04E2DDB63AA6288C701A93E248643A06EBD2D7AD(/*hidden argument*/NULL);
		NullCheck(L_27);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_27, L_28, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_29 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_30 = Color_get_blue_m5449DCBB31EEB2324489989754C00123982EBABA(/*hidden argument*/NULL);
		NullCheck(L_29);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_29, L_30, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		goto IL_0181;
	}

IL_0108:
	{
		int32_t L_31 = __this->get_color_40();
		if ((!(((uint32_t)L_31) == ((uint32_t)7))))
		{
			goto IL_0181;
		}
	}
	{
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_32 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_33 = Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905(/*hidden argument*/NULL);
		NullCheck(L_32);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_32, L_33, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_34 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_35 = Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957(/*hidden argument*/NULL);
		NullCheck(L_34);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_34, L_35, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_36 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_37 = Color_get_yellow_mC8BD62CCC364EA5FC4273D4C2E116D0E2DE135AE(/*hidden argument*/NULL);
		NullCheck(L_36);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_36, L_37, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_38 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_39 = Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0(/*hidden argument*/NULL);
		NullCheck(L_38);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_38, L_39, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_40 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_41 = Color_get_magenta_m04E2DDB63AA6288C701A93E248643A06EBD2D7AD(/*hidden argument*/NULL);
		NullCheck(L_40);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_40, L_41, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_42 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_43 = Color_get_blue_m5449DCBB31EEB2324489989754C00123982EBABA(/*hidden argument*/NULL);
		NullCheck(L_42);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_42, L_43, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_44 = __this->get_colors_32();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_45 = Color_get_cyan_m4E9C84C7E1003311C2D4BDB281F2D11DF5F7FDE2(/*hidden argument*/NULL);
		NullCheck(L_44);
		List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C(L_44, L_45, /*hidden argument*/List_1_Add_m00234E3A8CBBD2118C5149D6AEDCB9D69371D57C_RuntimeMethod_var);
	}

IL_0181:
	{
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_46 = __this->get_GridManagerObject_33();
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_47 = __this->get_colors_32();
		NullCheck(L_46);
		GridMapManager_SetColorList_mBF555D15D608B2AE94B83811BDFC9DF798FF7651_inline(L_46, L_47, /*hidden argument*/NULL);
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_48 = __this->get_GridManagerObject_33();
		NullCheck(L_48);
		GridMapManager_CreateGrid_mFFF3421D90C288DB3CA1F87BA5A87AB56FBED4B9(L_48, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::DefaultSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_DefaultSettings_m1E07491312E42FF549B5E429DFB3CDCD025E01A9 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	{
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_0 = __this->get_heightDropdown_26();
		NullCheck(L_0);
		Dropdown_set_value_m155A45649AB62AC1B7AB10213EA556F22E8E91F3(L_0, 4, /*hidden argument*/NULL);
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_1 = __this->get_widthDropdown_25();
		NullCheck(L_1);
		Dropdown_set_value_m155A45649AB62AC1B7AB10213EA556F22E8E91F3(L_1, 3, /*hidden argument*/NULL);
		Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * L_2 = __this->get_colorCountDropdown_27();
		NullCheck(L_2);
		Dropdown_set_value_m155A45649AB62AC1B7AB10213EA556F22E8E91F3(L_2, 0, /*hidden argument*/NULL);
		__this->set_color_40(5);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_3 = __this->get_score_23();
		int32_t* L_4 = __this->get_address_of_removedHexagons_36();
		String_t* L_5 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_5);
		return;
	}
}
// System.Void UIManager::Score(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_Score_m88AA8E3F6C04A7C3C11EF47F2A8D65E72EFEF63D (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, int32_t ____Score0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_removedHexagons_36();
		int32_t L_1 = ____Score0;
		__this->set_removedHexagons_36(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)));
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_2 = __this->get_score_23();
		int32_t L_3 = __this->get_removedHexagons_36();
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)5, (int32_t)L_3));
		String_t* L_4 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_5 = __this->get_score_23();
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(72 /* System.String UnityEngine.UI.Text::get_text() */, L_5);
		int32_t L_7 = Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA(L_6, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_bombCount_37();
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)1000), (int32_t)L_8)), (int32_t)((int32_t)1000))))))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_9 = __this->get_bombCount_37();
		__this->set_bombCount_37(((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)));
		GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * L_10 = __this->get_GridManagerObject_33();
		NullCheck(L_10);
		GridMapManager_SetBombProduction_mC86F578E5457E53A98856BB567B5EA081BED1BE2(L_10, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void UIManager::GameEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager_GameEnd_mA5D7B91BBE6BDDBC191E2860910EB1A927F8E6EB (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = __this->get_gameOverPanel_30();
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_1, (bool)1, /*hidden argument*/NULL);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_2 = __this->get_score2_24();
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_3 = __this->get_score_23();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(72 /* System.String UnityEngine.UI.Text::get_text() */, L_3);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		return;
	}
}
// System.Void UIManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIManager__ctor_m40CA6521CEDDF979D58B6050A6D294A32A1CEA69 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager__ctor_m40CA6521CEDDF979D58B6050A6D294A32A1CEA69_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_0 = (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 *)il2cpp_codegen_object_new(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86_il2cpp_TypeInfo_var);
		List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E(L_0, /*hidden argument*/List_1__ctor_mDDD30BB5143BE3D06A3674869D02E6D9A9EFF76E_RuntimeMethod_var);
		__this->set_colors_32(L_0);
		__this->set_width_38(8);
		__this->set_height_39(((int32_t)9));
		__this->set_color_40(5);
		PropertiesClass__ctor_m7D921B57B0E4D04F715F0E616CE62E11451889FE(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t GridMapManager_GetGridHeight_m76516C97F13FF377E0E91F5A48EA948867E5C41B_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_gridHeight_30();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t GridMapManager_GetGridWidth_m4DF42E736F4B0984D7E727901ACF84405DD895BA_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_gridWidth_29();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t HexagonManager_GetX_m8DE2CFA37B64286D27A6FC7528052ADB9DDD7F48_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_x_24();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t HexagonManager_GetY_m3B711DA0B2F3DAA2347CFE9AD3D7AD597DCB8B2D_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_y_25();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t HexagonManager_GetTimer_mEE88C69BB650E3740B5B30D59302877ED000E980_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_bombTimer_30();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HexagonManager_SetY_m67E6A395342D87F427D4EE382A82AD1D7CAF0CFD_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_y_25(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void HexagonManager_SetX_m479990D055BBF89BDF258489087EFC8AD084CC0C_inline (HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_x_24(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * GridMapManager_GetSelectedHexagon_m82E2459ADA9669F14C94801054CBB147501B7A4C_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, const RuntimeMethod* method)
{
	{
		HexagonManager_t6F19BAEBC89F380C6734D166100AFA3560476009 * L_0 = __this->get_selectedHexagon_35();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_mF388FA389F2A050264AA87E61D4F9AFC41F48873_inline (Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_Value_25();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void GridMapManager_SetGridHeight_m89865CCB85B689CA751046765A238E10A794F509_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, int32_t ___height0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___height0;
		__this->set_gridHeight_30(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void GridMapManager_SetGridWidth_mCDC775F41010E50C72D4C68A0643CF7E0D3B904B_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, int32_t ___width0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___width0;
		__this->set_gridWidth_29(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void GridMapManager_SetColorList_mBF555D15D608B2AE94B83811BDFC9DF798FF7651_inline (GridMapManager_tEDD9654F3ACD194E757015D53DCF8C3A0EBEA773 * __this, List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * ___list0, const RuntimeMethod* method)
{
	{
		List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * L_0 = ___list0;
		__this->set_colorList_39(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_m88A0089A1A4EEBC3017E2DA569A01C7919B10945_gshared_inline (Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m01FE6DA781A0E8D1656FE2780557BE6E20131F60_gshared_inline (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  List_1_get_Item_mD8823BFDE104583F7085C236E7FE924F9E1D4EBB_gshared_inline (List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_2 = (ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)L_2, (int32_t)L_3);
		return L_4;
	}
}
