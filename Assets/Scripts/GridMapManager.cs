﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMapManager : PropertiesClass
{
    public static GridMapManager instance = null;

	/* Variables to assign from editor */
	public GameObject hexPrefab;
	public GameObject hexParent;
	public GameObject outParent;
	public Sprite outlineSprite;
	public Sprite HexagonSprite;

	/* Member variables */
	private int gridWidth;
	private int gridHeight;
	private int selectionStatus;
	private bool bombProduction;
	private bool gameOver;
	private Vector2 selectedHexagonPosition;
	private HexagonManager selectedHexagon;
	private List<List<HexagonManager>> gameGrid;
	private List<HexagonManager> selectedGroup;
	private List<HexagonManager> bombs;
	private List<Color> colorList;

	/* Coroutine status variables */
	private bool HexagonRotationStatus;
	private bool HexagonExplosionStatus;
	private bool HexagonProductionStatus;


	/* Assign singleton if avail<able */
	void Awake()
	{
		if (instance == null)
			instance = this;
		else
			Destroy(this);
	}

	void Start()
	{
		
		gameOver = false;
		bombProduction = false;
		HexagonRotationStatus = false;
		HexagonExplosionStatus = false;
		HexagonProductionStatus = false;

		bombs = new List<HexagonManager>();
		selectedGroup = new List<HexagonManager>();
		gameGrid = new List<List<HexagonManager>>();
	}


	/* Grid creator coroutine. Width and height should be set before this call */
	public void CreateGrid()
	{
		List<int> missingCells = new List<int>(); //Removed hexagons cells


		/* Create gameGrid and fill missingCells */

		for (int i = 0; i < GetGridWidth(); ++i)
		{
			for (int j = 0; j < GetGridHeight(); ++j)
            {
				missingCells.Add(i);
			}
			gameGrid.Add(new List<HexagonManager>());
		}

		/* Fill grid with Hexagons */

		StartCoroutine(ProduceHexagon(missingCells, ColoredGridProduction()));
	}

	/* Select the hexagon group on touch position, returns the selected Hexagons*/
	public void SelectHexagon(Collider2D collider)
	{
		/* If selection is different than current hex, reset status and variable */
		if (selectedHexagon == null || !selectedHexagon.GetComponent<Collider2D>().Equals(collider))
		{
			selectedHexagon = collider.gameObject.GetComponent<HexagonManager>();
			selectedHexagonPosition.x = selectedHexagon.GetX();
			selectedHexagonPosition.y = selectedHexagon.GetY();
			selectionStatus = 0;
		}

		/* Else increase selection status without exceeding total number */
		else
		{
			selectionStatus = (++selectionStatus) % Selection_Status_Count;
            /*1 mod 6 = 1
             2 mod 6 = 2 ....
             6 mod 6 =0
             */

        //selectionStatus help for deciding of selectedHexagon neigbours 
		}

		DestroyOutline(); //destroy selected hexagons outline
		CreateOutline(); //create new hexagons outline
	}

	/* Rotate the hexagon group on touch position */
	public void RotateHexagons(bool clockWise)
	{
		/* Specifying that rotation started and destroying outline*/
		DestroyOutline();
		StartCoroutine(RotationCheckingCoroutine(clockWise));
	}

	#region Select 3 Hexagons
	/* For SelectHexagon() to find all 3 Hexagon to be outlined */
	private void FindHexagonGroup()
	{
		Vector2 firstPos, secondPos;

		/* Find 2 other required Hexagons coordinates on grid */

		selectedHexagon = gameGrid[(int)selectedHexagonPosition.x][(int)selectedHexagonPosition.y];


		/*Finding neighbours positions*/
		HexagonManager.NeighbourHexes neighbours = selectedHexagon.GetNeighbours();
		bool flag = false;


		/* Picking correct neighbour according to selection position */
		do
		{
			switch (selectionStatus)
			{
				case 0:
					firstPos = neighbours.up; secondPos = neighbours.upRight;
					break;
				case 1:
					firstPos = neighbours.upRight; secondPos = neighbours.downRight;
					break;
				case 2:
					firstPos = neighbours.downRight; secondPos = neighbours.down;
					break;
				case 3:
					firstPos = neighbours.down; secondPos = neighbours.downLeft;
					break;
				case 4:
					firstPos = neighbours.downLeft; secondPos = neighbours.upLeft;
					break;
				case 5:
					firstPos = neighbours.upLeft; secondPos = neighbours.up;
					break;
				default:
					firstPos = Vector2.zero; secondPos = Vector2.zero;
					break;
			}

			/* Finding two neighbours with valid positions */
			if (firstPos.x < 0 || firstPos.x >= gridWidth || firstPos.y < 0 || firstPos.y >= gridHeight || secondPos.x < 0 || secondPos.x >= gridWidth || secondPos.y < 0 || secondPos.y >= gridHeight)
			{
				selectionStatus = (++selectionStatus) % Selection_Status_Count;
			}
			else
			{
				flag = true;
			}
		} while (!flag);

		selectedGroup.Clear(); //If previously selectedGroup exists, there should be clear
		selectedGroup.Add(selectedHexagon);
		selectedGroup.Add(gameGrid[(int)firstPos.x][(int)firstPos.y].GetComponent<HexagonManager>());
		selectedGroup.Add(gameGrid[(int)secondPos.x][(int)secondPos.y].GetComponent<HexagonManager>());
	}

	#endregion

	#region Rotation Operations

	/* Check until all Hexagons finished rotating */
	private IEnumerator RotationCheckingCoroutine(bool clockWise)
	{
		List<HexagonManager> explosiveHexagon = null;
		bool flag = true;


		/* Rotate selected group until an matching explosive Hexagons found.
         * If it doesnt find matching hexagons, hexagon groups rotate all rotation */

		HexagonRotationStatus = true;

		for (int i = 0; i < selectedGroup.Count; ++i)
		{
			/* Swap Hexagons and wait until they are completed rotation */
			SwapHexagon(clockWise);
			yield return new WaitForSeconds(0.3f);

			/* Check if any explosion is available, break loop if it is */
			explosiveHexagon = CheckingExplosion(gameGrid);

			if (explosiveHexagon.Count > 0)
			{
				break;
			}
		}


		/* Rotation ends and explosion starts */
		HexagonExplosionStatus = true;
		HexagonRotationStatus = false;


		/* Explode the Hexagon until no explosive Hexagon are available */
		while (explosiveHexagon.Count > 0)
		{
			if (flag)
			{
                //Produce new Hexagons in place of Exploded Hexagons
				HexagonProductionStatus = true;
				StartCoroutine(ProduceHexagon(DetectBombANDClearExplodeHexagon(explosiveHexagon)));
				flag = false;
			}

			else if (!HexagonProductionStatus) //Explosion operations
			{
				explosiveHexagon= CheckingExplosion(gameGrid);
				flag = true;
			}

			yield return new WaitForSeconds(0.3f);
		}

		HexagonExplosionStatus = false;
		FindHexagonGroup();
		CreateOutline();
	}

   
    

	/* Swap positions of selected 3 Hexagons*/
	private void SwapHexagon(bool clockWise)
	{
		int x1, x2, x3, y1, y2, y3;
		Vector2 pos1, pos2, pos3;
		HexagonManager hex1, hex2, hex3;



		/* Taking each positions in local variable SelectedGroup to prevent data loss during rotation */
		hex1 = selectedGroup[0];
		hex2 = selectedGroup[1];
		hex3 = selectedGroup[2];



		x1 = hex1.GetX();
		x2 = hex2.GetX();
		x3 = hex3.GetX();

		y1 = hex1.GetY();
		y2 = hex2.GetY();
		y3 = hex3.GetY();

		pos1 = hex1.transform.position;
		pos2 = hex2.transform.position;
		pos3 = hex3.transform.position;


		/* If rotation is clokwise, rotate the position of hexagon on next index, else rotate to previous index */
		if (clockWise)
		{
			hex1.Rotate(x2, y2, pos2);
			gameGrid[x2][y2] = hex1;

			hex2.Rotate(x3, y3, pos3);
			gameGrid[x3][y3] = hex2;

			hex3.Rotate(x1, y1, pos1);
			gameGrid[x1][y1] = hex3;
		}
		else
		{
			hex1.Rotate(x3, y3, pos3);
			gameGrid[x3][y3] = hex1;

			hex2.Rotate(x1, y1, pos1);
			gameGrid[x1][y1] = hex2;

			hex3.Rotate(x2, y2, pos2);
			gameGrid[x2][y2] = hex3;
		}
	}
	#endregion

	#region Explosion Operations
	/* Returns a list that contains Hexagons which are ready to explode OR returns an empty list if there is none */
	private List<HexagonManager> CheckingExplosion(List<List<HexagonManager>> checkingList)
	{
		List<HexagonManager> neighbourHexagonsList = new List<HexagonManager>();
		List<HexagonManager> explosiveHexagonList = new List<HexagonManager>();
		HexagonManager currentHexagon;
		HexagonManager.NeighbourHexes currentNeighbours;
		Color currentColor;


		for (int i = 0; i < checkingList.Count; ++i)
		{
			for (int j = 0; j < checkingList[i].Count; ++j)
			{
				/* Current Hexagon informations */
				currentHexagon = checkingList[i][j];
				currentColor = currentHexagon.GetColor();
				currentNeighbours = currentHexagon.GetNeighbours();

				/* Fill neighbours list with up-upright-downright neighbours to valid positions */
				if (IsValid(currentNeighbours.up))
                {
					neighbourHexagonsList.Add(gameGrid[(int)currentNeighbours.up.x][(int)currentNeighbours.up.y]);
				}
					
				else
                {
					neighbourHexagonsList.Add(null);
				}


				if (IsValid(currentNeighbours.upRight))
				{
                    neighbourHexagonsList.Add(gameGrid[(int)currentNeighbours.upRight.x][(int)currentNeighbours.upRight.y]);
                }
				else
                {
					neighbourHexagonsList.Add(null);
				}

				if (IsValid(currentNeighbours.downRight))
				{
					neighbourHexagonsList.Add(gameGrid[(int)currentNeighbours.downRight.x][(int)currentNeighbours.downRight.y]);
				} 
				else
                {
					neighbourHexagonsList.Add(null);
				}
					

				/* If current 3 Hexagons are same color, add them to explosions list */
				for (int k = 0; k < neighbourHexagonsList.Count - 1; ++k)
				{
					if (neighbourHexagonsList[k] != null && neighbourHexagonsList[k + 1] != null)
					{
						if (neighbourHexagonsList[k].GetColor() == currentColor && neighbourHexagonsList[k + 1].GetColor() == currentColor)
						{
							if (!explosiveHexagonList.Contains(neighbourHexagonsList[k]))
                            {
								explosiveHexagonList.Add(neighbourHexagonsList[k]);
							}
								
							if (!explosiveHexagonList.Contains(neighbourHexagonsList[k + 1]))
                            {
								explosiveHexagonList.Add(neighbourHexagonsList[k + 1]);
							}
								
							if (!explosiveHexagonList.Contains(currentHexagon))
                            {
								explosiveHexagonList.Add(currentHexagon);
							}
								
						}
					}
				}

				neighbourHexagonsList.Clear();
			}
		}


		return explosiveHexagonList;
	}



	/* Detact bomb, Clear explosive Hexagons and repair the grid */
	private List<int> DetectBombANDClearExplodeHexagon(List<HexagonManager> list)
	{
		List<int> missingColumns = new List<int>();
		float positionX, positionY;


		/* Check for bombs */
		foreach (HexagonManager hexagon in bombs)
		{
			if (!list.Contains(hexagon))
			{
				hexagon.Trigger();
				if (hexagon.GetTimer() == 0)
				{
					gameOver = true;
					UIManager.instance.GameEnd();
					StopAllCoroutines();
					return missingColumns;
				}
			}
		}

		/* Remove Hexagon from game grid */
		foreach (HexagonManager hexagon in list)
		{
			if (bombs.Contains(hexagon))
			{
				bombs.Remove(hexagon);
			}
			UIManager.instance.Score(1);
			gameGrid[hexagon.GetX()].Remove(hexagon);
			missingColumns.Add(hexagon.GetX());
			Destroy(hexagon.gameObject);
		}

		/* Re-assign left Hexagon positions */
		foreach (int i in missingColumns)
		{
			for (int j = 0; j < gameGrid[i].Count; ++j)
			{
				positionX = GetGridStartCoordinateX() + (Hexagon_Distance_Horizontal * i);
				positionY = (Hexagon_Distance_Vertical * j * 2) + Grid_Vertical_Offset + (DetectColumn(i) ? Hexagon_Distance_Vertical : 0);
				gameGrid[i][j].SetY(j);
				gameGrid[i][j].SetX(i);
				gameGrid[i][j].ChangeWorldPosition(new Vector3(positionX, positionY, 0));
			}
		}

		/* Return missing column list */
		HexagonExplosionStatus= false;
		return missingColumns;
	}
	#endregion

	#region Outline Operations
	/* Destroy outline objects */
	private void DestroyOutline()
	{
		if (outParent.transform.childCount > 0)
		{
			foreach (Transform child in outParent.transform)
				Destroy(child.gameObject);
		}
	}



	/* Create outline objects*/
	private void CreateOutline()
	{
		/* Find selected Hexagon group */
		FindHexagonGroup();

		/* Creating outlines on same position with selected Hexagon and making bigger than actual Hexagon*/
		foreach (HexagonManager outlinedHexagon in selectedGroup)
		{
			GameObject gameObjectOutlined = outlinedHexagon.gameObject;
			GameObject outline = new GameObject("Outline");
			GameObject hexagon = new GameObject("Hexagon");

			outline.transform.parent = outParent.transform;

            //Creating outline
			outline.AddComponent<SpriteRenderer>();
			outline.GetComponent<SpriteRenderer>().sprite = outlineSprite;
			outline.GetComponent<SpriteRenderer>().color = Color.white;
			outline.transform.position = new Vector3(gameObjectOutlined.transform.position.x, gameObjectOutlined.transform.position.y, -1);
			outline.transform.localScale = Hexagon_Outline_Scale;

            //Creating hexagon
			hexagon.AddComponent<SpriteRenderer>();
			hexagon.GetComponent<SpriteRenderer>().sprite = HexagonSprite;
			hexagon.GetComponent<SpriteRenderer>().color = gameObjectOutlined.GetComponent<SpriteRenderer>().color;
			hexagon.transform.position = new Vector3(gameObjectOutlined.transform.position.x, gameObjectOutlined.transform.position.y, -2);
			hexagon.transform.localScale = gameObjectOutlined.transform.localScale;
			hexagon.transform.parent = outline.transform;
		}
	}
	#endregion

   

	private IEnumerator ProduceHexagon(List<int> columns, List<List<Color>> color = null)
	{
		Vector3 startPosition;
		float positionX, positionY;
		float startX = GetGridStartCoordinateX();
		bool columnStatus;


		/* Beginning of Hexagon production */
		HexagonProductionStatus = true;

		/* Produce new Hexagon */
		foreach (int i in columns)
		{
			/* Instantiate new Hexagon. This process has delay*/
			columnStatus = DetectColumn(i);
			positionX = startX + (Hexagon_Distance_Horizontal * i);
			positionY = (Hexagon_Distance_Vertical * gameGrid[i].Count * 2) + Grid_Vertical_Offset + (columnStatus ? Hexagon_Distance_Vertical : 0);
			startPosition = new Vector3(positionX, positionY, 0);

			GameObject newObj = Instantiate(hexPrefab, Hexagon_Start_Position, Quaternion.identity, hexParent.transform);
			HexagonManager newHex = newObj.GetComponent<HexagonManager>();
			yield return new WaitForSeconds(0.025f);


			/* Set bomb if production is true */
			if (bombProduction)
			{
				newHex.SetBomb();
				bombs.Add(newHex);
				bombProduction = false;
			}

			/* Set world and grid positions of Hexagon */
			if (color == null)
				newHex.SetColor(colorList[(int)(Random.value * Random_Num) % colorList.Count]);
			else
				newHex.SetColor(color[i][gameGrid[i].Count]);

			newHex.ChangeGridPosition(new Vector2(i, gameGrid[i].Count));
			newHex.ChangeWorldPosition(startPosition);
			gameGrid[i].Add(newHex);
		}

		/* End of Hexagon production */
		HexagonProductionStatus = false;
	}



	/* Produce grid with valid colors */
	private List<List<Color>> ColoredGridProduction()
	{
		List<List<Color>> returnValue = new List<List<Color>>();
		List<Color> checkList = new List<Color>();
		bool flag = true;


		/* Create a color list without ready to explode neighbours */
		for (int i = 0; i < GetGridWidth(); ++i)
		{
			returnValue.Add(new List<Color>());
			for (int j = 0; j < GetGridHeight(); ++j)
			{
				returnValue[i].Add(colorList[(int)(Random.value * Random_Num) % colorList.Count]);
				do
				{
					flag = true;
					returnValue[i][j] = colorList[(int)(Random.value * Random_Num) % colorList.Count];
					if (i - 1 >= 0 && j - 1 >= 0)
					{
						if (returnValue[i][j - 1] == returnValue[i][j] || returnValue[i - 1][j] == returnValue[i][j])
							flag = false;
					}
				} while (!flag);
			}
		}


		return returnValue;
	}


	#region Other Functions
	/* DetectColumn function find column of selecting Hexagon neighbours
	 * midIndex is the index of middle column of the grid
	 */
	public bool DetectColumn(int x)
	{
		int midIndex = GetGridWidth() / 2;
		return (midIndex % 2 == x % 2);
	}

	/* Checking availability if game is ready to take input */
	public bool InputAvailable()
	{
        //if Production stopped, not gameOver, hexagons rotation processes stopped and explosion stopped, input is available
		return !HexagonProductionStatus && !gameOver && !HexagonRotationStatus && !HexagonExplosionStatus;
	}
    


	/* Find x coordinate of the world position of first column */
	private float GetGridStartCoordinateX()
	{
		return gridWidth / 2 * -Hexagon_Distance_Horizontal;
	}



	/* Validate a position if it is in game grid */
	private bool IsValid(Vector2 pos)
	{
		return pos.x >= 0 && pos.x < GetGridWidth() && pos.y >= 0 && pos.y < GetGridHeight();
	}



	#endregion

	/* Setters & Getters */
	public void SetGridWidth(int width) { gridWidth = width; }
	public int GetGridWidth() { return gridWidth; }

	public void SetGridHeight(int height) { gridHeight = height; }
	public int GetGridHeight() { return gridHeight; }

	public void SetColorList(List<Color> list) { colorList = list; }
	public void SetBombProduction() { bombProduction = true; }

	
	public HexagonManager GetSelectedHexagon() { return selectedHexagon; }

}


