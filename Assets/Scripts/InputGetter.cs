﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputGetter : PropertiesClass
{
	private bool isValidTouch;
	private GridMapManager GridMapManagerObject;
	private Vector2 touchStartPosition;
	private HexagonManager selectedHexagon;



	void Start()
	{
		GridMapManagerObject = GridMapManager.instance;
	}

	void Update()
	{
		if (GridMapManagerObject.InputAvailable() && Input.touchCount > 0)
		{
			/* Taking collider of touched object */
			Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			Vector2 touchPos = new Vector2(worldPosition.x, worldPosition.y);
			Collider2D collider = Physics2D.OverlapPoint(touchPos);
			selectedHexagon = GridMapManagerObject.GetSelectedHexagon();

			/* Processing input */
			DetectingTouch();
			CheckingSelection(collider);
			CheckingRotation();
		}
	}



	/* Check if first touch arrived */
	private void DetectingTouch()
	{
		/* Set start poisiton at the beginning of touch[0] */
		if (Input.GetTouch(0).phase == TouchPhase.Began) //If phase is began
		{
			isValidTouch = true;
			touchStartPosition = Input.GetTouch(0).position;
		}
	}



	/* Checks if selection condition provided and calls grid manager to handle selection */
	private void CheckingSelection(Collider2D collider)
	{
		/* If collider exists and its tag match any Hexagon */
		if (collider != null && collider.transform.tag == "Hexagon")
		{
			/* Select hexagon if touch ended */
			if (Input.GetTouch(0).phase == TouchPhase.Ended && isValidTouch)
			{
				isValidTouch = false;
				GridMapManagerObject.SelectHexagon(collider);
			}
		}
	}



	/* Checks if rotation condition provided and calls grid manager to handle rotation */
	private void CheckingRotation()
	{
		if (Input.GetTouch(0).phase == TouchPhase.Moved && isValidTouch)
		{
			Vector2 touchCurrentPosition = Input.GetTouch(0).position;
			float distanceX = touchCurrentPosition.x - touchStartPosition.x;
			float distanceY = touchCurrentPosition.y - touchStartPosition.y;


			/*If rotation triggered by comparing distance between first touch position and current touch position */
			if ((Mathf.Abs(distanceX) > Hexagon_Rotate_Slide_Distance || Mathf.Abs(distanceY) > Hexagon_Rotate_Slide_Distance) && selectedHexagon != null)
			{
				Vector3 screenPosition = Camera.main.WorldToScreenPoint(selectedHexagon.transform.position);

				bool triggerOnX = Mathf.Abs(distanceX) > Mathf.Abs(distanceY); //Specify if rotate action trigger from a horizontal or vertical swipe

				bool swipeRightUp = triggerOnX ? distanceX > 0 : distanceY > 0; //Specify if swipe direction is right or up

				bool touchThanHex = triggerOnX ? touchCurrentPosition.y > screenPosition.y : touchCurrentPosition.x > screenPosition.x; //Specify if touch position value is bigger than hexagon position on triggered axis

                /*If X axis triggers rotation with same direction swipe, rotate clockwise, else rotate counter clockwise
                 If Y axis triggers rotation with opposite direction swipe, rotate clockwise else rotate counter clocwise
                 */

				bool clockWise = triggerOnX ? swipeRightUp == touchThanHex : swipeRightUp != touchThanHex;

				isValidTouch = false;
				GridMapManagerObject.RotateHexagons(clockWise);
			}
		}
	}
}
