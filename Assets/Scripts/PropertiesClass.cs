﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropertiesClass : MonoBehaviour
{
	/* Constant variables */

	protected const int Default_Grid_Width = 8;
	protected const int Default_Grid_Width_Index = 3;

	protected const int Default_Grid_Height = 8;
	protected const int Default_Grid_Height_Index = 4;

	protected const int Default_Color_Count = 5;
	protected const int Default_Color_Count_Index = 0;

	protected const int Selection_Status_Count = 6;

	protected const int Hexagon_Rotate_Slide_Distance = 5;
	protected const int Hexagon_Rotate_Constant = 9;

	protected const int Score_Constant = 5;
	protected const int Random_Num = 75486;
	protected const int Grid_Vertical_Offset = -3;

	protected const int Bomb_Timer_Start = 6;
	protected const int Bomb_Score_Threshold = 1000; //Eşik değer

	protected const float Hexagon_Distance_Horizontal = 0.445f;
	protected const float Hexagon_Distance_Vertical = 0.23f;
	protected const float Hexagon_Rotate_Threshold = 0.05f;



	protected readonly Vector3 Hexagon_Outline_Scale = new Vector3(0.685f, 0.685f, 0.685f);
	protected readonly Vector2 Hexagon_Start_Position = new Vector3(0f, 5.5f, 0f);
}
