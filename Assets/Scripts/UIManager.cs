﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class UIManager : PropertiesClass
{
    public Text score,score2; //score is for informationPanel, score2 is for gameOverPanel
    public Dropdown widthDropdown,heightDropdown,colorCountDropdown;
    public GameObject settingsScreen;
    public Transform informationPanel;
    public Transform gameOverPanel;
    public bool isClick;
    private List<Color> colors = new List<Color>();

    private GridMapManager GridManagerObject;
    public static UIManager instance;
    private int colorCount;
    private int removedHexagons;
    private int bombCount;
    public int width=8, height=9, color=5;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    void Start()
    {
        bombCount = 0;
        GridManagerObject = GridMapManager.instance;
        removedHexagons = 0;
        colorCount = 7;
        DefaultSettings();
        Debug.Log("Height= " + height);
        Debug.Log("Width= " + width);
    }

    void Update()
    {
        if (isClick)
        {
            StartGameButton();
            isClick = false;
        }
    }


    public void BackButton(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }


    public void WidthDropdownChange(Dropdown dropdown)
    {

        if (dropdown.value == 0)
        {
            width = 5;
        }
        else if (dropdown.value == 1)
        {
            width = 6;
        }
        else if (dropdown.value == 2)
        {
            width = 7;
        }
        else if (dropdown.value == 3)
        {
            width = 8;
        }
        else if (dropdown.value == 4)
        {
            width = 9;
        }
        Debug.Log("Width DROP DOWN CHANGED -> " + dropdown.value);
        Debug.Log("Width= " + width);
    }

    public void HeightDropdownChange(Dropdown dropdown)
    {
        
        if (dropdown.value == 0)
        {
            height = 5;
        }
        else if (dropdown.value == 1)
        {
            height = 6;
        }
        else if (dropdown.value == 2)
        {
            height = 7;
        }
        else if (dropdown.value == 3)
        {
            height = 8;
        }
        else if (dropdown.value == 4)
        {
            height = 9;
        }
        else if (dropdown.value == 5)
        {
            height = 10;
        }
        else if (dropdown.value == 6)
        {
            height = 11;
        }
        else if (dropdown.value == 7)
        {
            height = 12;
        }
        else if (dropdown.value == 8)
        {
            height = 13;
        }
        Debug.Log("Height DROP DOWN CHANGED -> " + dropdown.value);
        Debug.Log("Height= " + height);
    }


    public void ColorDropdownChanged(Dropdown dropdown)
    {
        if (dropdown.value == 0)
        {
            color = 5;
        }
        else if (dropdown.value == 1)
        {
            color = 6;
        }
        else if (dropdown.value == 2)
        {
            color = 7;
        }
    }

       
    public void StartGameButton()
    { 
        settingsScreen.SetActive(false);
        informationPanel.gameObject.SetActive(true);

        GridManagerObject.SetGridHeight(height);
        GridManagerObject.SetGridWidth(width);

        

        
        if (color==5)
        {
            colors.Add(Color.white);
            colors.Add(Color.red);
            colors.Add(Color.yellow);
            colors.Add(Color.green);
            colors.Add(Color.magenta);
        }
        else if(color==6)
        {
            colors.Add(Color.white);
            colors.Add(Color.red);
            colors.Add(Color.yellow);
            colors.Add(Color.green);
            colors.Add(Color.magenta);
            colors.Add(Color.blue);
        }
        else if (color==7)
        {
            colors.Add(Color.white);
            colors.Add(Color.red);
            colors.Add(Color.yellow);
            colors.Add(Color.green);
            colors.Add(Color.magenta);
            colors.Add(Color.blue);
            colors.Add(Color.cyan);
        }
        GridManagerObject.SetColorList(colors);
        GridManagerObject.CreateGrid();
    }
    

    /* Sets default values to UI */
    public void DefaultSettings()
    {
        heightDropdown.value = Default_Grid_Height_Index;
        widthDropdown.value = Default_Grid_Width_Index;
        colorCountDropdown.value = Default_Color_Count_Index;
        color = Default_Color_Count;
        score.text = removedHexagons.ToString();
    }

    public void Score(int _Score)
    {
        removedHexagons += _Score;

        //3 Hexagons is removed => 3 x 5= 15. In every explosion, player will gain 15 point.
        score.text = (Score_Constant * removedHexagons).ToString();

        if (Int32.Parse(score.text) > Bomb_Score_Threshold * bombCount + Bomb_Score_Threshold)
        {
            ++bombCount;
            GridManagerObject.SetBombProduction();
        }
    }

    public void GameEnd()
    {
        gameOverPanel.gameObject.SetActive(true);
        score2.text = score.text;
    }

    
}
