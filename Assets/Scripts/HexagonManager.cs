﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexagonManager : PropertiesClass
{
	GridMapManager GridManagerObject;
	public int x;
	public int y;
	public Color color;
	public Vector2 Hexposition;
	public bool lerp;
	public Vector2 speed;
	private int bombTimer;
	private TextMesh text;

      
	/* Hold neighbour grid coordinates */
	public struct NeighbourHexes
	{
		public Vector2 up;
		public Vector2 upLeft;
		public Vector2 upRight;
		public Vector2 down;
		public Vector2 downLeft;
		public Vector2 downRight;
	}



	void Start()
	{
		GridManagerObject = GridMapManager.instance;
		lerp = false;
	}

	void Update()
	{
		if (lerp)
		{
			float newX = Mathf.Lerp(transform.position.x, Hexposition.x, Time.deltaTime * Hexagon_Rotate_Constant);
			float newY = Mathf.Lerp(transform.position.y, Hexposition.y, Time.deltaTime * Hexagon_Rotate_Constant);
			transform.position = new Vector2(newX, newY);


			if (Vector3.Distance(transform.position, Hexposition) < Hexagon_Rotate_Threshold)
			{
				lerp = false;
				transform.position = Hexposition;
				
			}
		}
	}


	/* Save rotate changes */
	public void Rotate(int newX, int newY, Vector2 newPos)
	{
		Hexposition = newPos;
		SetX(newX);
		SetY(newY);
		lerp = true;
	}


	/* Builds a struct from grid position of neighbour hexagons */
	public NeighbourHexes GetNeighbours()
	{
		NeighbourHexes neighbours;
		bool detect = GridManagerObject.DetectColumn(GetX());


		neighbours.down = new Vector2(x, y - 1);
		neighbours.up = new Vector2(x, y + 1);
		neighbours.upLeft = new Vector2(x - 1, detect ? y + 1 : y);
		neighbours.upRight = new Vector2(x + 1, detect ? y + 1 : y);
		neighbours.downLeft = new Vector2(x - 1, detect ? y : y - 1);
		neighbours.downRight = new Vector2(x + 1, detect ? y : y - 1);


		return neighbours;
	}



	/* Set new world position for hexagon */
	public void ChangeWorldPosition(Vector2 newPosition)
	{
		Hexposition = newPosition;
		lerp = true;
	}



	/* Set new grid position for hexagon */
	public void ChangeGridPosition(Vector2 newPosition)
	{
		x = (int)newPosition.x;
		y = (int)newPosition.y;
	}

	public void SetBomb()
	{
		text = new GameObject().AddComponent<TextMesh>();
		text.alignment = TextAlignment.Center;
		text.anchor = TextAnchor.MiddleCenter;
		text.transform.localScale = transform.localScale;
		text.transform.position = new Vector3(transform.position.x, transform.position.y, -4);
		text.color = Color.black;
		text.transform.parent = transform;
		bombTimer = Bomb_Timer_Start;
		text.text = bombTimer.ToString();
	}

	#region Setters & Getters

	public void SetX(int value) { x = value; }
	public int GetX() { return x; }

	public void SetY(int value) { y = value; }
	public int GetY() { return y; }

	public void SetColor(Color newColor) { GetComponent<SpriteRenderer>().color = newColor; color = newColor; }
	public Color GetColor() { return GetComponent<SpriteRenderer>().color; }

	public void Trigger() { --bombTimer; text.text = bombTimer.ToString(); }
	public int GetTimer() { return bombTimer; }
	#endregion


}
